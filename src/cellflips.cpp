#include "cellflips/cellflips.h"
#include "cellflips/cellflipsinvariant.h"

namespace cellflips
{

QTextStream& operator<<(QTextStream& ss, const InvariantReport::Cell& c)
{
  ss << "[" << c.dim << "|" << c.num << "]";
  return ss;
}

QTextStream& operator<<(QTextStream& ss, const InvariantReport::Error& err)
{
  ss << InvariantReport::invariantToText(err.inv) << ":";
  for(const InvariantReport::Cell& c: err.cells)
    ss << " " << c;
  return ss;
}

QTextStream& operator<<(QTextStream& ss, const InvariantReport& report)
{
  ss << "There are " << report.errors.size() << " violations of the invariants:" << endl;
  for(const InvariantReport::Error& err: report.errors)
    ss << err << endl;
  return ss;
}

}
