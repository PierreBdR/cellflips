// -*- c++ -*-
#ifndef CELLFLIP_H
#define CELLFLIP_H

/**
 * \file cellflips.vvh
 * This files include the definition of the \f$nD\f$-cell complex.
 */

#include "cellflips/cellflips_utils.h"
#include "cellflips/cellflipslayer.h"
#include "cellflips/chain.h"

#include <initializer_list>

/**
 * \defgroup internal Internal classes, not for general use
 * \defgroup utility Utility classes, not the main API, but may be useful
 * \defgroup main Main API classes, the user must know about them
 */

/**
 * \namespace cellflips
 * Namespace containing all the definitions to work with an \f$nD\f$-cell complex
 */
namespace cellflips
{
// Useful macros for types
  //#define TEMPLATE_CELL_COMPLEX int _N, typename CellContent, typename LayerComplex
#define TEMPLATE_CELL_COMPLEX typename VertexContent, typename... CellContents
  //#define CELL_COMPLEX_ARGS _N,CellContent,LayerComplex
#define CELL_COMPLEX_ARGS VertexContent, CellContents...

  //#define DEBUG_OUTPUT 1
#ifdef DEBUG_OUTPUT
#  define DEBUG_OUT(ss) out << ss
#else
#  define DEBUG_OUT(ss)
#endif

  /**
   * \ingroup main
   * \class CellComplex
   * Class representing the whole cell complex.
   *
   * This is the main class for the cell complex.
   * It contains an instance of the layer, starting from a special one for the top cell.
   */
  template <TEMPLATE_CELL_COMPLEX>
  struct CellComplex
  {
    enum {
      N = sizeof...(CellContents) ///< Dimension of the cell complex
    };

    typedef typename LayerConstructor<VertexContent,CellContents...>::type layers_t;

    /// This class
    typedef CellComplex Self;

    /// Type of the top layer complex
    typedef ComplexNthLayer<N+1,Self> top_complex_t;
    /// Type of the main layer complex, i.e. for the N-cells
    typedef ComplexNthLayer<N,Self> main_layer_complex_t;
    /// Type used to count
    typedef typename top_complex_t::size_type size_type;

    /**
     * Type of a cell of dimension N1
     */
    template <int N1>
    using ncell_t = Cell<N1,typename NTypes<N1,Self>::cell_content_t>;

    /**
     * Type of an oriented cell of dimension N1
     */
    template <int N1>
    using oriented_ncell_t = OrientedObject<Cell<N1,typename NTypes<N1,Self>::cell_content_t> >;

    /**
     * Type of a cell flip of dimension N1
     */
    template <int N1>
    using ncell_flip_t = CellFlip<N1,
                                  typename cellflips::NTypes<N1,Self>::cell_content_t,
                                  typename cellflips::NTypes<N1-1,Self>::cell_content_t,
                                  typename cellflips::NTypes<N1-2,Self>::cell_content_t>;

    template <int N1>
    using nflips_set_t = util::set_vector<ncell_flip_t<N1>*>;

    template <int N1>
    using cell_flips_set_t = typename ComplexNthLayer<N1,Self>::cell_flips_set_t;

    template <int N1>
    using face_flips_set_t = typename ComplexNthLayer<N1,Self>::face_flips_set_t;

    template <int N1>
    using joint_flips_set_t = typename ComplexNthLayer<N1,Self>::joint_flips_set_t;

    /**
     * Type of a cell range of dimension N1
     *
     * \note This type cannot be use if inference on N1 is needed
     */
    template <int N1>
    using ncell_range = typename NTypes<N1,Self>::cell_range;

    /**
     * Result of splitting a cell in two
     */
    template <int N1>
    struct SplitResult
    {
      /// Type of the cell begin split
      typedef ncell_t<N1> cell_t;
      /// Type of the face of the cell
      typedef ncell_t<N1-1> face_t;

      /// Creates an invalid split result
      SplitResult()
        : left(0)
        , right(0)
        , membrane(0)
      { }

      /// Creates an initialized split result
      SplitResult(const cell_t& c1,
                  const cell_t& c2,
                  const face_t& f)
        : left(c1)
        , right(c2)
        , membrane(f)
      { }

      /// Returns true if valid (i.e. all elements are non-null)
      explicit operator bool() { return left and right; }

      cell_t left; ///< Cell on the "left", i.e. containing <tt>+membrane</tt>
      cell_t right; ///< Cell on the "right", i.e. containing <tt>-membrane</tt>
      face_t membrane; ///< Membrane between the two new cells
    };

    ///\name Cell types
    //@{
    /// Type of the top cell
    typedef typename top_complex_t::cell_t top_t;
    /// Type of a N-cell
    typedef typename main_layer_complex_t::cell_t cell_t;
    /// Type of a N-1-cell
    typedef typename main_layer_complex_t::face_t face_t;
    /// Type of a 1-cell
    typedef ncell_t<1> edge_t;
    /// Type of a 0-cell
    typedef ncell_t<0> vertex_t;
    /// Type of the bottom cell
    typedef ncell_t<-1> bottom_t;

    /// Type of an oriented N-cell
    typedef typename cell_t::oriented_cell_t oriented_cell_t;
    /// Type of an oriented N-1-cell
    typedef typename face_t::oriented_cell_t oriented_face_t;
    /// Type of an oriented 1-cell
    typedef oriented_ncell_t<1> oriented_edge_t;
    /// Type of an oriented 0-cell
    typedef oriented_ncell_t<0> oriented_vertex_t;
    //@}

    ///\name Flips types
    //@{
    /// Type of a N-flip
    typedef typename main_layer_complex_t::cell_flip_t cell_flip_t;

    /// Type of a N+1-flip
    typedef typename top_complex_t::cell_flip_t top_cell_flip_t;
    //@}

    /// The top cell
    static const top_t T;
    /// The bottom cell
    static const bottom_t _;

    /**
     * Instance of query type
     */
    QueryType Q;

    /**
     * \enum ErrorCodes List of error codes
     */
    enum ErrorCodes
      {
        NO_ERROR = 0, ///< No error
        BAD_BOUNDARY, ///< Provided an invalid boundary
        CELL_ALREADY_IN_COMPLEX, ///< Try to add a cell already in the cell complex
        NO_SUCH_FLIP, ///< Trying to find a flip not in the cell complex
        NO_SUCH_FACE, ///< Trying to access a face not in the cell complex
        NO_SUCH_CELL, ///< Trying to access a cell not in the cell complex
        CANNOT_INSERT_FLIP, ///< Failed to insert a flip
        ORIENTATION_OF_UNRELATED_CELLS, ///< Trying to get the orientation of unrelated cells
        UNKNOWN_CELL_ORIENTATION, ///< Trying to access/remove an orientation that is not known
        WRONG_BOUNDARY, ///< Trying to use an invalid boudary
        WRONG_BOUNDARY_ORIENTATION, ///< Trying to use a boundary with inconsistent orientation
        WRONG_CELL_ORIENTATION, ///< Trying to use a cell badly oriented
        CELL_BOUNDARY_NOT_CLOSED, ///< Trying to use a non-closed boundary for a cell
        CELL_BOUNDARY_SELF_INTERSECT, ///< Trying to use a self-intersecting boundary for a cell
        CELL_BOUNDARY_MULTIPLE_PARTS, ///< Trying to use a boundary broken into multiple disconnected parts for a cell
        MEMBRANE_NOT_IN_CELL, ///< Trying to divide a cell with a membrane not contained in the cell
        MEMBRANE_DOESNT_CUT_CELL_IN_TWO, ///< Trying to divide a cell with a membrane that cut the cell in more than two
        FACE_ALREADY_BETWEEN_CELLS, ///< Indicate a face given to define a n-cell is already the interface between two existing cells
        CELL_IS_A_FACE, ///< Indicate you are trying to use a cell that is used as face
        CHAIN_CONNECTED_INSIDE, ///< Indicate you are trying to use a chain with element connected to the rest of the complex inside the chain
        STRUCTURE_INVALID ///< General error after which the structure is invalid and should not be used anymore
      };
    /**
     * Current error code
     */
    mutable ErrorCodes error;

    /**
     * Translate the error code into a human-readable string
     */
    QString errorString() const
    {
      return errorString(error);
    }

    QString errorString(ErrorCodes error) const
    {
      switch(error)
        {
        case NO_ERROR:
          return "No error";
        case BAD_BOUNDARY:
          return "Bad boudary";
        case CELL_ALREADY_IN_COMPLEX:
          return "Trying to insert a cell that is already in the cell complex";
        case NO_SUCH_FLIP:
          return "The flip was not found";
        case NO_SUCH_FACE:
          return "The face was not found";
        case NO_SUCH_CELL:
          return "The cell was not found";
        case CANNOT_INSERT_FLIP:
          return "Cannot insert flip in flip lists";
        case ORIENTATION_OF_UNRELATED_CELLS:
          return "Trying to get the relative orientation of unrelated cells.";
        case UNKNOWN_CELL_ORIENTATION:
          return "Trying to access/remove an orientation that is not known.";
        case WRONG_BOUNDARY:
          return "The specified boundary is incorrect.";
        case WRONG_BOUNDARY_ORIENTATION:
          return "Trying to add a cell with badly oriented boundary";
        case WRONG_CELL_ORIENTATION:
          return "The cell has an orientation incompatible with the others";
        case CELL_BOUNDARY_NOT_CLOSED:
          return "Specified a boundary that is not closed.";
        case CELL_BOUNDARY_SELF_INTERSECT:
          return "Specified a boundary that self-intersect.";
        case CELL_BOUNDARY_MULTIPLE_PARTS:
          return "Specified a boundary is in multiple parts";
        case STRUCTURE_INVALID:
          return "The structure doesn't represent anymore a cell complex for a N-manifold";
        case MEMBRANE_NOT_IN_CELL:
          return "The membrane is not part of the cell.";
        case MEMBRANE_DOESNT_CUT_CELL_IN_TWO:
          return "The membrane doesn't cut the cell in two parts.";
        case FACE_ALREADY_BETWEEN_CELLS:
          return "A face is already an interface between to N-cells, and cannot be added to a third one.";
        case CELL_IS_A_FACE:
          return "The cell is the face of some other cell and cannot be used in this context.";
        case CHAIN_CONNECTED_INSIDE:
          return "The chain is connected to the rest of the cell complex with element not on its boundary.";
        default:
          return "Unknown error";
        }
    }

    /**
     * Creates a new, empty cell complex
     */
    CellComplex()
      : error(NO_ERROR)
    {
      get_layer<-1>(*this).setBottom(_);
      get_layer<0>(*this).setBottom(_);
    }

    /**
     * Instanciate the data structure, starting from the top cell
     */
    layers_t _layers;

    /**
     * Remove all element from all dimensions
     */
    void clear()
    {
      _clear(IntConstant<N+1>());
    }

  protected:

    template <int N1>
    void _clear(const IntConstant<N1>&)
    {
      get_layer<N1>(*this).clear();
      _clear(IntConstant<N1-1>());
    }

    void _clear(const IntConstant<0>&)
    {
    }

  public:

    ///\name Cells lookup
    //@{
    /**
     * Return the list of N1-cells
     */
    template <int N1>
    ncell_range<N1> cells() const
    {
      return get_layer<N1>(*this).cells();
    }

    /**
     * Return the list of N-cells
     */
    ncell_range<N> cells() const
    {
      return cells<N>();
    }

    /**
     * Return the list of N-1-cells (i.e. faces)
     */
    ncell_range<N-1> faces() const
    {
      return cells<N-1>();
    }

    /**
     * Return the list of 1-cells (i.e. edges)
     */
    ncell_range<1> edges() const
    {
      return cells<1>();
    }

    /**
     * Return the list of 0-cells (i.e. vertices)
     */
    ncell_range<0> vertices() const
    {
      return cells<0>();
    }

    /**
     * Return a N1-cell
     */
    template <int N1>
    ncell_t<N1> any() const
    {
      return get_layer<N1>(*this).any();
    }

    /**
     * Return a N-Cell
     */
    cell_t anyCell() const { return any<N>(); }

    /**
     * Return a N-1-Cell
     */
    face_t anyFace() const { return any<N-1>(); }

    /**
     * Return a 1-Cell
     */
    edge_t anyEdge() const { return any<1>(); }

    /**
     * Return a 0-Cell
     */
    vertex_t anyVertex() const { return any<0>(); }

    //@}
    ///\name Number of cells
    //@{

    /**
     * Check if there are any cells in the cell complex.
     *
     * It is equivalent to call \c empty<0>().
     *
     * \note Due to the recursive nature, it is sufficient to test if there is any vertex.
     */
    bool empty() const
    {
      return empty<0>();
    }

    /**
     * Check if there is any cell of dimension N1
     */
    template <int N1>
    bool empty() const
    {
      return get_layer<N1>(*this).empty();
    }


    /**
     * Return the number of N1-cells
     */
    template <int N1>
    size_type nbCells() const
    {
      return get_layer<N1>(*this).nbCells();
    }

    /**
     * Return the number of N-cells
     */
    size_type nbCells() const
    {
      return nbCells<N>();
    }

    /**
     * Return the number of (N-1)-cells
     */
    size_type nbFaces() const
    {
      return nbCells<N-1>();
    }

    /**
     * Return the number of 1-cells
     */
    size_type nbEdges() const
    {
      return nbCells<1>();
    }

    /**
     * Return the number of 0-cells
     */
    size_type nbVertices() const
    {
      return nbCells<0>();
    }
    //@}
    ///\name Cell queries
    //@{

    /**
     * Check the N1-cell exists
     */
    template <int N1>
    bool hasCell(const ncell_t<N1>& cell) const
    {
      return get_layer<N1>(*this).hasCell(cell);
    }

    /**
     * Check if the N-1-cell exist
     */
    bool hasFace(const face_t& c) const
    {
      return hasCell<N-1>(c);
    }

    /**
     * Check if the 1-cell exist
     */
    bool hasEdge(const edge_t& c) const
    {
      return hasCell<1>(c);
    }

    /**
     * Check if the 0-cell exist
     */
    bool hasVertex(const vertex_t& c) const
    {
      return hasCell<0>(c);
    }

    /**
     * Check if the N-cell exist
     */
    bool hasCell(const cell_t& c) const
    {
      return hasCell<N>(c);
    }

    /**
     * Check the N1-cell exists
     */
    template <int N1>
    bool contains(const Cell<N1,typename NTypes<N1,Self>::cell_content_t>& cell) const
    {
      return hasCell(cell);
    }

    /// Get the edge between \c v1 and \c v2, if it exist
    edge_t edge(const vertex_t& v1, const vertex_t& v2) const
    {
      auto matches = match(Q, v1, v2, Q);
      if(!matches.empty())
        return (*matches.begin())->cell;
      return edge_t::null;
    }

    //@}

    ///\name Unordered flips lookup
    //@{

    /**
     * Flip the triplet \c cell, \c face, \c joint.
     *
     * Find the flip matching \c {cell,face,Q,joint} and return what \c Q matched.
     */
    template <int N1>
    ncell_t<N1-1> flip(const ncell_t<N1>& cell,
                       const ncell_t<N1-1>& face,
                       const ncell_t<N1-2>& joint) const
    {
      typedef ncell_flip_t<N1> l_cell_flip_t;
      typedef ncell_t<N1-1> l_face_t;
      l_cell_flip_t *res = get_layer<N1>(*this).flip(cell, face, joint);
      if(res)
        return res->otherFace(face);
      return l_face_t::null;
    }

    /**
     * Retrieve the set of flips in which \c cell is a cell
     */
    template <int N1>
    const cell_flips_set_t<N1>& flipsFromCell(const ncell_t<N1>& cell) const
    {
      return get_layer<N1>(*this).cellFlips(cell);
    }

    /**
     * Retrieve the set of flips in which \c cell is a face
     */
    template <int N1>
    const face_flips_set_t<N1+1>& flipsFromFace(const ncell_t<N1>& cell) const
    {
      return get_layer<N1+1>(*this).faceFlips(cell);
    }

    /**
     * Retrieve the set of flips in which \c cell is a joint
     */
    template <int N1>
    const joint_flips_set_t<N1+2>& flipsFromJoint(const ncell_t<N1>& cell) const
    {
      return get_layer<N1+2>(*this).jointFlips(cell);
    }

    /**
     * Find the subset of cell flips matching the flip given from the \c lst argument.
     */
    template <int N1>
    nflips_set_t<N1>
    match(const ncell_flip_t<N1>& flip,
          const nflips_set_t<N1>& lst) const
    {
      static_assert(N1>0 and N1 <= N+1, "Cell flips exists only for dimensions from 1 to N+1.");
      auto flips = nflips_set_t<N1>{};
      for(auto pf: lst)
        if(cellflips::match(flip,*pf))
          flips.insert(pf);
      return flips;
    }

    /**
     * Find the subset of cell flips matching the flip given from the flips in the cell complex.
     */
    template <int N1>
    nflips_set_t<N1>
    match(const ncell_flip_t<N1>& flip) const
    {
      return match(flip.cell, flip.face1, flip.face2, flip.joint);
    }

#ifndef DOXYGEN_SHOULD_SKIP_THIS
    template <int N1>
    const cell_flips_set_t<N1>&
    match(const ncell_t<N1>& c,
          const QueryType&, const QueryType&, const QueryType) const
    {
      return flipsFromCell(c);
    }

    template <int N1>
    const face_flips_set_t<N1+1>&
    match(const QueryType&,
          const ncell_t<N1>& c,
          const QueryType&, const QueryType&) const
    {
      return flipsFromFace(c);
    }

    template <int N1>
    const face_flips_set_t<N1+1>&
    match(const QueryType&, const QueryType&,
          const ncell_t<N1>& c,
          const QueryType&) const
    {
      return flipsFromFace(c);
    }

    template <int N1>
    const joint_flips_set_t<N1+2>&
    match(const QueryType&, const QueryType&, const QueryType,
          const ncell_t<N1>& c ) const
    {
      return flipsFromJoint(c);
    }

    template <int N1>
    nflips_set_t<N1>
    match(const ncell_t<N1>& c,
          const ncell_t<N1-1>& f1, const QueryType&, const QueryType&) const
    {
      if(c.isNull())
      {
        const auto& result = match(Q,f1,Q,Q);
        return nflips_set_t<N1>(begin(result), end(result));
      }
      else if(f1.isNull())
      {
        const auto& result = match(c,Q,Q,Q);
        return nflips_set_t<N1>(begin(result), end(result));
      }
      auto result = nflips_set_t<N1>{};
      for(const auto& pfl: flipsFromFace(f1))
      {
        if(pfl->cell == c)
          result.insert(result.end(), pfl);
      }
      return result;
    }

    template <int N1>
    nflips_set_t<N1>
    match(const ncell_t<N1>& c, const QueryType&,
          const ncell_t<N1-1>& f1, const QueryType&) const
    {
      return match(c, f1, Q, Q);
    }

    template <int N1>
    nflips_set_t<N1>
    match(const ncell_t<N1>& c,
          const ncell_t<N1-1>& f1, const ncell_t<N1-1>& f2, const QueryType&) const
    {
      if(c.isNull())
        return match(Q,f1,f2,Q);
      else if(f1.isNull())
        return match(c,f2,Q,Q);
      else if(f2.isNull())
        return match(c,f1,Q,Q);
      auto result = flipsFromFace(f1);
      for(auto it = result.begin() ; it != result.end() ; )
        {
          if((*it)->cell == c and ((*it)->face1 == f2 or (*it)->face2 == f2))
            ++it;
          else
            it = result.erase(it);
        }
      return result;
    }

    template <int N1>
    nflips_set_t<N1>
    match(const ncell_t<N1>& c, const QueryType&, const QueryType&,
          const ncell_t<N1-2>& j) const
    {
      if(c.isNull())
      {
        const auto& result = match(Q,Q,Q,j);
        return nflips_set_t<N1>(begin(result), end(result));
      }
      else if(j.isNull())
      {
        const auto& result = match(c,Q,Q,Q);
        return nflips_set_t<N1>(begin(result), end(result));
      }
      typedef ncell_flip_t<N1> l_cell_flip_t;
      l_cell_flip_t *res = get_layer<N1>(*this).flip(c, j);
      nflips_set_t<N1> result;
      if(res)
        result.insert(res);
      return result;
    }

    template <int N1>
    nflips_set_t<N1>
    match(const ncell_t<N1>& cell, const ncell_t<N1-1>& face, const QueryType&,
          const ncell_t<N1-2>& joint) const
    {
      if(cell.isNull())
        return match(Q,face,Q,joint);
      else if(face.isNull())
        return match(cell,Q,Q,joint);
      else if(joint.isNull())
        return match(cell,face,Q,Q);
      typedef ncell_flip_t<N1> l_cell_flip_t;
      l_cell_flip_t *res = get_layer<N1>(*this).flip(cell, face, joint);
      auto result = nflips_set_t<N1>{};
      if(res)
        result.insert(res);
      return result;
    }

    template <int N1>
    nflips_set_t<N1>
    match(const ncell_t<N1>& c, const QueryType&, const ncell_t<N1-1>& face,
          const ncell_t<N1-2>& j) const
    {
      return match(c, face, Q, j);
    }

    template <int N1>
    nflips_set_t<N1+1>
    match(const QueryType&, const ncell_t<N1>& f1, const ncell_t<N1>& f2, const QueryType& ) const
    {
      if(f1.isNull())
      {
        const auto& result = match(Q, f2, Q, Q);
        return nflips_set_t<N1+1>(begin(result), end(result));
      }
      else if(f2.isNull())
      {
        const auto& result = match(Q, f1, Q, Q);
        return nflips_set_t<N1+1>(begin(result), end(result));
      }
      else if(f1 == f2)
        return nflips_set_t<N1+1>();

      auto result = nflips_set_t<N1+1>{flipsFromFace(f1)};
      for(auto it = result.begin() ; it != result.end() ; )
        {
          if((*it)->face1 == f2 or (*it)->face2 == f2)
            ++it;
          else
            it = result.erase(it);
        }
      return result;
    }

    template <int N1>
    nflips_set_t<N1+1>
    match(const QueryType&, const ncell_t<N1>& f1, const ncell_t<N1>& f2, const ncell_t<N1-1>& j) const
    {
      if(f1.isNull())
        return match(Q, f2, Q, j);
      else if(f2.isNull())
        return match(Q, f1, Q, j);
      else if(j.isNull())
        return match(Q, f1, f2, Q);
      else if(f1 == f2)
        return nflips_set_t<N1+1>();

      auto result = nflips_set_t<N1+1>{flipsFromFace(f1)};
      for(auto it = result.begin() ; it != result.end() ; )
        {
          if((*it)->joint == j and ((*it)->face1 == f2 or (*it)->face2 == f2))
            ++it;
          else
            it = result.erase(it);
        }
      return result;
    }

    template <int N1>
    nflips_set_t<N1+1>
    match(const QueryType&, const ncell_t<N1>& f1, const QueryType&, const ncell_t<N1-1>& j) const
    {
      if(f1.isNull())
      {
        const auto& result = match(Q,Q,Q,j);
        return nflips_set_t<N1+1>(begin(result), end(result));
      }
      else if(j.isNull())
      {
        const auto& result = match(Q, f1, Q, Q);
        return nflips_set_t<N1+1>(begin(result), end(result));
      }

      auto result = nflips_set_t<N1+1>{flipsFromFace(f1)};
      for(auto it = result.begin() ; it != result.end() ; )
        {
          if((*it)->joint == j)
            ++it;
          else
            it = result.erase(it);
        }
      return result;
    }

    template <int N1>
    nflips_set_t<N1+1>
    match(const QueryType&, const QueryType&, const ncell_t<N1>& f1, const ncell_t<N1-1>& j) const
    {
      return match(Q, f1, Q, j);
    }

    template <int N1>
    nflips_set_t<N1>
    match(const ncell_t<N1>& cell, const ncell_t<N1-1>& face1, const ncell_t<N1-1>& face2, const ncell_t<N1-2>& joint) const
    {
      if(cell.isNull())
        return match(Q, face1, face2, joint);
      else if(face1.isNull())
        return match(cell, face2, Q, joint);
      else if(face2.isNull())
        return match(cell, face1, Q, joint);
      else if(joint.isNull())
        return match(cell, face1, face2, Q);

      const auto& flips = flipsFromCell(cell);
      nflips_set_t<N1> result;
      ncell_flip_t<N1> flip(cell, face1, face2, joint);
      for(auto it = flips.begin() ; it != flips.end() ; ++it)
        {
          if(flip.equiv(**it))
            {
              result.insert(*it);
              return result;
            }
        }
      return result;
      //return match(cellFlip(cell, face1, face2, joint));
    }

#else // DOXYGEN_SHOULD_SKIP_THIS
    /**
     * Find the set of flips matching the results.
     *
     * Each argument, but not all at once, can be replaced by a QueryType variable, which is equivalent to put a null cell.
     * Note that the methods with a QueryType are implemented separately to optimise access to the tuples.
     *
     * Also, all the methods having only one non-query variable return constant reference to the actual data structure, which avoid copying it.
     */
    template <int N1>
    nflips_set_t<N1>
    match(const ncell_t<N1>& cell, const ncell_t<N1-1>& face1, const ncell_t<N1-1>& face2, const ncell_t<N1-2>& joint)
    {
      // Implementation depends on the actual type of the cells
      return nflips_set_t<N1>();
    }
#endif // DOXYGEN_SHOULD_SKIP_THIS

    //@}
    ///\name Neighborhood relationship
    //@{

    /**
     * Get the neighbors of the cell \c c
     */
    template <int N1>
    Set<ncell_t<N1>>
    neighbors(const ncell_t<N1>& c) const
    {
      static_assert(N1 >= 0 and N1 <= N, "neighbors can only be called for cells of dimension from 0 to N.");
      auto result = set(cobounds(bounds(c)));
      result.erase(c);
      return result;
    }

    /**
     * As a special case, the neighbors of a vertex are actually the adjacent cells of the vertex
     */
    Set<vertex_t> neighbors(const vertex_t& v) const
    {
      return adjacentCells(v);
    }

    /**
     * As an optimization, the neighbors of a \f$N\f$-cell are the adjacent cells
     */
    Set<cell_t> neighbors(const cell_t& c) const
    {
      return adjacentCells(c);
    }

    /**
     * Number of neighbors for the cell \c c
     */
    template <int N1>
    size_type nbNeighbors(const ncell_t<N1>& c) const
    {
      return neighbors(c).size();
    }

    /**
     * Check if \c c1 and \c c2 are neighbors
     */
    template <int N1>
    bool areNeighbors(const ncell_t<N1>& c1,
                      const ncell_t<N1>& c2) const
    {
      static_assert(N1 >= 0 and N1 <= N, "areNeighbors can only be called for cells of dimension from 0 to N.");
      typedef ncell_flip_t<N1> l_cell_flip_t;
      for(l_cell_flip_t* pf: match(c1, Q, Q, Q))
        {
          if(!match(c2, pf->face1, Q, Q).empty()) return true;
          if(!match(c2, pf->face2, Q, Q).empty()) return true;
        }
      return false;
    }

    /**
     * As a special case, two vertices are neighbors if they are adjacent
     */
    bool areNeighbors(const vertex_t& v1, const vertex_t& v2) const
    {
      return areAdjacent(v1, v2);
    }

    /**
     * As an optimization, two \f$N\f$-cells are neighbors if they are adjacent
     */
    bool areNeighbors(const cell_t& c1, const cell_t& c2) const
    {
      return areAdjacent(c1, c2);
    }

    /**
     * Return a neighbor of the cell
     */
    template <int N1>
    const ncell_t<N1>&
    anyNeighbor(const ncell_t<N1>& c) const
    {
      typedef ncell_t<N1> l_cell_t;
      typedef ncell_flip_t<N1> l_cell_flip_t;
      for(l_cell_flip_t* pf1: match(c,Q,Q,Q))
        {
          for(l_cell_flip_t* pf2: match(Q,pf1->face1,Q,Q)) if(pf2->cell != c)
            return pf2->cell;
          for(l_cell_flip_t* pf2: match(Q,pf1->face2,Q,Q)) if(pf2->cell != c)
            return pf2->cell;
        }
      return l_cell_t::null;
    }

    /**
     * As a special case, a neighbor of a vertex is an adjacent cell of the vertex
     */
    const vertex_t& anyNeighbor(const vertex_t& v) const
    {
      return anyAdjacentCell(v);
    }

    /**
     * As an optimization, a neighbor of a \f$N\f$-cell is an adjacent cell
     */
    const cell_t& anyNeighbor(const cell_t& c) const
    {
      return anyAdjacentCell(c);
    }

    /**
     * Get the co-neighbors of the cell \c c
     */
    template <int N1>
    Set<ncell_t<N1>>
    coneighbors(const ncell_t<N1>& c) const
    {
      static_assert(N1 >= 0 and N1 <= N, "coneighbors can only be called for cells of dimension from 0 to N.");
      Set<ncell_t<N1>> result = bounds(cobounds(c));
      result.erase(c);
      return result;
    }

    /**
     * Number of co-neighbors of a cell
     */
    template <int N1>
    size_type nbConeighbors(const ncell_t<N1>& c) const
    {
      return coneighbors(c).size();
    }

    /**
     * Check if \c c1 and \c c2 are co-neighbors
     */
    template <int N1>
    bool areConeighbors(const ncell_t<N1>& c1,
                        const ncell_t<N1>& c2) const
    {
      static_assert(N1 >= 0 and N1 <= N, "areNeighbors can only be called for cells of dimension from 0 to N.");
      typedef ncell_flip_t<N1+1> l_cell_flip_t;
      for(l_cell_flip_t* pf: match(Q, c1, Q, Q))
        if(!match(pf->cell, c2, Q, Q).empty()) return true;
      return false;
    }

    /**
     * Return a co-neighbor of the cell \c c
     */
    template <int N1>
    const ncell_t<N1>&
    anyConeighbor(const ncell_t<N1>& c) const
    {
      typedef ncell_t<N1> l_cell_t;
      typedef ncell_flip_t<N1+1> l_cell_flip_t;
      for(l_cell_flip_t* pf1: match(Q,c,Q,Q))
        return pf1->otherFace(c);
      return l_cell_t::null;
    }

    /**
     * Get the adjacent cells of the cell \c c
     */
    template <int N1>
    Set<ncell_t<N1>>
    adjacentCells(const ncell_t<N1>& c) const
    {
      typedef ncell_t<N1> l_cell_t;
      typedef ncell_flip_t<N1+1> l_coface_flip_t;

      Set<l_cell_t> result;

      const auto& matches = match(Q,c,Q,Q);
      for(l_coface_flip_t* pf: matches)
      result.insert(pf->otherFace(c));

      return result;
    }

    /**
     * Number of cells adjacent to \c c
     */
    template <int N1>
    size_type nbAdjacentCells(const ncell_t<N1>& c) const
    {
      return adjacentCells(c).size();
    }

    /**
     * Check if \c c1 and \c c2 are adjacent
     */
    template <int N1>
    bool areAdjacent(const ncell_t<N1>& c1,
                     const ncell_t<N1>& c2) const
    {
      static_assert(N1 >= 0 and N1 <= N, "areAdjacent can only be called for cells of dimension from 0 to N.");
      return not match(Q, c1, c2, Q).empty();
    }

    /**
     * Return an adjacent cell of \c c
     */
    template <int N1>
    const ncell_t<N1>&
    anyAdjacentCell(const ncell_t<N1>& c) const
    {
      static_assert(N1 >= 0 and N1 <= N, "areNeighbors can only be called for cells of dimension from 0 to N.");
      typedef ncell_flip_t<N1+1> l_cell_flip_t;
      typedef ncell_t<N1> l_face_t;

      for(l_cell_flip_t* pf: match(Q, c, Q, Q))
        return pf->otherFace(c);

      return l_face_t::null;
    }
    //@}
    ///\name Bounds - Descending topological relationship
    //@{

    /**
     * Returns the bounds (i.e. faces) of a vertex.
     *
     * \note If the vertex is in the complex, this is the bottom cell.
     */
    Set<bottom_t> bounds(const vertex_t& v) const
    {
      Set<bottom_t> result;
      if(hasCell(v))
        result.insert(_);
      return result;
    }

    /**
     * Returns the union of the bounds of a set of cells
     */
    template <int N1>
    Set<ncell_t<N1-1>>
    bounds(const Set<ncell_t<N1>>& cset) const
    {
      return bounds<N1-1>(cset);
    }

    /**
     * Returns the union of the N1-bounds of a set of cells
     */
    template <int N1, int N2>
    Set<ncell_t<N1>>
    bounds(const Set<ncell_t<N2>>& cset) const
    {
      typedef ncell_t<N2> l_cell_t;
      typedef ncell_t<N1> l_bound_t;
      Set<l_bound_t> result;
      for(const l_cell_t& c: cset)
        result |= set(bounds<N1>(c));
      return result;
    }

    /**
     * Returns the set of bounds of a cell
     */
    template <int N1>
    Set<ncell_t<N1-1> >
    bounds(const ncell_t<N1>& c) const
    {
      static_assert(N1 > 0 and N1 <= N+1, "You cannot only ask for the bounds of a cell of dimension between 0 and N+1.");
      typedef ncell_t<N1-1> l_face_t;
      typedef ncell_flip_t<N1> l_cell_flip_t;
      Set<l_face_t> result;
      const auto& matches = match(c,Q,Q,Q);
      for(l_cell_flip_t* pct: matches)
      {
        result.insert(pct->face1);
        result.insert(pct->face2);
      }
      return result;
    }

    /**
     * Get the set of N1-cells that are in the boundary of \c c
     */
    template <int N1, int N2>
    Set<ncell_t<N1>>
    bounds(const ncell_t<N2>& c) const
    {
      static_assert(N2 > N1, "The dimension of the n-bounds must be stricly smaller than the dimension of the cell.");
      static_assert(N2 > N1 and N2 <= N+1, "The cell must be of dimension from N2+1 to N+1.");
      return _bounds<N1>(c);
    }

    /**
     * Number of bounds of the cell \c c
     */
    template <int N1>
    size_type nbBounds(const ncell_t<N1>& c) const
    {
      return bounds(c).size();
    }

    /**
     * Number of N1-bounds of the cell \c c
     */
    template <int N1, int N2>
    size_type nbBounds(const ncell_t<N2>& c) const
    {
      return bounds<N1>(c).size();
    }

    /**
     * Number of bounds of a set of cells
     */
    template <int N1>
    size_type nbBounds(const Set<ncell_t<N1>>& c) const
    {
      return bounds(c).size();
    }

    /**
     * Number of N1-bounds of a set of cells
     */
    template <int N1, int N2>
    size_type nbBounds(const Set<ncell_t<N2>>& c) const
    {
      return bounds<N1>(c).size();
    }

    /// Check if \c c2 is in the boundary of \c c1
    template <int N1, int N2>
    bool isBound(const ncell_t<N1>& c1,
                 const ncell_t<N2>& c2) const
    {
      static_assert((N2 >= -1) and (N1 <= N+1) and (N1 > N2), "The second cell has to be of dimension strictly smaller than the first cell and between -1 and N+1 (included).");
      return _isBound(c1, c2);
    }

    /**
     * Returns a N1-bound of the cell \c c
     */
    template <int N1, int N2>
    const ncell_t<N1>& anyBound(const ncell_t<N2>& c) const
    {
      static_assert(N1 >= -1 and N1<N2, "The dimension of a n-bound must be stricly smaller than the dimension of the cell.");
      return _anybounds<N1>(c);
    }

    /**
     * Returns a N1-bound of the vertex \c v
     *
     * \note N1 MUST be -1
     */
    template <int N1>
    const ncell_t<N1>& anyBound(const vertex_t& v) const
    {
      static_assert(N1 == -1, "You can only ask for the -1-bounds of a vertex.");
      return anyBound(v);
    }


    /**
     * Return a bound of the cell \c c
     */
    template <int N1>
    const ncell_t<N1-1>& anyBound(const ncell_t<N1>& c) const
    {
      static_assert(N1 > 0 and N1 <= N+1, "You cannot only ask for the bounds of a cell of dimension between 0 and N+1.");
      typedef ncell_t<N1-1> l_face_t;
      const auto& matches = match(c, Q, Q, Q);
      if(not matches.empty())
        return (*matches.begin())->face1;
      return l_face_t::null;
    }

    /**
     * Return the bottom cell if the vertex exist, as the bottom cell is always a face of a vertex
     */
    const bottom_t& anyBound(const vertex_t& v) const
    {
      if(hasCell(v)) return _;
      return bottom_t::null;
    }

    /**
     * Return the faces of the cell \c c, i.e. the N1-1-bounds
     */
    template <int N1>
    Set<ncell_t<N1-1>> faces(const ncell_t<N1>& c) const
    {
      return bounds(c);
    }

    /**
     * Return the faces of a set of cells, i.e. the N1-1-bounds
     */
    template <int N1>
    Set<ncell_t<N1-1>> faces(const Set<ncell_t<N1>>& c) const
    {
      return bounds(c);
    }

    /**
     * Number of faces of the cell \c c
     */
    template <int N1>
    size_type nbFaces(const ncell_t<N1>& c) const
    {
      return nbBounds(c);
    }

    /**
     * Number of faces of a set of cells
     */
    template <int N1>
    size_type nbFaces(const Set<ncell_t<N1>>& c) const
    {
      return nbBounds(c);
    }

    /**
     * Return true if \c f is a face of \c c.
     */
    template <int N1>
    bool isFace(const ncell_t<N1>& c, const ncell_t<N1-1>& f) const
    {
      return isBound(c,f);
    }

    /**
     * Return a bound of the cell \c c, i.e. a N1-1-bound
     */
    template <int N1>
    const ncell_t<N1-1>& anyFace(const ncell_t<N1>& c) const
    {
      return anyBound(c);
    }

    /**
     * Return the joints of a cell
     */
    Set<bottom_t> joints(edge_t& e) const
    {
      Set<bottom_t> result;
      if(hasCell(e)) result.insert(_);
      return result;
    }

    /**
     * Return the joints of a cell
     */
    template <int N1>
    Set<ncell_t<N1-2>>
    joints(const ncell_t<N1>& c) const
    {
      static_assert(N1 > 1 and N1 <= N+1, "You can only ask joints of a cell of dimension between 2 and N+1, inclus.");
      typedef ncell_flip_t<N1> l_cell_flip_t;
      typedef ncell_t<N1-2> l_joint_t;
      Set<l_joint_t> result;
      const auto& matches = match(c,Q,Q,Q);
      for(l_cell_flip_t* pf: matches)
        result.insert(pf->joint);
      return result;
    }

    /**
     * Joints of a set of cells
     */
    template <int N1>
    Set<ncell_t<N1-2>>
    joints(const Set<ncell_t<N1>>& C) const
    {
      return bounds<N1-2>(C);
    }

    /**
     * Number of joints of the cell \c c
     */
    template <int N1>
    size_type nbJoints(const ncell_t<N1>& c) const
    {
      return joints(c).size();
    }

    /**
     * Number of joints of a set of cells
     */
    template <int N1>
    size_type nbJoints(const Set<ncell_t<N1>>& c) const
    {
      return joints(c).size();
    }

    /**
     * Return true if \c j is a joint of the cell \c c
     */
    template <int N1>
    bool isJoint(const ncell_t<N1>& c, const ncell_t<N1-2>& j) const
    {
      return isBound(c,j);
    }

    /**
     * Return a joint of the cell \c c, i.e. a N1-2-bound
     */
    template <int N1>
    const ncell_t<N1-2>& anyJoint(const ncell_t<N1>& c) const
    {
      static_assert(N1 > 1 and N1 <= N+1, "You cannot only ask for the joints of a cell of dimension between 1 and N+1.");
      typedef ncell_t<N1-2> l_joint_t;
      const auto& matches = match(c,Q,Q,Q);
      if(not matches.empty())
        return (*matches.begin())->joint;
      return l_joint_t::null;
    }

    /**
     * Return the bottom cell if the edge exist, as the bottom cell is the joint of all edge.
     */
    const bottom_t& anyJoint(const edge_t& e) const
    {
      if(hasCell(e)) return _;
      return bottom_t::null;
    }

    /**
     * Return the list of edges of the cell
     */
    template <int N1>
    Set<edge_t> edges(const ncell_t<N1>& c) const
    {
      return bounds<1>(c);
    }

    /**
     * Return the list of edges in a set of cells
     */
    template <int N1>
    Set<edge_t> edges(const Set<ncell_t<N1>>& c) const
    {
      return bounds<1>(c);
    }

    /**
     * Return the number of edges of the cell
     */
    template <int N1>
    size_type nbEdges(const ncell_t<N1>& c) const
    {
      return edges(c).size();
    }

    /**
     * Return the number of edges in a set of cells
     */
    template <int N1>
    size_type nbEdges(const Set<ncell_t<N1>>& c) const
    {
      return edges(c).size();
    }

    /**
     * Return true if \c e is an edge of \c c
     */
    template <int N1>
    bool isEdge(const ncell_t<N1>& c, const edge_t& e) const
    {
      return isBound(c,e);
    }

    /**
     * Return an edge of the cell
     */
    template <int N1>
    const edge_t& anyEdge(const ncell_t<N1>& c) const
    {
      return anyBound<1>(c);
    }

    /**
     * Return the list of edges of the cell
     */
    template <int N1>
    Set<vertex_t> vertices(const ncell_t<N1>& c) const
    {
      return bounds<0>(c);
    }

    /**
     * Return the list of edges in a set of cells
     */
    template <int N1>
    Set<vertex_t> vertices(const Set<ncell_t<N1>>& c) const
    {
      return bounds<0>(c);
    }

    /**
     * Return the number of vertices in a cell
     */
    template <int N1>
    size_type nbVertices(const ncell_t<N1>& c) const
    {
      return vertices(c).size();
    }

    /**
     * Return the number of vertices in a set of cells
     */
    template <int N1>
    size_type nbVertices(const Set<ncell_t<N1>>& c) const
    {
      return vertices(c).size();
    }

    /**
     * Return true if \c v is a vertex of \c c
     */
    template <int N1>
    bool isVertex(const ncell_t<N1>& c, const vertex_t& v) const
    {
      return isBound(c,v);
    }

    /**
     * Retyrb a vertex of the cell
     */
    template <int N1>
    const vertex_t& anyVertex(const ncell_t<N1>& c) const
    {
      return anyBound<0>(c);
    }

    //@}

    ///\name Cobounds - Ascending topological relationship
    //@{
    /**
     * Return the set of cobounds of a cell
     *
     * The co-bounds are the cells that have \c c as a face
     */
    Set<ncell_t<N+1>>
    cobounds(const cell_t& c) const
    {
      Set<ncell_t<N+1>> result;
      if(hasCell(c))
        result.insert(T);
      return result;
    }

    /**
     * Return the cobounds of the bottom cell (i.e. the vertices)
     */
    Set<vertex_t>
    cobounds(const bottom_t& c) const
    {
      Set<vertex_t> result;
      if(hasCell(c))
      {
        const auto& vs = vertices();
        result.insert(begin(vs), end(vs));
      }
      return result;
    }

    /**
     * Return the set of cobounds of a cell
     *
     * The co-bounds are the cells that have \c c as a face
     */
    template <int N1>
    Set<ncell_t<N1+1> >
    cobounds(const ncell_t<N1>& f) const
    {
      static_assert(N1 >= -1 and N1 < N, "You can only ask for the cobounds of a cell of dimension between -1 and N.");
      typedef ncell_t<N1+1> l_cell_t;
      typedef ncell_flip_t<N1+1> l_cell_flip_t;
      Set<l_cell_t> result;
      const auto& matches = match(Q,f,Q,Q);
      for(l_cell_flip_t* pct: matches)
      {
        result.insert(pct->cell);
      }
      return result;
    }

    /**
     * Return the set of N1-cobounds of a cell
     *
     * The co-bounds are the cells that have \c c as a bound
     */
    template <int N1, int N2>
    Set<ncell_t<N1> >
    cobounds(const ncell_t<N2>& c) const
    {
      static_assert(N1>N2, "The return-dimension must be greater than the dimension of the cell.");
      static_assert(N2 >= -1 and N2 < N1, "The dimension of the cell must be between -1 and N1-1.");
      return _cobounds<N1>(c);
    }

    /**
     * Return the N1-cobounds of a set of cells
     */
    template <int N1, int N2>
    Set<ncell_t<N1>>
    cobounds(const Set<ncell_t<N2>>& C) const
    {
      typedef ncell_t<N2> l_cell_t;
      Set<ncell_t<N1>> result;
      for(const l_cell_t& c: C)
        result |= cobounds<N1>(c);
      return result;
    }

    /**
     * Return the cobounds of a set of cells
     */
    template <int N1>
    Set<ncell_t<N1+1>>
    cobounds(const Set<ncell_t<N1>>& C) const
    {
      return cobounds<N1+1>(C);
    }

    /**
     * Return the number of co-bounds of the cell \c c
     */
    template <int N1>
    size_type nbCobounds(const ncell_t<N1>& c) const
    {
      return cobounds(c).size();
    }

    /**
     * Return the number of N1-co-bounds of the cell \c c
     */
    template <int N1, int N2>
    size_type nbCobounds(const ncell_t<N2>& c) const
    {
      return cobounds<N1>(c).size();
    }

    /**
     * Return the number of co-bounds of a set of cells
     */
    template <int N1>
    size_type nbCobounds(const Set<ncell_t<N1>>& c) const
    {
      return cobounds(c).size();
    }

    /**
     * Return the number of N1-co-bounds of a set of cells
     */
    template <int N1, int N2>
    size_type nbCobounds(const Set<ncell_t<N2>>& c) const
    {
      return cobounds<N1>(c).size();
    }

    /**
     * Return true if \c c2 is a cobounds of \c c1
     */
    template <int N1, int N2>
    bool isCobound(const ncell_t<N1>& c1, const ncell_t<N2>& c2) const
    {
      static_assert(N2 > N1, "The dimension of the cobound must be higher than the dimension of the reference cell.");
      return isBound(c2, c1);
    }

    /**
     * Return a cell for which \c c is a bound
     */
    template <int N1>
    const ncell_t<N1+1>& anyCobound(const ncell_t<N1>& c) const
    {
      static_assert(N1 >= -1 and N1 < N, "anyCobound requires a cell of dimension between -1 and N.");
      typedef ncell_t<N1+1> l_cell_t;
      const auto& matches = match(Q,c,Q,Q);
      if(not matches.empty())
        return (*matches.begin())->cell;
      return l_cell_t::null;
    }

    /**
     * If the cell exist, return the top cell, as all cells are faces of the top cell.
     */
    const top_t& anyCobound(const cell_t& c) const
    {
      if(hasCell(c)) return T;
      return top_t::null;
    }

    /**
     * Return a N1-cell for which \c c is a bound
     */
    template <int N1, int N2>
    const ncell_t<N1>& anyCobound(const ncell_t<N2>& c) const
    {
      static_assert(N1 > N2, "The dimension of the cobound must be higher than the dimension of the reference cell.");
      static_assert(N2 >= -1, "The dimension of the cell must be at least -1.");
      return _anycobound<N1>(c);
    }

    /**
     * Return the cofaces of a cell
     */
    template <int N1>
    Set<ncell_t<N1+1> >
    cofaces(const ncell_t<N1>& f) const
    {
      return cobounds(f);
    }

    /**
     * Return the cofaces of a set of cells
     */
    template <int N1>
    Set<ncell_t<N1+1> >
    cofaces(const Set<ncell_t<N1>>& F) const
    {
      return cobounds(F);
    }

    /**
     * Return the number of cofaces of a cell
     */
    template <int N1>
    size_type nbCofaces(const ncell_t<N1>& f) const
    {
      return cofaces(f).size();
    }

    /**
     * Return the number of cofaces of a set of cells
     */
    template <int N1>
    size_type nbCofaces(const Set<ncell_t<N1>>& f) const
    {
      return cofaces(f).size();
    }

    /**
     * Return true is \c c is a coface of \c f
     */
    template <int N1>
    bool isCoface(const ncell_t<N1>& f, const ncell_t<N1+1>& c) const
    {
      return isCobound(f,c);
    }

    /**
     * Return the cofaces of a cell
     */
    template <int N1>
    ncell_t<N1+1>
    anyCoface(const ncell_t<N1>& f) const
    {
      return anyCobound(f);
    }

    /**
     * Return the co-joints of a cell
     *
     * Co-joints are cells having \c e as joint
     */
    Set<top_t> cojoints(const face_t& e) const
    {
      Set<top_t> result;
      if(hasCell(e)) result.insert(T);
      return result;
    }

    /**
     * Return the co-joints of a cell
     *
     * Co-joints are cells having \c c as joint
     */
    template <int N1>
    Set<ncell_t<N1+2>>
    cojoints(const ncell_t<N1>& c) const
    {
      typedef ncell_flip_t<N1+2> l_cell_flip_t;
      typedef ncell_t<N1+2> l_cell_t;
      Set<l_cell_t> result;
      const auto& matches = match(Q,Q,Q,c);
      for(l_cell_flip_t* pf: matches)
        result.insert(pf->cell);
      return result;
    }

    /**
     * Return the cojoints of a set of cells
     */
    template <int N1>
    Set<ncell_t<N1+2> >
    cojoints(const Set<ncell_t<N1>>& J) const
    {
      return cobounds<N1+2>(J);
    }

    /**
     * Return the number of cojoints of a cell
     */
    template <int N1>
    size_type nbCojoints(const ncell_t<N1>& j) const
    {
      return cojoints(j).size();
    }

    /**
     * Return the number of cojoints of a set of cells
     */
    template <int N1>
    size_type nbCojoints(const Set<ncell_t<N1>>& j) const
    {
      return cojoints(j).size();
    }

    /**
     * Return true if \c j is a joint of \c c
     */
    template <int N1>
    bool isCojoint(const ncell_t<N1>& j, const ncell_t<N1+2>& c) const
    {
      return isBound(c,j);
    }

    /**
     * Return the top-cell if the face exist, as a face is always the joint of the top cell
     */
    const top_t& anyCojoint(const face_t& f) const
    {
      if(hasCell(f)) return T;
      return top_t::null;
    }

    /**
     * Return a cell for which \c c is a joint
     */
    template <int N1>
    const ncell_t<N1+2>& anyCojoint(const ncell_t<N1>& c) const
    {
      typedef ncell_t<N1+2> l_cell_t;
      const auto& matches = match(Q,Q,Q,c);
      if(not matches.empty())
        return (*matches.begin())->cell;
      return l_cell_t::null;
    }

    //@}

    ///\name Global topological queries
    //@{

    /**
     * True if the N-cell \c c is on the exterior of the cell complex
     */
    bool border(const cell_t& c) const
    {
      for(const face_t& f: bounds(c))
        if(border(f)) return true;
      return false;
    }

    /**
     * True if the face \c f is on the exterior of the cell complex
     */
    bool border(const face_t& f) const
    {
      return hasCell(f) and match(Q,Q,Q,f).empty();
    }

    /**
     * True if the N1-cell \c c is in the boundary of a border face
     */
    template <int N1>
    bool border(const ncell_t<N1>& c) const
    {
      static_assert(N1 < N-1, "Error, border doesn't work with the top cell.");
      for(const face_t& f: cobounds<N-1>(c)) if(border(f))
        return true;
      return false;
    }

    //@}

    ///\name Ordered topological queries
    //@{

    /**
     * Get the oriented neighbors of the cell \c c
     */
    template <int N1>
    Chain<ncell_t<N1>>
    orientedAdjacentCells(const oriented_ncell_t<N1>& oc) const
    {
      typedef ncell_t<N1> l_cell_t;
      typedef ncell_flip_t<N1+1> l_coface_flip_t;

      const l_cell_t& c = ~oc;

      Chain<l_cell_t> result;

      const auto& matches = match(Q,c,Q,Q);
      for(l_coface_flip_t* pf: matches)
      {
        l_cell_t c1 = pf->otherFace(c);
        if(!result.contains(c1))
          result.insert(-oc.orientation() * relativeOrientation(c, pf->joint) * relativeOrientation(c1, pf->joint) * c1);
      }

      return result;
    }

    /**
     * Returns the oriented boundary of a chain
     */
    template <int N1>
    Chain<ncell_t<N1-1>>
    boundary(const Chain<ncell_t<N1>>& chain) const
    {
      typedef oriented_ncell_t<N1> l_oriented_cell_t;
      typedef ncell_t<N1-1> l_face_t;
      Chain<l_face_t> result;
      for(const l_oriented_cell_t& oc: chain)
        result |= boundary(oc);
      return result;
    }

    /**
     * Returns the oriented boundary of an oriented vertex
     */
    Chain<bottom_t> boundary(const typename vertex_t::oriented_cell_t& v) const
    {
      Chain<bottom_t> result;
      if(hasCell(~v))
        result.insert(typename bottom_t::oriented_cell_t(_, v.orientation()));
      return result;
    }

    /**
     * Return the oriented boundary of an oriented cell
     */
    template <int N1>
    Chain<ncell_t<N1-1> >
    boundary(const OrientedObject<ncell_t<N1>>& oc) const
    {
      typedef ncell_t<N1-1> l_face_t;
      Chain<l_face_t> result;
      RelativeOrientation ro = oc.orientation();
      for(const l_face_t& f: bounds(~oc))
        result.insert(ro*relativeOrientation(~oc, f)*f);
      return result;
    }

    /**
     * Get the oriented edge from \c src to \c tgt
     *
     * \returns \c +e if \c e is an edge from \c src to \c tgt, \c -e if \c e is an edge from \c tgt to \c src, a null 
     * edge otherwise.
     */
    oriented_edge_t orientedEdge(const vertex_t& src, const vertex_t& tgt) const
    {
      typedef ncell_flip_t<1> l_cell_flip_t;
      const auto& matches = match(Q, tgt, src, Q);
      if(!matches.empty())
        {
          l_cell_flip_t* pf = *matches.begin();
          if(pf->face1 == tgt)
            return +pf->cell;
          else
            return -pf->cell;
        }
      return oriented_edge_t(edge_t::null, invalid);
    }

    /**
     * Return the ordered list of joints of an oriented N1-cell.
     *
     * The orientation is fixed by a (N1-3)-bound
     */
    template <int N1>
    std::vector<ncell_t<N1-2>> orderedJoints(const OrientedObject<ncell_t<N1>>& oc,
                                             const ncell_t<N1-3>& jj) const
    {
      typedef ncell_flip_t<N1> l_cell_flip_t;
      typedef ncell_t<N1-1> l_face_t;
      typedef ncell_t<N1-2> l_joint_t;

      std::vector<l_joint_t> result;

      for(const l_joint_t& j: cobounds(jj))
      {
        const auto& matches = match(~oc,Q,Q,j);
        if(!matches.empty())
        {
          result.reserve(matches.size());
          l_cell_flip_t* pf = *matches.begin();
          l_face_t prev_face = pf->face1;
          l_face_t next_face = pf->face2;
          if(oc.orientation() != relativeOrientation(pf->joint, jj))
            std::swap(prev_face, next_face);
          l_joint_t cur = pf->joint;
          l_joint_t start(cur);
          do
          {
            result.push_back(cur);
            cur = flip(next_face, cur, jj);
            next_face = flip(~oc, next_face, cur);
          } while(cur != start);
          break;
        }
      }

      return result;
    }

    /**
     * Return the ordered list of cojoints of an oriented N1-cell.
     *
     * The orientation is fixed by a (N1+3)-cobound
     */
    template <int N1>
    std::vector<ncell_t<N1+2>> orderedCojoints(const OrientedObject<ncell_t<N1>>& oj,
                                                              const ncell_t<N1+3>& cc) const
    {
      typedef ncell_flip_t<N1+2> l_cojoint_flip_t;
      typedef ncell_t<N1+1> l_coface_t;
      typedef ncell_t<N1+2> l_cojoint_t;

      std::vector<l_cojoint_t> result;
      for(const l_cojoint_t& cj: bounds(cc))
      {
        const auto& matches = match(cj,Q,Q,~oj);
        if(!matches.empty())
        {
          result.reserve(matches.size());
          l_cojoint_flip_t* pf = *matches.begin();
          l_coface_t prev_face = pf->face2;
          l_coface_t next_face = pf->face1;
          if(oj.orientation() == neg)
            std::swap(prev_face, next_face);
          l_cojoint_t cur = pf->cell;
          l_cojoint_t start(cur);
          do
          {
            result.push_back(cur);
            cur = flip(cc, cur, next_face);
            next_face = flip(cur, next_face, ~oj);
          } while(cur != start);
          break;
        }
      }
      return result;
    }


    /**
     * Return the ordered list of the vertices of an oriented 2-cell
     *
     * The vector is such that two successive vertices define an edge oriented with the 2-cell
     */
    std::vector<vertex_t> orderedVertices(const oriented_ncell_t<2>& of) const
    {
      typedef ncell_flip_t<2> l_cell_flip_t;
      const auto& matches = match(~of,Q,Q,Q);
      std::vector<vertex_t> result(matches.size(), vertex_t(0));
      if(matches.empty()) return result;
      l_cell_flip_t cf = **matches.begin();
      edge_t prev_edge = cf.face1;
      edge_t next_edge = cf.face2;
      if(of.orientation() == neg)
        std::swap(next_edge, prev_edge);
      for(size_type i = 0 ; i < matches.size() ; ++i)
      {
        result[i] = cf.joint;
        const auto& nextMatches = match(cellFlip(~of,next_edge,Q,Q), matches);
        auto fiter = nextMatches.begin();
        if((*fiter)->otherFace(next_edge) == prev_edge) fiter++;
        cf = **fiter;
        prev_edge = next_edge;
        next_edge = cf.otherFace(prev_edge);
      }
      return result;
    }
    /*
    std::vector<vertex_t> orderedVertices(const oriented_ncell_t<2>& of) const
    {
      typedef oriented_ncell_t<2> l_oriented_cell_t;
      typedef ncell_flip_t<2> l_cell_flip_t;
      const auto& matches = match(~of,Q,Q,Q);
      l_cell_flip_t* pf = *matches.begin();
      std::vector<vertex_t> result(matches.size(), vertex_t(0));
      edge_t prev_edge = pf->face1;
      edge_t next_edge = pf->face2;
      if(of.orientation() == neg)
        std::swap(next_edge, prev_edge);
      vertex_t cur = pf->joint;
      for(size_type i = 0 ; i < matches.size() ; ++i)
      {
        result[i] = cur;
        cur = flip(next_edge, cur, _);
        next_edge = flip(~of, next_edge, cur);
      }
      return result;
    }
    */

    /**
     * Return the pair of vertices defining the end points of the edges \c e.
     *
     * The edge is defined by <tt>{+endPoints(e).first, -endPoints(e).second}</tt> or equivalently <tt>{target,
     * source}</tt>
     *
     * If the edge doesn't exist, both are null
     */
    std::pair<vertex_t,vertex_t> orderedVertices(const oriented_edge_t& oe) const
    {
      std::pair<vertex_t,vertex_t> result = { vertex_t(0), vertex_t(0) };
      const auto& m = match(~oe,Q,Q,Q);
      if(!m.empty())
        {
          switch(oe.orientation())
            {
            case pos:
              result.first = (*m.begin())->face1;
              result.second = (*m.begin())->face2;
              break;
            case neg:
              result.first = (*m.begin())->face2;
              result.second = (*m.begin())->face1;
              break;
            default:
              break;
            }
        }
      return result;
    }

    /**
     * Returns the source of the oriented edge \c oe, if any
     */
    const vertex_t& source(const oriented_edge_t& oe) const
    {
      const auto& matches = match(~oe,Q,Q,Q);
      if(not matches.empty())
      {
        switch(oe.orientation())
        {
          case pos:
            return (*matches.begin())->face2;
          case neg:
            return (*matches.begin())->face1;
          default:
            return vertex_t::null;
        }
      }
      else
        return vertex_t::null;
    }

    /**
     * Returns the target of the oriented edge \c oe, if any
     */
    const vertex_t& target(const oriented_edge_t& oe) const
    {
      const auto& matches = match(~oe,Q,Q,Q);
      if(not matches.empty())
      {
        switch(oe.orientation())
        {
          case pos:
            return (*matches.begin())->face1;
          case neg:
            return (*matches.begin())->face2;
          default:
            return vertex_t::null;
        }
      }
      else
        return vertex_t::null;
    }

    /**
     * Return the oriented co-boundary of an oriented cell
     *
     * The co-boundary is the set of cells for which this cell is a face
     */
    Chain<top_t> coboundary(const oriented_cell_t& oc) const
    {
      Chain<top_t> result;
      if(hasCell(~oc))
        result.insert((oc.orientation()*relativeOrientation(T,~oc))*T);
      return result;
    }

    /**
     * Return the co-boundary of the bottom cell
     * \note this is the chain of all the vertices oriented positively
     */
    Chain<vertex_t> coboundary(const bottom_t& ) const
    {
      return +set(vertices());
    }

    /**
     * Return the oriented co-boundary of an oriented cell
     *
     * The co-boundary is the set of cells for which this cell is a face
     */
    template <int N1>
    Chain<ncell_t<N1+1>>
    coboundary(const OrientedObject<ncell_t<N1>>& of) const
    {
      static_assert(N1 >= 0 and N1 < N, "You cannot only ask for the coboundary of a cell of dimension between -1 and N.");
      typedef ncell_t<N1+1> l_cell_t;
      typedef oriented_ncell_t<N1+1> l_oriented_cell_t;
      typedef ncell_flip_t<N1+1> l_cell_flip_t;
      Chain<l_cell_t> result;
      const auto& matches = match(Q,~of,Q,Q);
      RelativeOrientation ro = of.orientation();
      for(l_cell_flip_t* pf: matches)
      {
        l_oriented_cell_t oc = ro*relativeOrientation(pf->cell, ~of)*pf->cell;
        if(!result.contains(oc))
          result.insert(oc);
      }
      return result;
    }

    //@}

    ///\name Edition methods
    //@{

    /**
     * Add a vertex to the cell complex
     *
     * \return The vertex added, or null if it failed (i.e. the vertex is already in the structure)
     */
    vertex_t addVertex(const vertex_t& v = vertex_t::null)
    {
      vertex_t nv(v, true);
      if(get_layer<0>(*this).addVertex(nv)) return nv;
      return vertex_t::null;
    }

    /**
     * Remove a vertex \c v, as long as it's not part of any edge
     */
    bool removeVertex(const vertex_t& v)
    {
      if(!match(Q,v,Q,Q).empty())
        return false;
      return get_layer<0>(*this).removeVertex(v);
    }

    //@}

    ///\name General utility methods
    //@{

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#  define L_CELL_TYPE Cell<N1, typename NTypes<N1,Self>::cell_content_t>
#  define L_FACE_TYPE Cell<N1-1, typename NTypes<N1-1,Self>::cell_content_t>
#  define L_JOINT_TYPE Cell<N1-2, typename NTypes<N1-2,Self>::cell_content_t>
#  define L_QUERY_TYPE QueryType

#  define CELL_FLIP_FCT(N2, CT,FT1,FT2,JT) \
    template <int N1> \
    ncell_flip_t<N1+N2> cellFlip(const CT& c,  \
                                 const FT1& f1,\
                                 const FT2& f2,\
                                 const JT& j) const\
    {                                                                        \
      static_assert((N1+N2) > 0 and (N1+N2) <= N+1, "A flip's dimension must be between 1 and N+1."); \
      return ncell_flip_t<N1+N2>(c, f1, f2, j);        \
    }

    CELL_FLIP_FCT(0, L_CELL_TYPE, L_FACE_TYPE, L_FACE_TYPE, L_JOINT_TYPE)

    CELL_FLIP_FCT(1, L_QUERY_TYPE, L_CELL_TYPE, L_CELL_TYPE, L_FACE_TYPE)
    CELL_FLIP_FCT(0, L_CELL_TYPE, L_QUERY_TYPE, L_FACE_TYPE, L_JOINT_TYPE)
    CELL_FLIP_FCT(0, L_CELL_TYPE, L_FACE_TYPE, L_QUERY_TYPE, L_JOINT_TYPE)
    CELL_FLIP_FCT(0, L_CELL_TYPE, L_FACE_TYPE, L_FACE_TYPE, L_QUERY_TYPE)

    CELL_FLIP_FCT(1, L_QUERY_TYPE, L_QUERY_TYPE, L_CELL_TYPE, L_FACE_TYPE)
    CELL_FLIP_FCT(1, L_QUERY_TYPE, L_CELL_TYPE, L_QUERY_TYPE, L_FACE_TYPE)
    CELL_FLIP_FCT(1, L_QUERY_TYPE, L_CELL_TYPE, L_CELL_TYPE, L_QUERY_TYPE)

    CELL_FLIP_FCT(0, L_CELL_TYPE, L_QUERY_TYPE, L_QUERY_TYPE, L_JOINT_TYPE)
    CELL_FLIP_FCT(0, L_CELL_TYPE, L_QUERY_TYPE, L_FACE_TYPE, L_QUERY_TYPE)

    CELL_FLIP_FCT(0, L_CELL_TYPE, L_FACE_TYPE, L_QUERY_TYPE, L_QUERY_TYPE)

    CELL_FLIP_FCT(0, L_CELL_TYPE, L_QUERY_TYPE, L_QUERY_TYPE, L_QUERY_TYPE)
    CELL_FLIP_FCT(1, L_QUERY_TYPE, L_CELL_TYPE, L_QUERY_TYPE, L_QUERY_TYPE)
    CELL_FLIP_FCT(1, L_QUERY_TYPE, L_QUERY_TYPE, L_CELL_TYPE, L_QUERY_TYPE)
    CELL_FLIP_FCT(2, L_QUERY_TYPE, L_QUERY_TYPE, L_QUERY_TYPE, L_CELL_TYPE)

#  undef L_CELL_TYPE
#  undef L_FACE_TYPE
#  undef L_JOINT_TYPE
#  undef L_QUERY_TYPE
#  undef CELL_FLIP_FCT
#else // DOXYGEN_SHOULD_SKIP_THIS

    /**
     *
     * This function returns a flip with the cell, faces and joint specified. Any one of the argument (but not all) can 
     * be replaced by \c Q, the query value.
     */
    template <int N1>
    ncell_flip_t<N1> cellFlip(const ncell_t<N1>& c, const ncell_t<N1-1>& f1, const ncell_t<N1-1>& f2, const ncell_t<N1-2>& j)
    {
      // Implementation dependent on the arguments
      return 0;
    }

#endif // DOXYGEN_SHOULD_SKIP_THIS

    /**
     * Orient a closed set of cells so it forms a valid boundary.
     *
     * If possible, the orientation of the (closed) boundary defines a N1+1-cell oriented like its neighbors.
     */
    template <int N1>
    Chain<ncell_t<N1>>
    orientCell(Set<ncell_t<N1>> cell) const
    {
      typedef ncell_t<N1> l_face_t;
      typedef ncell_flip_t<N1+1> l_cell_flip_t;
      typedef typename l_face_t::oriented_cell_t l_oriented_face_t;

      l_oriented_face_t ref(l_face_t(0));

      for(const l_face_t& f: cell)
        {
          const auto& m = match(Q,f,Q,Q);
          if(!m.empty())
            {
              l_cell_flip_t* pf = *m.begin();
              ref = -relativeOrientation(pf->cell, f)*f;
            }
        }
      if(!~ref)
        ref = +cell.any();
      return orientCell(std::move(cell), ref);
    }

    /**
     * Orient a closed set of cells so it forms a valid boundary.
     *
     * The orientation is such as the reference is in the boundary
     */
    template <int N1>
    Chain<ncell_t<N1>>
    orientCell(Set<ncell_t<N1>> cell,
               const oriented_ncell_t<N1>& ref) const
    {
      typedef ncell_t<N1> l_face_t;
      typedef ncell_t<N1-1> l_joint_t;
      typedef typename l_face_t::oriented_cell_t l_oriented_face_t;

      Chain<l_face_t> chain(ref);
      cell.erase(~ref);

      std::list<l_oriented_face_t> faces;
      faces.push_back(ref);

      while(!faces.empty())
        {
          l_oriented_face_t of1 = faces.front();
          faces.pop_front();
          Set<l_joint_t> js = bounds(~of1);
          for(const l_joint_t& j: js)
            {
              std::list<l_face_t> to_delete;
              for(const l_face_t& f2: cell)
                {
                  const auto& m = match(f2, j, Q, Q);
                  if(!m.empty())
                    {
                      l_oriented_face_t of2 = -of1.orientation()*relativeOrientation(~of1, j)*relativeOrientation(f2, j)*f2;
                      faces.push_back(of2);
                      chain.insert(of2);
                      to_delete.push_back(f2);
                    }
                }
              for(const l_face_t& f2: to_delete)
                cell.erase(f2);
            }
        }

      if(!cell.empty())
        {
          error = CELL_BOUNDARY_MULTIPLE_PARTS;
          chain.clear();
          return chain;
        }
      return chain;
    }

    /**
     * \overload
     */
    template <int N1>
    Chain<ncell_t<N1>>
    orientCell(std::initializer_list<ncell_t<N1>> cell) const
    {
      return orientCell(Set<ncell_t<N1>>(cell));
    }

    /**
     * \overload
     */
    template <int N1>
    Chain<ncell_t<N1>>
    orientCell(std::initializer_list<ncell_t<N1>> cell,
               const oriented_ncell_t<N1>& ref) const
    {
      return orientCell(Set<ncell_t<N1>>(cell), ref);
    }


    /**
     * Check the boundary, and provide hooks to gather information
     * \param cell chain describing the cell to check
     * \param face_fct hook that gets an oriented face as argument: called once per face in the cell
     * \param joint_fct hook that gets an oriented joint. It's always called after face_fct for the face containing the 
     * joint. The orientation of the joint depend on the orientation of the face.
     */
    template <typename CellType, typename FaceFctType, typename JointFctType>
    bool checkLayer(const Chain<CellType>& cell,
                    const FaceFctType& face_fct,
                    const JointFctType& joint_fct)
    {
      typedef CellType l_face_t;
      typedef ncell_t<l_face_t::N-1> l_joint_t;
      typedef ncell_t<l_face_t::N+1> l_cell_t;
      typedef typename l_face_t::oriented_cell_t l_oriented_face_t;
      typedef typename l_joint_t::oriented_cell_t l_oriented_joint_t;
      Chain<l_joint_t> global_boundary;
      std::unordered_set<l_oriented_joint_t> used_joints; // Check unicity of ordered joint
      // Check the number of groups of joints, to ensure there is only one loop
      std::unordered_map<l_joint_t,size_type> joint_groups;
      std::vector<size_type> joint_groups_equ;
      // First construct all the local (non-oriented) boundaries, and store their orientation on the side
      // Also accumulate the boundary of the boundary ... to check the boundary of the new cell is closed
      for(const l_oriented_face_t& oc: cell)
        {
          if(!face_fct(oc)) return false;
          const Chain<l_joint_t>& b1 = boundary(oc);
          if(b1.empty())
            {
              error = NO_SUCH_FACE;
              return l_cell_t::null;
            }
          size_type cur_group = joint_groups_equ.size();
          joint_groups_equ.push_back(cur_group);
          for(const l_oriented_joint_t& oj: b1)
            {
              if(!used_joints.insert(oj).second) // The element is already there
                {
                  error = CELL_BOUNDARY_SELF_INTERSECT;
                  return l_cell_t::null;
                }
              auto found = joint_groups.find(~oj);
              if(found != joint_groups.end())
                {
                  size_type new_group = joint_groups_equ[found->second];
                  if(new_group > cur_group)
                    joint_groups_equ[new_group] = cur_group;
                  else
                    {
                      joint_groups_equ[cur_group] = new_group;
                      cur_group = new_group;
                    }
                }
              else
                {
                  joint_groups[~oj] = cur_group;
                }
              if(!joint_fct(oc, oj)) return false;
            }
          global_boundary |= b1;
        }
      if(!global_boundary.empty())
        {
          DEBUG_OUT("Boundary of the boundary: " << global_boundary << endl);
          error = CELL_BOUNDARY_NOT_CLOSED;
          return l_cell_t::null;
        }
      // Now, collapse the joint groups
      {
        for(size_type& grp: joint_groups_equ)
        {
          grp = joint_groups_equ[grp];
          if(grp != 0)
            {
              error = CELL_BOUNDARY_MULTIPLE_PARTS;
              return l_cell_t::null;
            }
        }
      }
      return true;
    }

    /**
     * Display all the flips and vertices of this cell complex
     */
    void displayFlips() const
    {
      out << "Dimension of the Cell Complex: " << N << endl;
      _displayFlips<N+1>();
      out << endl;
    }

    //@}

    /**
     * \name Atomic edition operations
     * These methods should be used with care, as they do not enforce the main invariants of the data structure
     */
    //@{
    /**
     * Add a flip directly
     */
    template <int N1>
    ncell_flip_t<N1>* addFlip(const ncell_flip_t<N1>& flip)
    {
      static_assert(N1>0 and N1 <= N+1, "Flips must be of dimension ranging from 1 to N+1.");
      typedef ncell_flip_t<N1> l_cell_flip_t;
      l_cell_flip_t* pf = new l_cell_flip_t(flip);
      if(addFlip(pf)) return pf;
      error = CANNOT_INSERT_FLIP;
      return 0;
    }

    /// \overload
    template <int N1>
    ncell_flip_t<N1>* addFlip(const ncell_t<N1>& cell,
                              const ncell_t<N1-1>& f1, const ncell_t<N1-1>& f2,
                              const ncell_t<N1-2>& j)
    {
      typedef ncell_flip_t<N1> l_cell_flip_t;
      return addFlip(l_cell_flip_t(cell, f1, f2, j));
    }


    /**
     * Add a flip directly.
     *
     * \note The cell complex takes ownership of the pointer whether it succeeds or not!
     */
    template <int N1>
    bool addFlip(ncell_flip_t<N1>* flip)
    {
      static_assert(N1>0 and N1 <= N+1, "Flips must be of dimension ranging from 1 to N+1.");
      return get_layer<N1>(*this).addFlip(flip);
    }

    /**
     * Change the face of an existing flip
     *
     * \param flip Pointer to the flip to change
     * \param new_face New face
     * \param old_face Face to change (by default: exterior)
     *
     * \returns True on success
     */
    template <typename CellFlipType>
    bool updateFlip(CellFlipType* flip,
                    const typename CellFlipType::face_t& new_face,
                    const typename CellFlipType::face_t& old_face = CellFlipType::face_t::exterior)
    {
      return get_layer<CellFlipType::N>(*this).updateFlip(flip, new_face, old_face);
    }

    /**
     * Change the cell of an existing flip
     *
     * \param flip Pointer to the flip to change
     * \param new_cell New cell
     *
     * \returns True on success
     */
    template <typename CellFlipType>
    bool updateFlip(CellFlipType* flip,
                    const typename CellFlipType::cell_t& new_cell)
    {
      return get_layer<CellFlipType::N>(*this).updateFlip(flip, new_cell);
    }

    /**
     * Change the joint of an existing flip
     *
     * \param flip Pointer to the flip to change
     * \param new_cell New joint
     *
     * \returns True on success
     */
    template <typename CellFlipType>
    bool updateFlip(CellFlipType* flip,
                    const typename CellFlipType::joint_t& new_joint)
    {
      return get_layer<CellFlipType::N>(*this).updateFlip(flip, new_joint);
    }


    /**
     * Remove the orientation between a cell and all its bounds/cobounds
     */
    template <int N1>
    bool removeCellOrientations(const ncell_t<N1>& cell)
    {
      typedef ncell_t<N1-1> face_t;
      typedef ncell_t<N1+1> coface_t;
      const Set<face_t>& faces = this->bounds(cell);
      const Set<coface_t>& cofaces = this->cobounds(cell);
      auto& b1 = get_layer<N1>(*this);
      auto& b2 = get_layer<N1+1>(*this);
      bool result = true;
      for(const face_t& f: faces)
        result &= b1.removeRelativeOrientation(cell, f);
      for(const coface_t& f: cofaces)
        result &= b2.removeRelativeOrientation(f, cell);
      return result;
    }

    /**
     * Set the orientation between a cell and a set of bounds
     */
    template <int N1>
    bool setCellOrientations(const ncell_t<N1>& cell,
                             const Chain<ncell_t<N1-1>>& bounds)
    {
      typedef oriented_ncell_t<N1-1> l_oriented_face_t;
      typedef  ComplexNthLayer<N1,Self> l_layer_t;
      l_layer_t& layer = get_layer<N1>(*this);
      bool success = true;
      for(const l_oriented_face_t& of: bounds)
        success &= layer.setRelativeOrientation(cell, ~of, of.orientation());
      return success;
    }

    /**
     * Set the orientation between a cell and a face
     */
    template <int N1>
    bool setRelativeOrientation(const ncell_t<N1>& cell,
                                const ncell_t<N1-1>& f,
                                const RelativeOrientation& ro)
    {
      return get_layer<N1>(*this).setRelativeOrientation(cell, f, ro);
    }

    /**
     * \overload
     */
    template <int N1>
    bool setRelativeOrientation(const ncell_t<N1>& cell,
                                const OrientedObject<ncell_t<N1-1>>& of)
    {
      return setRelativeOrientation(cell, ~of, of.orientation());
    }

    /**
     * Remove the relative orientation between \c cell and \c face
     */
    template <int N1>
    bool removeRelativeOrientation(const ncell_t<N1>& cell,
                                   const ncell_t<N1-1>& face)
    {
      return get_layer<N1>(*this).removeRelativeOrientation(cell, face);
    }

    /**
     * Remove any relative orientation
     */
    void clearRelativeOrientations()
    {
      _clearRelativeOrientations(IntConstant<N>());
    }

    /**
     * Remove a flip
     */
    template <int N1>
    bool removeFlip(const ncell_flip_t<N1>& flip)
    {
      static_assert(N1>0 and N1 <= N+1, "Flips must be of dimension ranging from 1 to N+1.");
      const auto& found = match(flip);
      if(found.empty())
      {
        error = NO_SUCH_FLIP;
        return false;
      }
      return get_layer<N1>(*this).removeFlip(*found.begin());
    }

    /**
     * Remove (and delete) a flip
     */
    template <int N1>
    bool removeFlip(ncell_flip_t<N1>* flip)
    {
      static_assert(N1>0 and N1 <= N+1, "Flips must be of dimension ranging from 1 to N+1.");
      return get_layer<N1>(*this).removeFlip(flip);
    }
    //@}

    //@{
    ///\name Orientation queries

    /// Relative orientation of top cells are always true
    RelativeOrientation relativeOrientation(const top_t& ,
                                            const cell_t& ) const
    {
      return pos;
    }

    /// Relative orientation of bottom cells are always true
    RelativeOrientation relativeOrientation(const vertex_t& ,
                                            const bottom_t& ) const
    {
      return pos;
    }

    /// Relative orientation between \c c and \c f
    template <int N1>
    RelativeOrientation relativeOrientation(const ncell_t<N1>& c,
                                            const ncell_t<N1-1>& f) const
    {
      return get_layer<N1>(*this).relativeOrientation(c, f);
    }

    /// Relative orientation of two cells, neighbors or co-neighbors
    template <int N1>
    RelativeOrientation relativeOrientation(const ncell_t<N1>& c1,
                                            const ncell_t<N1>& c2) const
    {
      /// First, try to find a common face
      auto faces_c1 = this->faces(c1);
      auto faces_c2 = this->faces(c2);
      faces_c1 &= faces_c2;
      if(not faces_c1.empty())
      {
        auto f = *begin(faces_c1);
        return -this->relativeOrientation(c1, f) * this->relativeOrientation(c2, f);
      }
      auto cofaces_c1 = this->cofaces(c1);
      auto cofaces_c2 = this->cofaces(c2);
      cofaces_c1 &= cofaces_c2;
      if(not cofaces_c1.empty())
      {
        auto cf = *begin(cofaces_c1);
        return this->relativeOrientation(cf, c1) * this->relativeOrientation(cf, c2);
      }
      return invalid; // Cells are neither neighbors or co-neighbors
    }

    /// Relative orientation of two vertices is always the same as adjacent orientation
    RelativeOrientation relativeOrientation(const vertex_t& v1, const vertex_t& v2) const
    {
      if(hasCell(v1) and hasCell(v2))
        return neg;
      return invalid;
    }

    /// Relative orientation of two cell is always the same as adjacent orientation
    RelativeOrientation relativeOrientation(const cell_t& c1, const cell_t& c2) const
    {
      if(hasCell(c1) and hasCell(c2))
        return pos;
      return invalid;
    }

    /// Relative orientation of two oriented cells
    template <int N1, int N2>
    RelativeOrientation relativeOrientation(const oriented_ncell_t<N1>& c1, const oriented_ncell_t<N2>& c2) const
    {
      return c1.orientation()*c2.orientation()*relativeOrientation(~c1, ~c2);
    }

    /// Relative orientation of two adjacent cells
    template <int N1>
    RelativeOrientation adjacentOrientation(const ncell_t<N1>& c1,
                                            const ncell_t<N1>& c2) const
    {
      for(auto pfl: this->match(Q, c1, c2, Q))
      {
        auto f = pfl->joint;
        return -this->relativeOrientation(c1, f) * this->relativeOrientation(c2, f);
      }
      return invalid; // The cells are not adjacent!
    }

    /// Relative orientation of two adjacent oriented cells
    template <int N1>
    RelativeOrientation adjacentOrientation(const oriented_ncell_t<N1>& c1,
                                            const oriented_ncell_t<N1>& c2) const
    {
      return c1.orientation() * c2.orientation() * adjacentOrientation(~c1, ~c2);
    }

    /// If they are neighbors, two vertices always have opposite orientation
    RelativeOrientation adjacentOrientation(const vertex_t& v1, const vertex_t& v2) const
    {
      if(this->match(Q, v1, v2, Q).empty())
        return invalid;
      return neg;
    }

    /// If they are neighbors, two cells always have same orientation
    RelativeOrientation adjacentOrientation(const cell_t& c1, const cell_t& c2) const
    {
      if(this->match(Q, c1, c2, Q).empty())
        return invalid;
      return pos;
    }
    //@}


  protected:
    ///\name Unordered lookup implementation
    //@{
    /**
     * Returns the bounds of the cell \c c
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1+1)> = 0>
    Set<ncell_t<N1>>
    _bounds(const ncell_t<N2>& c) const
    {
      return bounds(c);
    }

    /**
     * Returns the joints of the cell \c c
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1+2)> = 0>
    Set<ncell_t<N1>>
    _bounds(const ncell_t<N2>& c) const
    {
      return joints(c);
    }

    /**
     * Returns the bounds of the cell \c c if there is a difference of more than 2
     */
    template <int N1, int N2,
              EnableIf<(N2 > N1+2)> = 0>
    Set<ncell_t<N1>>
    _bounds(const ncell_t<N2>& c) const
    {
      typedef ncell_t<N2-2> l_joint_t;
      typedef ncell_t<N1> l_target_t;
      Set<l_target_t> result;
      for(const l_joint_t& j: joints(c))
        result |= bounds<N1>(j);
      return result;
    }

    /**
     * Return the co-bounds of the cell \c c
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1-1)> = 0>
    Set<ncell_t<N1>>
    _cobounds(const ncell_t<N2>& c) const
    {
      return cobounds(c);
    }

    /**
     * Return the co-joints of the cell \c c
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1-2)> = 0>
    Set<ncell_t<N1>>
    _cobounds(const ncell_t<N2>& c) const
    {
      return cojoints(c);
    }

    /**
     * Return the N1-cobounds of the cell \c c if the difference is at least 3
     */
    template <int N1, int N2, 
              EnableIf<(N2 < N1-2)> = 0>
    Set<ncell_t<N1>>
    _cobounds(const ncell_t<N2>& c) const
    {
      typedef ncell_t<N2+2> l_cell_t;
      typedef ncell_t<N1> l_target_t;
      Set<l_target_t> result;
      for(const l_cell_t& v: cojoints(c))
        result |= cobounds<N1>(v);
      return result;
    }


    /**
     * Return the co-bounds of the cell \c c
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1-1) and (N2 <= N)> = 0>
    const ncell_t<N1>&
    _anycobound(const ncell_t<N2>& c) const
    {
      return anyCobound(c);
    }

    /**
     * Return the co-joints of the cell \c c
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1-2) and (N2 <= N)> = 0>
    const ncell_t<N1>&
    _anycobound(const ncell_t<N2>& c) const
    {
      return anyCojoint(c);
    }

    /**
     * Return the N1-cobounds of the cell \c c if the difference is at least 3
     */
    template <int N1, int N2,
              EnableIf<(N2 < N1-2) and (N2 <= N)> = 0>
    const ncell_t<N1>&
    _anycobound(const ncell_t<N2>& c) const
    {
      return anyCobound<N1>(anyCojoint(c));
    }

    template <int N1, int N2,
              EnableIf<(N2 == N+1)> = 0>
    const ncell_t<N1>&
    _anycobound(const ncell_t<N2>& c) const
    {
      if(hasCell(c)) return T;
      return top_t::null;
    }


    /**
     * Check that \c c2 is a bound of \c c1
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1-1) and (N2 > -1)> = 0>
    bool _isBound(const ncell_t<N1>& c1,
                  const ncell_t<N2>& c2) const
    {
      return not match(c1, c2, Q, Q).empty();
    }

    /**
     * Check that \c c2 is a joint of \c c1
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1-2) and (N2 > -1)> = 0>
    bool _isBound(const ncell_t<N1>& c1,
                  const ncell_t<N2>& c2) const
    {
      return not match(c1, Q, Q, c2).empty();
    }

    /**
     * Check that \c c2 is in the boundary of a joint of \c c1
     */
    template <int N1, int N2, 
              EnableIf<(N2 < N1-2) and (N2 > -1)> = 0>
    bool _isBound(const ncell_t<N1>& c1,
                  const ncell_t<N2>& c2) const
    {
      typedef ncell_t<N1-2> l_joint_t;
      Set<l_joint_t> js = joints(c1);
      for(const l_joint_t& j: js)
        if(isBound(j, c2)) return true;
      return false;
    }

    template <int N1, int N2,
              EnableIf<(N2 == -1)> = 0>
    bool _isBound(const ncell_t<N1>& c1,
                  const ncell_t<N2>& c2) const
    {
      return hasCell(c1) and hasCell(c2);
    }

    /**
     * Returns a bound of \c c
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1+1) and (N1 > -1)> = 0>
    const ncell_t<N1>&
    _anybounds(const ncell_t<N2>& c) const
    {
      return anyBound(c);
    }

    /**
     * Returns a joint of \c c
     */
    template <int N1, int N2,
              EnableIf<(N2 == N1+2) and (N1 > -1)> = 0>
    const ncell_t<N1>&
    _anybounds(const ncell_t<N2>& c) const
    {
      return anyJoint(c);
    }

    /**
     * Returns a bounds of a joint of \c c
     */
    template <int N1, int N2,
             EnableIf<(N2 > N1+2) and (N1 > -1)> = 0>
    const ncell_t<N1>&
    _anybounds(const ncell_t<N2>& c) const
    {
      return anyBound<N1>(anyJoint(c));
    }

    template <int N1, int N2, 
              EnableIf<(N1 == -1)> = 0>
    const ncell_t<N1>&
    _anybounds(const ncell_t<N2>& c) const
    {
      if(hasCell(c)) return _;
      return bottom_t::null;
    }

    //@}


    ///\name General utility implementation methods
    //@{

    template <int N1,
              EnableIf<(N1 >= 0)> = 0>
    void _displayFlips() const
    {
      get_layer<N1>(*this).displayFlips();
      _displayFlips<N1-1>();
    }

    template <int N1,
              EnableIf<(N1 < 0)> = 0>
    void _displayFlips() const
    {
    }
    //@}

    void _clearRelativeOrientations(const IntConstant<0>&)
    { } // nothing to do

    template <int N1>
    void _clearRelativeOrientations(const IntConstant<N1>&)
    {
      get_layer<N1>(*this).clearRelativeOrientations();
      _clearRelativeOrientations(IntConstant<N1-1>());
    }

  };

  // Define the dimension of a cell complex
  template <typename V, typename... Cs>
  struct dimension<CellComplex<V,Cs...> >
  {
    typedef CellComplex<V,Cs...> type;
    enum
    {
      N = type::N
    };
  };

  /**
   * Type of a cell of dimension N1
   */
  template <int N1, typename Complex>
  using ncell_t = Cell<N1,typename NTypes<N1,Complex>::cell_content_t>;

  /**
   * Type of an oriented cell of dimension N1
   */
  template <int N1, typename Complex>
  using oriented_ncell_t = OrientedObject<Cell<N1,typename NTypes<N1,Complex>::cell_content_t> >;

  /**
   * Type of a cell flip of dimension N1
   */
  template <int N1, typename Complex>
  using ncell_flip_t = CellFlip<N1,
                                typename cellflips::NTypes<N1,Complex>::cell_content_t,
                                typename cellflips::NTypes<N1-1,Complex>::cell_content_t,
                                typename cellflips::NTypes<N1-2,Complex>::cell_content_t>;

  /**
   * Type of a cell range of dimension N1
   *
   * \note This type cannot be use if inference on N1 is needed
   */
  template <typename Complex, int N1>
  using ncell_range = typename NTypes<N1,Complex>::cell_range;


  template <TEMPLATE_CELL_COMPLEX>
  const typename CellComplex<CELL_COMPLEX_ARGS>::top_t CellComplex<CELL_COMPLEX_ARGS>::T = CellComplex<CELL_COMPLEX_ARGS>::top_t::exterior;

  template <TEMPLATE_CELL_COMPLEX>
  const typename CellComplex<CELL_COMPLEX_ARGS>::bottom_t CellComplex<CELL_COMPLEX_ARGS>::_;

#undef TEMPLATE_CELL_COMPLEX
#undef CELL_COMPLEX_ARGS
}

#endif // CELLFLIP_H

