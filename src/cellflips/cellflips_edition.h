#ifndef CELLFLIPS_EDITION_H
#define CELLFLIPS_EDITION_H

#include <list>
#include <unordered_map>
#include "cellflips/cellflips.h"
#include "cellflips/chain.h"

namespace cellflips
{

template <typename Complex, int N1>
bool removeCell(Complex& C, const ncell_t<N1,Complex>& c);

namespace detail
{

template <typename Container>
struct CellContainerDim
{
  typedef typename Container::value_type::cell_t cell_t;
  static const int N = cell_t::N;
};

template <int N1, typename Complex,
          EnableIf<(Complex::N == N1)> = 0>
bool addTopFlips(Complex& C,
                 const ncell_t<N1,Complex>& c,
                 const oriented_ncell_t<N1-1,Complex>& of,
                 std::list<ncell_flip_t< N1+1,Complex>>& top_flips)
{
  typedef typename Complex::cell_t cell_t;
  QueryType Q;
  if(!C.match(Q,Q,Q,~of).empty())
  {
    C.error = Complex::FACE_ALREADY_BETWEEN_CELLS;
    return false;
  }
  for(const cell_t& c1: C.cobounds(~of))
  {
    auto fl = C.cellFlip(C.T, Q, Q, ~of);
    if(of.orientation() == pos)
    {
      fl.face1 = c;
      fl.face2 = c1;
    }
    else
    {
      fl.face1 = c1;
      fl.face2 = c;
    }
    top_flips.push_back(fl);
  }
  return true;
}

template <int N1, typename Complex,
          EnableIf<(Complex::N != N1)> = 0>
bool addTopFlips(Complex&,
                 const ncell_t<N1,Complex>&,
                 const oriented_ncell_t<N1-1,Complex>&,
                 std::list<ncell_flip_t< N1+1,Complex>>&)
{ return true; }


template <typename Complex, int N1>
bool addCellChecks(Complex& C,
                   const Chain<ncell_t<N1,Complex>>& cell,
                   const ncell_t<N1+1,Complex>& c,
                   std::unordered_map<ncell_t<N1-1,Complex>,ncell_flip_t<N1+1,Complex>>& flips,
                   std::list<ncell_flip_t<N1+2,Complex>>& top_flips,
                   bool link_to_top)
{
  QueryType Q;
  typedef typename Complex::size_type size_type;
  typedef ncell_t<N1,Complex> l_face_t;

  typedef ncell_flip_t<N1+1,Complex> l_cell_flip_t;

  // Data structure to check the connectivity of the surface
  std::unordered_map<l_face_t,size_type> group_face;
  std::vector<size_type> group_equiv;
  group_equiv.reserve(cell.size());

  // For all oriented face
  for(const auto& of: cell)
  {
    if(!C.hasCell(~of)) // Check the face exists
    {
      C.error = Complex::NO_SUCH_FACE;
      return false;
    }
    {
      group_face[~of] = group_equiv.size();
      group_equiv.push_back(group_equiv.size());
    }
    // Then, for all joint
    for(const auto& oj: C.boundary(of))
    {
      auto found = flips.find(~oj);
      if(found == flips.end())
      {
        // If this is a new joint
        if(oj.orientation() == pos)
          flips.insert({~oj, C.cellFlip(c,~of,Q,~oj)});
        else
          flips.insert({~oj, C.cellFlip(c,Q,~of,~oj)});
      }
      else
      {
        // If we are completing an existing flip
        l_cell_flip_t& flip = found->second;
        if(oj.orientation() == pos)
        {
          if(flip.face1)
          {
            C.error = Complex::WRONG_BOUNDARY_ORIENTATION;
            return false;
          }
          flip.face1 = ~of;
        }
        else
        {
          if(flip.face2)
          {
            C.error = Complex::WRONG_BOUNDARY_ORIENTATION;
            return false;
          }
          flip.face2 = ~of;
        }
        // The two faces are connected, so store this
        size_type g1 = 0, g2 = 0;
        auto f1 = group_face.find(flip.face1);
        g1 = group_equiv[f1->second];
        while(g1 != group_equiv[g1]) g1 = group_equiv[g1];
        auto f2 = group_face.find(flip.face2);
        g2 = group_equiv[f2->second];
        while(g2 != group_equiv[g2]) g2 = group_equiv[g2];
        size_type g = std::min(g1, g2);
#ifdef DEBUG_OUTPUT
        out << g1 << "[" << f1->second << "] <=> "
          << g2 << "[" << f2->second << "] => "
          << (g < g1 ? QString::number(g1) : QString()) << "/"
          << (g < g2 ? QString::number(g2) : QString()) << " -> "
          << g << endl;
#endif
        if(g < g1)
          group_equiv[g1] = g;
        if(g < g2)
          group_equiv[g2] = g;
        f1->second = g;
        f2->second = g;
      }
    }
    if(link_to_top and !addTopFlips(C, c, of, top_flips))
      return false;
  }

  // Compact the groups
  for(size_type i = 0 ; i < group_equiv.size() ; ++i)
  {
    size_type g = group_equiv[group_equiv[i]];
    if(g > 0)
    {
      C.error = Complex::CELL_BOUNDARY_MULTIPLE_PARTS;
#ifdef DEBUG_OUTPUT
      out << "groups:";
      for(int i = 0 ; i < group_equiv.size() ; ++i)
        out << " " << i << ":" << group_equiv[i];
      out << endl;
#endif
      return false;
    }
    group_equiv[i] = g;
  }

  // Now, check all the flips are complete
  for(const auto& p: flips)
  {
    if(!p.second)
    {
      C.error = Complex::CELL_BOUNDARY_NOT_CLOSED;
      return false;
    }
  }
  return true;
}

// Remove a 'top cell', i.e. a cell not part of another cell
template <typename Complex, int N1>
bool removeTopCell(Complex& C, const ncell_t<N1,Complex>& c)
{
  QueryType Q;
  typedef ncell_flip_t< N1,Complex> l_cell_flip_t;
  typedef ncell_t<N1-1,Complex> l_face_t;
  if(!C.match(Q,c,Q,Q).empty())
  {
    C.error = Complex::CELL_IS_A_FACE;
    return false;
  }
  Set<l_face_t> faces;
  auto matches = C.match(c, Q, Q, Q);
  if(matches.empty())
  {
    C.error = Complex::NO_SUCH_CELL;
    return false;
  }

  for(l_cell_flip_t* pf: matches)
  {
    faces.insert(pf->face1);
    faces.insert(pf->face2);
    C.removeFlip(pf);
  }

  for(const l_face_t& f: faces)
    if(C.match(Q,f,Q,Q).empty())
      if(!cellflips::removeCell(C, f))
        return false;

  return true;
}


template <typename Complex, int N1,
          EnableIf<(Complex::N == N1)> = 0>
bool removeCell(Complex& C, const ncell_t<N1,Complex>& c)
{
  // First, copy the set of flips
  QueryType Q;
  auto matches = C.match(Q,c,Q,Q);
  // To remove them all
  for(auto pf: matches)
    C.removeFlip(pf);
  // Call explicitly the template version
  return removeTopCell(C, c);
}

template <typename Complex, int N1,
          EnableIf<(Complex::N > N1) and (N1 > -1)> = 0>
bool removeCell(Complex& C, const ncell_t<N1,Complex>& c)
{
  return removeTopCell(C, c);
}

/**
 * Remove a 0-cell (i.e. a vertex)
 *
 * The vertex must not be connected to any edge to be removable.
 */
template <typename Complex>
bool removeCell(Complex& C, const ncell_t<0,Complex>& v)
{
  return C.removeVertex(v);
}

template <typename Complex>
void splitEdgeNeighbors(Complex& C,
                        const typename Complex::edge_t& e,
                        const typename Complex::vertex_t& v,
                        const typename Complex::edge_t& e1,
                        const typename Complex::vertex_t& v1,
                        const typename Complex::edge_t& e2)
{
  QueryType Q;
  typedef ncell_flip_t<2,Complex> l_cell_flip_t;
  auto m_neighbors1 = C.match(Q,e,Q,v1);
  for(l_cell_flip_t* pf: m_neighbors1)
  {
    l_cell_flip_t ct1 = *pf;
    l_cell_flip_t ct2 = *pf;
    ct2.joint = v;
    ct2.face1 = e2;
    ct2.face2 = e1;

    if(pf->face1 == e)
      ct1.face1 = e1;
    else
    {
      ct1.face2 = e1;
      ct2.reverse();
    }

    C.removeFlip(pf);

    C.addFlip(ct1);
    if(e2)
      C.addFlip(ct2);
  }
}

template <typename Complex, int N1,
          EnableIf<(N1 < Complex::N)> = 0>
void splitVolume(Complex& C,
                 const ncell_t<N1,Complex>& c,
                 const ncell_t<N1,Complex>& c1,
                 const ncell_t<N1,Complex>& c2)
{
  typedef ncell_flip_t<N1+2,Complex> l_cell_flip_t;
  QueryType Q;
  const auto& matches = C.match(Q,Q,Q,c);

  std::list<l_cell_flip_t> to_add;
  std::list<l_cell_flip_t*> to_remove;

  for(l_cell_flip_t* pf: matches)
  {
    l_cell_flip_t cf1 = *pf;
    l_cell_flip_t cf2 = *pf;

    cf1.joint = c1;
    cf2.joint = c2;

    to_remove.push_back(pf);
    to_add.push_back(cf1);
    to_add.push_back(cf2);
  }

  for(l_cell_flip_t* pf: to_remove)
    C.removeFlip(pf);

  for(const l_cell_flip_t& cf: to_add)
    C.addFlip(cf);
}

template <typename Complex, int N1,
          EnableIf<(N1 >= Complex::N)> = 0>
void splitVolume(Complex&,
                 const ncell_t<N1,Complex>& ,
                 const ncell_t<N1,Complex>& ,
                 const ncell_t<N1,Complex>&)
{
}

template <typename Complex, int N1>
void addMembraneOrientations(Complex& C,
                             const ncell_t<N1,Complex>& m,
                             const Chain<ncell_t<N1-1,Complex>>& mBoundary,
                             const ncell_t<N1+1,Complex>& cl,
                             const ncell_t<N1+1,Complex>& cr)
{
  typedef oriented_ncell_t<N1-1,Complex> l_oriented_face_t;
  for(const l_oriented_face_t& of: mBoundary)
  {
    C.setRelativeOrientation(m, ~of, of.orientation());
  }
  C.setRelativeOrientation(cl, m, pos);
  C.setRelativeOrientation(cr, m, neg);
}

template <typename Complex, int N1,
          EnableIf<(Complex::N == N1)> = 0>
void updateTopRelativeOrientations(Complex&,
                                   const ncell_t<N1,Complex>&,
                                   const ncell_t<N1,Complex>&,
                                   const ncell_t<N1,Complex>&)
{
  // Nothing to do here!
}

template <typename Complex, int N1,
          EnableIf<(Complex::N > N1)> = 0>
void updateTopRelativeOrientations(Complex& C,
                                   const ncell_t<N1,Complex>& cell,
                                   const ncell_t<N1,Complex>& cl,
                                   const ncell_t<N1,Complex>& cr)
{
  typedef ncell_t<N1+1,Complex> l_volume_t;
  typedef oriented_ncell_t<N1+1,Complex> l_oriented_volume_t;
  Chain<l_volume_t> cb = C.coboundary(+cell);
  for(const l_oriented_volume_t& ov: cb)
  {
    C.setRelativeOrientation(~ov, cl, ov.orientation());
    C.setRelativeOrientation(~ov, cr, ov.orientation());
  }
}

template <typename Complex, int N1>
void updateRelativeOrientations(Complex& C,
                                const ncell_t<N1,Complex>& cell,
                                const ncell_t<N1,Complex>& cl,
                                const Set<ncell_t<N1-1,Complex>>& cell_l,
                                const ncell_t<N1,Complex>& cr,
                                const Set<ncell_t<N1-1,Complex>>& cell_r)
{
  typedef ncell_t<N1-1,Complex> l_face_t;

  for(const l_face_t& f: cell_l)
  {
    C.setRelativeOrientation(cl, f, C.relativeOrientation(cell, f));
  }

  for(const l_face_t& f: cell_r)
  {
    C.setRelativeOrientation(cr, f, C.relativeOrientation(cell, f));
  }

  updateTopRelativeOrientations(C, cell, cl, cr);
}

template <typename Complex, int N1,
          EnableIf<(N1 == Complex::N)> = 0>
void updateSTopFlips(Complex&,
                     const ncell_t<N1,Complex>&,
                     const ncell_t<N1,Complex>&,
                     const ncell_t<N1,Complex>&)
{
  // Nothing to do here!
}

template <typename Complex, int N1,
          EnableIf<(N1 < Complex::N)> = 0>
void updateSTopFlips(Complex& C,
                     const ncell_t<N1,Complex>& c,
                     const ncell_t<N1,Complex>& cl,
                     const ncell_t<N1,Complex>& cr)
{
  typedef ncell_flip_t<N1+2,Complex> l_cojoint_flip_t;
  QueryType Q;
  auto matches = C.match(Q,Q,Q,c);
  for(l_cojoint_flip_t* pf: matches)
  {
    l_cojoint_flip_t cf1 = *pf;
    l_cojoint_flip_t cf2 = *pf;
    cf1.joint = cl;
    cf2.joint = cr;

    C.removeFlip(pf);
    C.addFlip(cf1);
    C.addFlip(cf2);
  }
}

template <typename Complex, typename MatchType, int N1>
std::pair<util::set_vector<ncell_t<N1-1,Complex>>, util::set_vector<ncell_t<N1-1,Complex>> >
splitBoundary(const Complex& C,
              const MatchType& matches,
              const Chain<ncell_t<N1-2,Complex>>& mBoundary)
{
  typedef ncell_t<N1-1,Complex> l_face_t;
  typedef oriented_ncell_t<N1-2,Complex> l_oriented_joint_t;
  typedef oriented_ncell_t<N1-1,Complex> l_oriented_face_t;
  typedef typename Complex::size_type size_type;

  std::pair<util::set_vector<l_face_t>, util::set_vector<l_face_t> > result;

  std::unordered_map<l_face_t,size_type> face_group;
  face_group.reserve(matches.size());
  std::vector<size_type> group_eqs;
  group_eqs.reserve(matches.size());

  for(auto pct: matches)
  {
    if(!mBoundary.contains(pct->joint))
    {
      const l_face_t& f1 = pct->face1;
      const l_face_t& f2 = pct->face2;
      auto found1 = face_group.find(f1);
      auto found2 = face_group.find(f2);
      if(found1 == face_group.end() and
         found2 == face_group.end())
      {
        size_type gr = group_eqs.size();
        face_group[f1] = gr;
        face_group[f2] = gr;
        group_eqs.push_back(gr);
      }
      else if(found1 == face_group.end())
        face_group[f1] = found2->second;
      else if(found2 == face_group.end())
        face_group[f2] = found1->second;
      else
      {
        size_type& g1 = found1->second;
        size_type& g2 = found2->second;
        if(g2 < g1)
        {
          group_eqs[g1] = g2;
          g1 = g2;
        }
        else
        {
          group_eqs[g2] = g1;
          g2 = g1;
        }
      }
    }
  }

  // Now, compact the groups
  size_type g1, g2;
  bool fg1 = false, fg2 = false;
  for(size_type i = 0 ; i < group_eqs.size() ; ++i)
  {
    size_type cg = group_eqs[group_eqs[i]]; 
    group_eqs[i] = cg;
    if(!fg1)
    {
      fg1 = true;
      g1 = cg;
    }
    else if(cg != fg1)
    {
      if(!fg2)
      {
        fg2 = true;
        g2 = cg;
      }
      else if(g2 != cg)
      {
        C.error = Complex::STRUCTURE_INVALID;
        return result;
      }
    }
  }

  // Find which group correspond to which cell
  // For that, take an element, and check in which it's correctly oriented
  l_oriented_joint_t oj = mBoundary.any();
  const Chain<l_face_t>& faces = C.coboundary(oj);
  const l_oriented_face_t& of = faces.any();
  size_type group = face_group[~of];
  if((group == g1 and of.orientation() == pos) or
     (group == g2 and of.orientation() == neg))
  {
    std::swap(g1, g2);
  }

  // Put the faces in the right group
  for(auto fg: face_group)
  {
    if(group_eqs[fg.second] == g1)
    {
      result.first.insert(fg.first);
    }
    else
    {
      result.second.insert(fg.first);
    }
  }

  return result;
}

template <typename Complex, int N1,
          EnableIf<(N1 < Complex::N)> = 0>
bool replaceSuperTopFlips(Complex& C,
                           const Chain<ncell_t<N1,Complex>>& C1,
                           const Chain<ncell_t<N1,Complex>>& C2)
{
  // Here, replace the k+2 flips
  typedef ncell_flip_t<N1+2,Complex> l_cojoint_flip_t;
  typedef ncell_t<N1,Complex> l_cell_t;
  typedef oriented_ncell_t<N1,Complex> l_oriented_cell_t;
  util::set_vector<l_cojoint_flip_t*> to_delete;
  util::set_vector<l_cojoint_flip_t> to_add;
  QueryType Q;

  {
    l_cell_t c1 = ~C1.any();
    for(l_cojoint_flip_t* pf: C.match(Q, Q, Q, c1))
    {
      l_cojoint_flip_t nf(*pf);
      for(const l_oriented_cell_t& oc: C2)
      {
        nf.joint = ~oc;
        to_add.insert(nf);
      }
    }
    for(const l_oriented_cell_t& oc: C1)
    {
      for(l_cojoint_flip_t* pf: C.match(Q,Q,Q,~oc))
        to_delete.insert(pf);
    }
  }

  for(l_cojoint_flip_t* pf: to_delete)
    C.removeFlip(pf);

  for(const l_cojoint_flip_t& nf: to_add)
    C.addFlip(nf);

  return true;
}

template <typename Complex, int N1,
          EnableIf<(N1 >= Complex::N)> = 0>
bool replaceSuperTopFlips(Complex& ,
                           const Chain<ncell_t<N1,Complex>>&,
                           const Chain<ncell_t<N1,Complex>>&)
{
  // Nothing to do
  return true;
}


} // End detail namespace

/**
 * Add a cell given its oriented neighborhood
 *
 * \param cell Chain describing the boundary of the new cell
 * \param nc If non-null, name of the cell to be created. It cannot be already in the cell complex
 * \param link_to_top When adding a \f$ N\f$ -cell, will also link the cell to the top cell T. For any other \f$ k\f$ 
 * -cell, \f$ k<N\f$ , this parameter has no effect.
 *
 * \relates cellflips::CellComplex
 */
template <typename Complex, int N1>
ncell_t<N1+1,Complex>
addCell(Complex& C,
        const Chain<ncell_t<N1,Complex>>& cell,
        const ncell_t<N1+1,Complex>& nc = ncell_t<N1+1,Complex>::null,
        bool link_to_top = true)
{
  static_assert(N1>=0 and N1 <= Complex::N, "You can only add cell of dimension from 0 to N.");
  typedef ncell_t<N1+1,Complex> l_cell_t;
  typedef ncell_t<N1-1,Complex> l_joint_t;
  typedef ncell_flip_t<N1+1,Complex> l_cell_flip_t;
  typedef ncell_flip_t<N1+2,Complex> l_top_cell_flip_t;

  if(nc)
  {
    if(C.contains(nc))
    {
      C.error = Complex::CELL_ALREADY_IN_COMPLEX;
      return l_cell_t::null;
    }
  }

  l_cell_t c(nc, true);

  // Store the flips in construction
  std::unordered_map<l_joint_t,l_cell_flip_t> flips;
  std::list<l_top_cell_flip_t> top_flips;

  if(!detail::addCellChecks(C, cell, c, flips, top_flips, link_to_top))
    return l_cell_t::null;

  // Now, write the flips
  for(const auto& p: flips)
    C.addFlip(p.second);

  if(link_to_top)
  {
    for(const l_top_cell_flip_t& cf: top_flips)
      C.addFlip(cf);
  }

  // Now, add the relative orientations
  C.setCellOrientations(c, cell);

  return c;
}

/**
 * \overload
 *
 * In addition, this method detect if the container contains oriented or non-oriented cell
 * \relates cellflips::CellComplex
 */
template <typename Complex, typename Container>
ncell_t<dimension<Container>::N+1,Complex>
addCell(Complex& C, const Container& cell,
        const ncell_t<dimension<Container>::N+1,Complex>& nc,
        bool link_to_top = true)
{
  return ::cellflips::addCell(C, chain(cell), nc, link_to_top);
}

/**
 * \overload
 *
 * In addition, this method detect if the container contains oriented or non-oriented cell
 * \relates cellflips::CellComplex
 */
template <typename Complex, typename Container>
ncell_t<dimension<Container>::N+1,Complex>
addCell(Complex& C, const Container& cell, bool link_to_top = true)
{
  typedef ncell_t<dimension<Container>::N+1,Complex> l_cell_t;
  return ::cellflips::addCell(C, chain(cell), l_cell_t::null, link_to_top);
}

/**
 * \overload
 * \relates cellflips::CellComplex
 */
template <typename Complex, int N1>
ncell_t<N1+1,Complex>
addCell(Complex& C,
        std::initializer_list<oriented_ncell_t<N1,Complex>> cell,
        bool link_to_top = true)
{
  return ::cellflips::addCell(C, chain(cell), ncell_t<N1+1,Complex>(), link_to_top);
}

/**
 * \overload
 * \relates cellflips::CellComplex
 */
template <typename Complex, int N1>
ncell_t<N1+1,Complex>
addCell(Complex& C,
        std::initializer_list<oriented_ncell_t<N1,Complex>> cell,
        const ncell_t<N1+1,Complex>& nc,
        bool link_to_top = true)
{
  return ::cellflips::addCell(C, chain(cell), nc, link_to_top);
}

template <typename Complex, int N1>
bool removeCell(Complex& C, const ncell_t<N1,Complex>& c)
{
  return detail::removeCell(C, c);
}

/**
 * Split an edge
 * \relates cellflips::CellComplex
 */
template <typename Complex>
typename Complex::template SplitResult<1>
splitEdge(Complex& C,
          const typename Complex::edge_t& e,
          const typename Complex::template SplitResult<1>& def = typename Complex::template SplitResult<1>())
{
  typedef ncell_flip_t<1,Complex> l_cell_flip_t;
  typedef typename Complex::edge_t edge_t;
  typedef typename Complex::vertex_t vertex_t;
  typename Complex::template SplitResult<1> result;
  QueryType Q;

  edge_t el(def.left, true);
  edge_t er(def.right, true);
  vertex_t v(def.membrane, true);
  vertex_t v1(0), v2(0);

  std::list<l_cell_flip_t> to_add;
  std::list<l_cell_flip_t*> to_remove;

  const auto& m_boundary = C.match(e,Q,Q,Q);
  if(m_boundary.empty())
  {
    C.error = Complex::NO_SUCH_CELL;
    return result;
  }
  else if(m_boundary.size() > 1)
  {
    C.error = Complex::STRUCTURE_INVALID;
    return result;
  }

  for(l_cell_flip_t* pf: m_boundary)
  {
    // We will assume the face is from v1 to v2, so the flip is (e<v2,v1>_)
    v1 = pf->face2;
    v2 = pf->face1;

    l_cell_flip_t cf1 = *pf;
    cf1.cell = el;
    cf1.face1 = v;

    l_cell_flip_t cf2 = *pf;
    cf2.cell = er;
    cf2.face2 = v;

    to_remove.push_back(pf);
    to_add.push_back(cf1);
    to_add.push_back(cf2);
  }

  C.addVertex(v);

  for(const l_cell_flip_t& cf: to_add)
    C.addFlip(cf);

  C.setRelativeOrientation(el, v, pos);
  C.setRelativeOrientation(er, v2, pos);
  C.setRelativeOrientation(el, v1, neg);
  C.setRelativeOrientation(er, v, neg);

  detail::updateTopRelativeOrientations(C, e, el, er);

  C.removeCellOrientations(e);

  for(l_cell_flip_t* pf: to_remove)
    C.removeFlip(pf);

  detail::splitEdgeNeighbors(C, e, v, el, v1, er);
  detail::splitEdgeNeighbors(C, e, v, er, v2, edge_t::null);

  detail::splitVolume(C, e, el, er);//, TestType<(N>1)>());

  result.left = el;
  result.right = er;
  result.membrane = v;
  return result;
}

/**
 * Split a cell using a boundary
 * \relates cellflips::CellComplex
 */
template <typename Complex, int N1>
typename Complex::template SplitResult<N1>
splitCell(Complex& C,
          const ncell_t<N1,Complex>& cell,
          std::initializer_list<oriented_ncell_t<N1-2,Complex>> mBoundary,
          const typename Complex::template SplitResult<N1>& def = typename Complex::template SplitResult<N1>())
{
  return splitCell(C, cell, chain(mBoundary), def);
}

/**
 * Specialization of splitCell to split an edge.
 *
 * \note the boundary is ignored as it has to be {_}
 * \relates cellflips::CellComplex
 */
template <typename Complex>
typename Complex::template SplitResult<1>
splitCell(Complex& C,
          const typename Complex::edge_t& e,
          const Chain<typename Complex::bottom_t>& mBoundary,
          const typename Complex::template SplitResult<1>& def = typename Complex::template SplitResult<1>())
{
  Q_UNUSED(mBoundary);
  return splitEdge(e, def);
}

/**
 * Split a cell using a boundary
 * \relates cellflips::CellComplex
 */
template <typename Complex, int N1>
typename Complex::template SplitResult<N1>
splitCell(Complex& C,
          const ncell_t<N1,Complex>& cell,
          const Chain<ncell_t<N1-2,Complex>>& mBoundary,
          const typename Complex::template SplitResult<N1>& def = typename Complex::template SplitResult<N1>())
{
  typedef ncell_t<N1+1,Complex> l_coface_t;
  typedef ncell_t<N1,Complex> l_cell_t;
  typedef ncell_t<N1-1,Complex> l_face_t;
  typedef ncell_t<N1-2,Complex> l_joint_t;
  typedef typename l_joint_t::oriented_cell_t l_oriented_joint_t;
  typedef ncell_t<N1-3,Complex> l_m_joint_t;

  typedef ncell_flip_t<N1,Complex> l_cell_flip_t;
  typedef ncell_flip_t<N1-1,Complex> l_face_flip_t;
  typedef ncell_flip_t<N1+1,Complex> l_coface_flip_t;

  typename Complex::template SplitResult<N1> result;

  l_cell_t cl(def.left, true);
  l_cell_t cr(def.right, true);
  l_face_t m(def.membrane, true);

  // First, get the flips to add the membrane

  std::unordered_map<l_m_joint_t,l_face_flip_t> face_flips;
  std::list<l_cell_flip_t> cell_flips;
  std::list<l_cell_flip_t*> del_cell_flips;
  std::list<l_coface_flip_t> top_flips;
  std::list<l_coface_flip_t*> del_top_flips;
  QueryType Q;

  if(!detail::addCellChecks(C, mBoundary, m, face_flips, cell_flips, true))
    return result;

  // Now, get the sides and check the membrane is in the cell
  Set<l_joint_t> unseen_membrane = ~mBoundary;
  const auto& matches = C.match(cell,Q,Q,Q);
  std::unordered_map<l_face_t,int> face_group;
  std::vector<int> group_equiv;
  group_equiv.reserve(matches.size());
  l_face_t facel(0); // Face in the left cell
  // Group the faces
  for(l_cell_flip_t* pf: matches)
  {
    if(!mBoundary.contains(pf->joint))
    {
      int gl, gr;
      auto findl = face_group.find(pf->face1);
      if(findl != face_group.end())
      {
        gl = group_equiv[findl->second];
        while(gl != group_equiv[gl]) gl = group_equiv[gl];
      }
      else
      {
        gl = group_equiv.size();
        group_equiv.push_back(gl);
        findl = face_group.insert(std::make_pair(pf->face1, gl)).first;
      }
      auto findr = face_group.find(pf->face2);
      if(findr != face_group.end())
      {
        gr = group_equiv[findr->second];
        while(gr != group_equiv[gr]) gr = group_equiv[gr];
      }
      else
      {
        gr = group_equiv.size();
        group_equiv.push_back(gr);
        findr = face_group.insert(std::make_pair(pf->face2, gr)).first;
      }
      int grp = std::min(gl, gr);
#ifdef DEBUG_OUTPUT
      out << gl << "[" << findl->second << "] <=> "
        << gr << "[" << findr->second << "] => "
        << (grp < gl ? QString::number(gl) : QString()) << "/"
        << (grp < gr ? QString::number(gr) : QString())
        << " -> " << grp << endl;
#endif
      if(grp < gl)
        group_equiv[gl] = grp;
      if(grp < gr)
        group_equiv[gr] = grp;
      findl->second = grp;
      findr->second = grp;
    }
    else
    {
      unseen_membrane.erase(pf->joint);
      if(face_group.find(pf->face1) == face_group.end())
      {
        face_group[pf->face1] = group_equiv.size();
        group_equiv.push_back(group_equiv.size());
      }
      if(face_group.find(pf->face2) == face_group.end())
      {
        face_group[pf->face2] = group_equiv.size();
        group_equiv.push_back(group_equiv.size());
      }
      if(not facel)
      {
        DEBUG_OUT("Deciding facel on " << *pf << endl);
        l_oriented_joint_t oj = *mBoundary.find(pf->joint);
        if(oj.orientation() == neg)
          facel = pf->face1;
        else
          facel = pf->face2;
      }
    }
  }
  if(!unseen_membrane.empty())
  {
    C.error = Complex::MEMBRANE_NOT_IN_CELL;
    return result;
  }
  if(!facel)
  {
    C.error = Complex::MEMBRANE_NOT_IN_CELL;
    return result;
  }
  DEBUG_OUT("facel = " << facel << endl);
  // Compact the group
  int gl = -1, gr = -1;
  for(int i = 0 ; i < (int)group_equiv.size() ; ++i)
  {
    int grp = group_equiv[group_equiv[i]];
    group_equiv[i] = grp;
    if(gl < 0)
      gl = grp;
    else if(gl != grp)
    {
      if(gr < 0)
        gr = grp;
      else if(gr != grp)
      {
#ifdef DEBUG_OUTPUT
        out << "group_equiv:";
        for(int i= 0 ; i < group_equiv.size() ; ++i)
          out << " " << i << ":" << group_equiv[i];
        out << endl;
#endif
        C.error = Complex::MEMBRANE_DOESNT_CUT_CELL_IN_TWO;
        return result;
      }
    }
  }

  // Check gl is really the left cell
  if(gl != group_equiv[face_group[facel]])
    std::swap(gl, gr);

  // Now, get the list of face per cell
  Set<l_face_t> cell_l, cell_r;
  for(const auto& pf: face_group)
  {
    if(group_equiv[face_group[pf.first]] == gl)
      cell_l.insert(pf.first);
    else
      cell_r.insert(pf.first);
  }

#ifdef DEBUG_OUTPUT
  out << "Faces of cell l " << cl << ": ";
  for(const l_face_t& f: cell_l)
    out << shortString(f) << " ";
  out << endl;
  out << "Faces of cell r " << cr << ": ";
  for(const l_face_t& f: cell_r)
    out << shortString(f) << " ";
  out << endl;
#endif

  // Next, dimension n flips
  {
    const auto& matches = C.match(cell,Q,Q,Q);
    for(l_cell_flip_t* pf: matches)
    {
      DEBUG_OUT("Processing flip " << *pf << endl);
      if(cell_l.find(pf->face1) != cell_l.end() and
         cell_l.find(pf->face2) != cell_l.end())
      {
        DEBUG_OUT("   - in cell l" << endl);
        l_cell_flip_t nf = *pf;
        nf.cell = cl;
        DEBUG_OUT("  => inserting flip " << nf << endl);
        cell_flips.push_back(nf);
      }
      else if(cell_r.find(pf->face1) != cell_r.end() and
              cell_r.find(pf->face2) != cell_r.end())
      {
        DEBUG_OUT("   - in cell r" << endl);
        l_cell_flip_t nf = *pf;
        nf.cell = cr;
        DEBUG_OUT("  => inserting flip " << nf << endl);
        cell_flips.push_back(nf);
      }
      else if(cell_l.find(pf->face1) != cell_l.end())
      {
        DEBUG_OUT("   - " << pf->face1 << " in cell_l and " << pf->face2 << " in cell_r" << endl);
        l_cell_flip_t nf1 = *pf;
        l_cell_flip_t nf2 = *pf;
        nf1.cell = cl;
        nf1.face2 = m;
        nf2.cell = cr;
        nf2.face1 = m;
        DEBUG_OUT("  => inserting flips " << nf1 << " - " << nf2 << endl);
        cell_flips.push_back(nf1);
        cell_flips.push_back(nf2);
      }
      else
      {
        DEBUG_OUT("   - " << pf->face2 << " in cell_l and " << pf->face1 << " in cell_r" << endl);
        l_cell_flip_t nf1 = *pf;
        l_cell_flip_t nf2 = *pf;
        nf1.cell = cl;
        nf1.face1 = m;
        nf2.cell = cr;
        nf2.face2 = m;
        DEBUG_OUT("  => inserting flips " << nf1 << " - " << nf2 << endl);
        cell_flips.push_back(nf1);
        cell_flips.push_back(nf2);
      }
      del_cell_flips.push_back(pf);
    }
  }

  // Now, dimension n+1 flips
  {
    Set<l_coface_t> volumes;
    const auto& matches = C.match(Q,cell,Q,Q);
    for(l_coface_flip_t* pf: matches)
    {
      if(cell_l.find(pf->joint) != cell_l.end())
      {
        l_coface_flip_t nf = *pf;
        nf.replaceFace(cell, cl);
        top_flips.push_back(nf);
      }
      else
      {
        l_coface_flip_t nf = *pf;
        nf.replaceFace(cell, cr);
        top_flips.push_back(nf);
      }
      del_top_flips.push_back(pf);
      if(volumes.find(pf->cell) == volumes.end())
      {
        top_flips.push_back(C.relativeOrientation(pf->cell, cell) * C.cellFlip(pf->cell, cl, cr, m));
        volumes.insert(pf->cell);
      }
    }
    // In case we are in top-dimension and the cell has no neighbors
    if(matches.empty())
    {
      for(const l_coface_t& vol: C.cobounds(cell))
        top_flips.push_back(C.relativeOrientation(vol, cell) * C.cellFlip(vol, cl, cr, m));
    }
  }

  // All is good: write the flips for the membrane
  for(const auto& p: face_flips)
    C.addFlip(p.second);
  // Add the orientation for the membrane
  detail::addMembraneOrientations(C, m, mBoundary, cl, cr);

  // update the orientations
  detail::updateRelativeOrientations(C, cell, cl, cell_l, cr, cell_r);

  C.removeCellOrientations(cell);

  for(l_cell_flip_t* pf: del_cell_flips)
    C.removeFlip(pf);
  for(const l_cell_flip_t& cf: cell_flips)
    C.addFlip(cf);
  for(l_coface_flip_t* pf: del_top_flips)
    C.removeFlip(pf);
  for(const l_coface_flip_t& cf: top_flips)
    C.addFlip(cf);

  detail::updateSTopFlips(C, cell, cl, cr);

  result.left = cl;
  result.right = cr;
  result.membrane = m;
  return result;
}


/**
 * Replace a chain with another one of same boundary.
 * \relates cellflips::CellComplex
 */
template <typename Complex, int N1>
bool replace(Complex& C,
             const Chain<ncell_t<N1,Complex>>& C1,
             const Chain<ncell_t<N1,Complex>>& C2)
{
  typedef ncell_flip_t< N1+1,Complex> l_coface_flip_t;
  typedef ncell_t<N1+1,Complex> l_coface_t;
  typedef oriented_ncell_t<N1+1,Complex> l_oriented_coface_t;
  typedef ncell_t<N1,Complex> l_cell_t;
  typedef oriented_ncell_t<N1,Complex> l_oriented_cell_t;
  typedef ncell_t<N1-1,Complex> l_face_t;
  QueryType Q;
  // First, check that the cells in C2 are never in face position
  for(const l_oriented_cell_t& oc: C2)
  {
    if(!C.match(Q,~oc,Q,Q).empty())
    {
      C.error = Complex::CELL_IS_A_FACE;
      return false;
    }
  }
  // Now, check the boundaries of C1 and C2
  Chain<l_face_t> bs = C.boundary(C1);
  if(bs != C.boundary(C2))
  {
    C.error = Complex::WRONG_BOUNDARY;
    return false;
  }
  /*
   * // Now, check the joints not at the boundary of C1
   *if(C1.size() != 1)
   *{
   *  for(const l_joint_t& j: joints(C1)) if(!bs.contains(j))
   *  {
   *    for(const l_face_t& f: cofaces(j))
   *    {
   *      if(!C1.contains(f))
   *      {
   *        C.error = Complex::CHAIN_CONNECTED_INSIDE;
   *        return false;
   *      }
   *    }
   *  }
   *}
   * // Now, check the joints not at the boundary of C2
   *if(C2.size() != 1)
   *{
   *  for(const l_joint_t& j: joints(C2)) if(!bs.contains(j))
   *  {
   *    for(const l_face_t& f: cofaces(j))
   *    {
   *      if(!C2.contains(f))
   *      {
   *        C.error = Complex::CHAIN_CONNECTED_INSIDE;
   *        return false;
   *      }
   *    }
   *  }
   *}
   */
  std::unordered_map<l_face_t,std::pair<l_oriented_cell_t,l_oriented_cell_t> > boundary_map;
  // Associate the boundary element to the elements inside C1 and C2
  for(const l_oriented_cell_t& oc: C1)
  {
    const Set<l_face_t>& fs = C.faces(~oc);
    for(const l_face_t& f: fs)
      if(bs.contains(f))
      {
        boundary_map[f].first = oc;
      }
  }
  for(const l_oriented_cell_t& oc: C2)
  {
    const Set<l_face_t>& fs = C.faces(~oc);
    for(const l_face_t& f: fs)
      if(bs.contains(f))
      {
        boundary_map[f].second = oc;
      }
  }
  // add and remove inside flips
  util::set_vector<l_coface_flip_t> to_add;
  util::set_vector<l_coface_flip_t*> to_delete;

  // First find flips at the boundary
  for(auto bpe: boundary_map)
  {
    l_face_t f = bpe.first;
    l_cell_t c1 = ~bpe.second.first;
    l_cell_t c2 = ~bpe.second.second;
    const auto& m = C.match(Q,c1,Q,f);
    for(l_coface_flip_t* pf: m)
    {
      l_coface_flip_t nf(*pf);
      nf.replaceFace(c1, c2);
      to_add.insert(nf);
    }
  }

  const l_oriented_cell_t& ref = C1.any();
  Chain<l_coface_t> cbs = C.coboundary(ref);

  // Prepare removal of $k+1$-flips containing cells in C1
  for(const l_oriented_cell_t& oc: C1)
  {
    const auto& m = C.match(Q, ~oc, Q, Q);
    for(l_coface_flip_t* pf: m)
    {
      to_delete.insert(pf);
    }
  }

  std::unordered_map<l_face_t,std::pair<l_cell_t,l_cell_t> > inside_flips;

  // Prepare cell-orientations

  // Prepare $k+1$-flips with cells of C2 only
  for(const l_oriented_cell_t& oc: C2)
  {
    for(const l_face_t& f: C.bounds(~oc)) if(!bs.contains(f))
    {
      auto found = inside_flips.find(f);
      if(found != inside_flips.end())
      {
        if(found->second.second)
        {
          C.error = Complex::CHAIN_CONNECTED_INSIDE;
          return false;
        }
        else
        {
          found->second.second = ~oc;
        }
      }
      else
        inside_flips[f] = std::make_pair(~oc, l_cell_t(0));
    }
  }

  for(auto ife: inside_flips)
  {
    l_cell_t f1 = ife.second.first;
    l_cell_t f2 = ife.second.second;
    l_face_t j = ife.first;
    for(const l_oriented_coface_t& ocf: cbs)
    {
      l_coface_flip_t nf(~ocf, f1, f2, j);
      RelativeOrientation cor = ocf.orientation();
      RelativeOrientation f1or = C2.find(f1)->orientation();
      if(cor*f1or != C.relativeOrientation(f1, j))
        nf.reverse();
      to_add.insert(nf);
    }
  }

  if(!detail::replaceSuperTopFlips(C, C1, C2))
    return false;

  bool success = true;

  if(N1 != Complex::N)
  {
    for(const l_oriented_cell_t& oc: C1)
      for(const l_oriented_coface_t& ocf: cbs)
        if(!C.removeRelativeOrientation(~ocf, ~oc))
        {
          C.error = Complex::UNKNOWN_CELL_ORIENTATION;
          success = false;
        }
  }

  if(!success) return false;

  for(l_coface_flip_t* pf: to_delete)
    success &= C.removeFlip(pf);

  if(!success) return false;

  for(const l_coface_flip_t& nf: to_add)
    success &= bool(C.addFlip(nf));

  if(!success) return false;

  for(const l_oriented_coface_t& ocf: cbs)
    success &= C.setCellOrientations(~ocf, ocf.orientation() * C2);

  return success;
}

namespace detail
{

template <int N1, typename CellComplex,
          EnableIf<(N1 > CellComplex::N)> = 0>
bool computeOrientations(CellComplex&)
{
  return true; // Nothing to be done here
}

template <int N1, typename CellComplex,
          EnableIf<(N1 > 1) and (N1 <= CellComplex::N)> = 0>
bool computeOrientations(CellComplex& C)
{
  QueryType Q;
  typedef ncell_t<N1-1,CellComplex> l_face_t;
  for(auto c: C.template cells<N1>())
  {
    auto fs = Chain<l_face_t>();
    for(auto pfl: C.match(c, Q, Q, Q))
    {
      auto f1 = pfl->face1;
      auto f2 = pfl->face2;
      auto j = pfl->joint;
      auto r1 = C.relativeOrientation(f1, j);
      auto r2 = C.relativeOrientation(f2, j);
      if(r1 == invalid or r2 == invalid)
      {
        C.error = CellComplex::UNKNOWN_CELL_ORIENTATION;
        return false;
      }
      if(fs.contains(f1))
      {
        if(fs.contains(-r1*f1))
        {
          C.error = CellComplex::WRONG_BOUNDARY_ORIENTATION;
          return false;
        }
      }
      else
        fs.insert(r1*f1);
      if(fs.contains(f2))
      {
        if(fs.contains(r2*f2))
        {
          C.error = CellComplex::WRONG_BOUNDARY_ORIENTATION;
          return false;
        }
      }
      else
        fs.insert(-r2*f2);
    }
    for(auto of: fs)
      C.setRelativeOrientation(c, of);
  }
  return computeOrientations<N1+1>(C);
}

// Special case for edges, as there is no doubt about orientation of vertices
template <int N1, typename CellComplex,
          EnableIf<(N1 == 1)> = 0>
bool computeOrientations(CellComplex& C)
{
  QueryType Q;
  for(auto e: C.edges())
  {
    const auto& flips = C.match(e, Q, Q, Q);
    if(flips.empty())
    {
      C.error = CellComplex::NO_SUCH_FLIP;
      return false;
    }
    auto pfl = *flips.begin();
    C.setRelativeOrientation(e, pfl->face1, pos);
    C.setRelativeOrientation(e, pfl->face2, neg);
  }
  return computeOrientations<2>(C);
}

}

/**
 * Compute orientations based on the flips only
 *
 * \relates cellflips::CellComplex
 */
template <typename CellComplex>
bool computeOrientations(CellComplex& C)
{
  C.clearRelativeOrientations();
  return detail::computeOrientations<1>(C);
}

}

#endif // CELLFLIPS_EDITION_H

