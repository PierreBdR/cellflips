#ifndef CELLFLIPS_LAZY_H
#define CELLFLIPS_LAZY_H

#include <cellflips/cellflips.h>
#include <cstddef>
#include <iterator>
#include <climits>
#include <algorithm>
#include <util/range.h>
#include <memory>

namespace cellflips
{

namespace details
{

/**
 * \class generator_iterator
 *
 * \ingroup implementation
 *
 * Implement an iterator on a generator. A generator is a class for which a given set of expressions are defined. If g
 * is a generator, then:
 *
 *  - g.start() : returns the position of the beginning
 *  - g.finished() : returns the position past the end
 *  - auto pos = g.start(); ++pos; : pos points to the next element in g
 *  - g.hasNext(pos) : does g has a next position
 *  - g[pos] : value of the element for position pos
 *  - pos1 == po2 : Test if pos1 and pos2 points toward the same element
 *
 * Before moving forward, the hasNext method will always be called.
 */
template <typename Generator>
struct generator_iterator
{
  typedef Generator generator_t;
  typedef typename generator_t::value_type value_type;
  typedef typename generator_t::pointer pointer;
  typedef typename generator_t::const_pointer const_pointer;
  typedef typename generator_t::reference reference;
  typedef typename generator_t::const_reference const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;
  typedef std::forward_iterator_tag iterator_category;

  typedef typename Generator::position_t position_t;

  generator_iterator() { }

  generator_iterator(const Generator* g, bool end = false)
    : gen(g)
    , position(end ? g->finished() : g->start())
  { }

  generator_iterator(const generator_iterator&) = default;
  generator_iterator(generator_iterator&&) = default;

  generator_iterator& operator=(const generator_iterator&) = default;
  generator_iterator& operator=(generator_iterator&&) = default;

  bool operator==(const generator_iterator& other) const
  {
    //return gen == other.gen and position == other.position;
    return position == other.position;
  }

  bool operator!=(const generator_iterator& other) const
  {
    return not (*this == other);
  }

  generator_iterator& operator++()
  {
    if(gen->hasNext(position))
      ++position;
    else position = gen->finished();
    return *this;
  }

  generator_iterator operator++(int)
  {
    generator_iterator copy(*this);
    ++(*this);
    return copy;
  }

  const_reference operator*() const { return (*gen)[position]; }
  const_pointer operator->() const { return gen->get(position); }

  private:
  const Generator* gen = 0;
  position_t position = position_t();
};

template <typename Leaf>
struct Generator
{
public:
  typedef generator_iterator<Leaf> iterator;
  typedef generator_iterator<Leaf> const_iterator;

  iterator begin() { return iterator(static_cast<Leaf*>(this)); }
  iterator end() { return iterator(static_cast<Leaf*>(this), true); }

  const_iterator begin() const { return const_iterator(static_cast<const Leaf*>(this)); }
  const_iterator end() const { return const_iterator(static_cast<const Leaf*>(this), true); }

  const_iterator cbegin() const { return const_iterator(static_cast<const Leaf*>(this)); }
  const_iterator cend() const { return const_iterator(static_cast<const Leaf*>(this), true); }
};

template <int N2, int N1, typename CellComplex>
struct Bounds;

template <int N1, typename CellComplex>
class Faces : public Generator<Faces<N1,CellComplex>>
{
public:

  typedef ncell_t<N1,CellComplex> l_cell_t;
  typedef ncell_t<N1-1,CellComplex> l_face_t;

  typedef std::size_t position_t;

  typedef l_face_t value_type;
  typedef const l_face_t* pointer;
  typedef const l_face_t* const_pointer;
  typedef const l_face_t& reference;
  typedef const l_face_t& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  Faces(const CellComplex* C,
         const ncell_t<N1,CellComplex>& cc)
    : c(cc)
  {
    QueryType Q;
    flips = &C->match(c, Q, Q, Q);
    flips_it = begin(*flips);
    end_flips = end(*flips);
    findNext();
  }

  Faces(const CellComplex& CC,
         const ncell_t<N1,CellComplex>& cc)
    : Faces(&CC, cc)
  { }

  Faces(const Faces&) = default;
  Faces(Faces&&) = default;
  Faces& operator=(const Faces&) = default;
  Faces& operator=(Faces&&) = default;

  bool hasNext(position_t i) const
  {
    if(i+1 < visited.size()) return true;
    return findNext();
  }

  const_reference operator[](position_t i) const
  {
    return visited[i];
  }

  const_pointer get(position_t i) const
  {
    return &visited[i];
  }

  bool empty() const
  {
    return flips->empty();
  }

  position_t start() const { return (empty() ? finished() : 0); }
  position_t finished() const { return std::numeric_limits<position_t>::max(); }

private:
  bool findNext() const
  {
    bool found_next = false;
    while(not found_next and (flips_it != end_flips))
    {
      auto pfl = *flips_it;
      auto first = begin(visited);
      auto last = end(visited);
      if(find(first, last, pfl->face1) == last)
      {
        found_next = true;
        visited.push_back(pfl->face1);
      }
      first = begin(visited);
      last = end(visited);
      if(find(first, last, pfl->face2) == last)
      {
        found_next = true;
        visited.push_back(pfl->face2);
      }
      ++flips_it;
    }
    return found_next;
  }

  typedef cell_flips_set_t<N1,CellComplex> match_t;

  l_cell_t c;
  mutable std::vector<l_face_t> visited;
  const match_t* flips = 0;
  mutable typename match_t::const_iterator flips_it;
  typename match_t::const_iterator end_flips;
};

template <int N1, typename CellComplex>
class Joints
{
public:

  typedef ncell_t<N1,CellComplex> l_cell_t;
  typedef ncell_t<N1-2,CellComplex> l_joint_t;

private:
  typedef cell_flips_set_t<N1,CellComplex> match_t;

  typedef typename match_t::const_iterator match_iterator_t;

public:
  struct iterator : public match_iterator_t
  {
    iterator() : match_iterator_t() { }
    iterator(const match_iterator_t& copy) : match_iterator_t(copy) { }
    iterator(match_iterator_t&& copy) : match_iterator_t(std::move(copy)) { }
    iterator(const iterator&) = default;
    iterator(iterator&&) = default;

    iterator& operator=(const match_iterator_t& other)
    {
      match_iterator_t::operator=(other);
      return *this;
    }
    iterator& operator=(match_iterator_t&& other)
    {
      match_iterator_t::operator=(std::move(other));
      return *this;
    }

    iterator& operator=(const iterator&) = default;
    iterator& operator=(iterator&&) = default;

    typedef l_joint_t value_type;
    typedef const l_joint_t* pointer;
    typedef const l_joint_t* const_pointer;
    typedef const l_joint_t& reference;
    typedef const l_joint_t& const_reference;

    reference operator*() { return (**static_cast<match_iterator_t*>(this))->joint; }
    const_reference operator*() const { return (**static_cast<const match_iterator_t*>(this))->joint; }
    pointer operator->() { return &(**this); }
    const_pointer operator->() const { return &(**this); }
  };

  typedef iterator const_iterator;

  typedef l_joint_t value_type;
  typedef const l_joint_t* pointer;
  typedef const l_joint_t* const_pointer;
  typedef const l_joint_t& reference;
  typedef const l_joint_t& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  Joints(const CellComplex* C,
         const l_cell_t& c)
  {
    QueryType Q;
    flips = &C->match(c, Q, Q, Q);
  }

  Joints(const CellComplex& CC,
         const ncell_t<N1,CellComplex>& cc)
    : Joints(&CC, cc)
  { }

  Joints(const Joints&) = default;
  Joints(Joints&&) = default;
  Joints& operator=(const Joints&) = default;
  Joints& operator=(Joints&&) = default;

  iterator begin() { return flips->begin(); }
  iterator end() { return flips->end(); }

  const_iterator begin() const { return flips->begin(); }
  const_iterator end() const { return flips->end(); }

  const_iterator cbegin() const { return flips->cbegin(); }
  const_iterator cend() const { return flips->cend(); }

  bool empty() const
  {
    return flips->empty();
  }

private:
  const match_t* flips = 0;
};

template <int N2, int N1, typename CellComplex>
class LowBounds : public Generator<LowBounds<N2,N1,CellComplex> >
{
public:

  typedef ncell_t<N1,CellComplex> l_cell_t;
  typedef ncell_t<N2,CellComplex> l_face_t;

  typedef l_face_t value_type;
  typedef const l_face_t* pointer;
  typedef const l_face_t* const_pointer;
  typedef const l_face_t& reference;
  typedef const l_face_t& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  typedef std::size_t position_t;

  LowBounds(const CellComplex* CC,
            const ncell_t<N1,CellComplex>& cc)
    : C(CC)
    , c(cc)
    , upper(C, c)
    , upper_it(upper.begin())
  {
    findNext();
  }

  LowBounds(const CellComplex& CC,
            const ncell_t<N1,CellComplex>& cc)
    : LowBounds(&CC, cc)
  { }

  LowBounds(const LowBounds&) = default;
  LowBounds(LowBounds&&) = default;
  LowBounds& operator=(const LowBounds&) = default;
  LowBounds& operator=(LowBounds&&) = default;

  bool hasNext(size_t i) const
  {
    if(i+1 < visited.size()) return true;
    return findNext();
  }

  const_reference operator[](size_t i) const
  {
    return visited[i];
  }

  const_pointer get(size_t i) const
  {
    return &visited[i];
  }

  bool empty() const
  {
    return upper.empty();
  }

  position_t start() const { return (empty() ? finished() : 0); }
  position_t finished() const { return std::numeric_limits<position_t>::max(); }

private:
  bool findNext() const
  {
    bool found_next = false;
    while(not found_next and (upper_it != upper.end()))
    {
      if(not current)
      {
        current = make_unique<joints_t>(C, *upper_it);
        current_it = current->begin();
      }
      if(current_it != current->end())
      {
        auto f = *current_it;
        ++current_it;
        if(std::find(visited.begin(), visited.end(), f) == visited.end())
        {
          found_next = true;
          visited.push_back(f);
        }
      }
      else
      {
        current.release();
        ++upper_it;
      }
    }
    return found_next;
  }

  typedef typename Bounds<N2+2,N1,CellComplex>::type upper_bounds_t;
  typedef Joints<N2+2,CellComplex> joints_t;

  const CellComplex* C;
  l_cell_t c;

  upper_bounds_t upper;
  mutable typename upper_bounds_t::iterator upper_it;
  mutable std::unique_ptr<joints_t> current;
  mutable typename joints_t::iterator current_it;
  mutable std::vector<l_face_t> visited;
};

template <int diff, int N1, typename CellComplex>
struct Bounds_
{
  typedef LowBounds<N1-diff, N1, CellComplex> type;
};

template <int N1, typename CellComplex>
struct Bounds_<1,N1,CellComplex>
{
  typedef Faces<N1,CellComplex> type;
};

template <int N1, typename CellComplex>
struct Bounds_<2,N1,CellComplex>
{
  typedef Joints<N1,CellComplex> type;
};

template <int N2, int N1, typename CellComplex>
struct Bounds
{
  typedef typename Bounds_<N1-N2,N1,CellComplex>::type type;
};

template <int N1, typename CellComplex>
class Boundary : public Generator<Boundary<N1,CellComplex> >
{
public:

  typedef oriented_ncell_t<N1,CellComplex> l_oriented_cell_t;
  typedef oriented_ncell_t<N1-1,CellComplex> l_oriented_face_t;

  typedef l_oriented_face_t value_type;
  typedef const l_oriented_face_t* pointer;
  typedef const l_oriented_face_t* const_pointer;
  typedef const l_oriented_face_t& reference;
  typedef const l_oriented_face_t& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  typedef std::size_t position_t;

  Boundary(const CellComplex* CC,
         const oriented_ncell_t<N1,CellComplex>& cc)
    : C(CC)
    , oc(cc)
  {
    QueryType Q;
    flips = &C->match(~oc, Q, Q, Q);
    flips_it = begin(*flips);
    end_flips = end(*flips);
    findNext();
  }

  Boundary(const CellComplex& CC,
           const oriented_ncell_t<N1,CellComplex>& cc)
    : Boundary(&CC, cc)
  { }

  Boundary(const Boundary&) = default;
  Boundary(Boundary&&) = default;
  Boundary& operator=(const Boundary&) = default;
  Boundary& operator=(Boundary&&) = default;

  bool hasNext(size_t i) const
  {
    if(i+1 < visited.size()) return true;
    return findNext();
  }

  const_reference operator[](size_t i) const
  {
    return visited[i];
  }

  const_pointer get(size_t i) const
  {
    return &visited[i];
  }

  bool empty() const
  {
    return flips->empty();
  }

  position_t start() const { return (empty() ? finished() : 0); }
  position_t finished() const { return std::numeric_limits<position_t>::max(); }

private:
  bool findNext() const
  {
    bool found_next = false;
    while(not found_next and (flips_it != end_flips))
    {
      auto pfl = *flips_it;
      auto first = begin(visited);
      auto last = end(visited);
      auto of1 = (oc.orientation() * C->relativeOrientation(~oc, pfl->face1)) * pfl->face1;
      if(find(first, last, of1) == last)
      {
        found_next = true;
        visited.push_back(of1);
      }
      auto of2 = (oc.orientation() * C->relativeOrientation(~oc, pfl->face2)) * pfl->face2;
      first = begin(visited);
      last = end(visited);
      if(find(first, last, of2) == last)
      {
        found_next = true;
        visited.push_back(of2);
      }
      ++flips_it;
    }
    return found_next;
  }

  typedef cell_flips_set_t<N1,CellComplex> match_t;

  const CellComplex* C;
  l_oriented_cell_t oc;
  mutable std::vector<l_oriented_face_t> visited;
  const match_t* flips = 0;
  mutable typename match_t::const_iterator flips_it;
  typename match_t::const_iterator end_flips;
};

template <int N2, int N1, typename CellComplex>
struct Cobounds;

template <int N1, typename CellComplex>
class Cofaces : public Generator<Cofaces<N1,CellComplex> >
{
public:

  typedef ncell_t<N1,CellComplex> l_cell_t;
  typedef ncell_t<N1+1,CellComplex> l_coface_t;

  typedef std::size_t position_t;

  typedef l_coface_t value_type;
  typedef const l_coface_t* pointer;
  typedef const l_coface_t* const_pointer;
  typedef const l_coface_t& reference;
  typedef const l_coface_t& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  Cofaces(const CellComplex* C,
         const ncell_t<N1,CellComplex>& cc)
    : c(cc)
  {
    QueryType Q;
    flips = &C->match(Q, c, Q, Q);
    flips_it = begin(*flips);
    end_flips = end(*flips);
    findNext();
  }

  Cofaces(const CellComplex& CC,
         const ncell_t<N1,CellComplex>& cc)
    : Cofaces(&CC, cc)
  { }

  Cofaces(const Cofaces&) = default;
  Cofaces(Cofaces&&) = default;
  Cofaces& operator=(const Cofaces&) = default;
  Cofaces& operator=(Cofaces&&) = default;

  bool hasNext(position_t i) const
  {
    if(i+1 < visited.size()) return true;
    return findNext();
  }

  const_reference operator[](position_t i) const
  {
    return visited[i];
  }

  const_pointer get(position_t i) const
  {
    return &visited[i];
  }

  bool empty() const
  {
    return flips->empty();
  }

  position_t start() const { return (empty() ? finished() : 0); }
  position_t finished() const { return std::numeric_limits<position_t>::max(); }

private:
  bool findNext() const
  {
    bool found_next = false;
    while(not found_next and (flips_it != end_flips))
    {
      auto pfl = *flips_it;
      auto first = begin(visited);
      auto last = end(visited);
      if(find(first, last, pfl->cell) == last)
      {
        found_next = true;
        visited.push_back(pfl->cell);
      }
      ++flips_it;
    }
    return found_next;
  }

  typedef face_flips_set_t<N1+1,CellComplex> match_t;

  l_cell_t c;
  mutable std::vector<l_coface_t> visited;
  const match_t* flips = 0;
  mutable typename match_t::const_iterator flips_it;
  typename match_t::const_iterator end_flips;
};

template <int N1, typename CellComplex>
class Cojoints
{
public:

  typedef ncell_t<N1,CellComplex> l_cell_t;
  typedef ncell_t<N1+2,CellComplex> l_cojoint_t;

private:
  typedef cell_flips_set_t<N1+2,CellComplex> match_t;

  typedef typename match_t::const_iterator match_iterator_t;

public:
  struct iterator : public match_iterator_t
  {
    iterator() : match_iterator_t() { }
    iterator(const match_iterator_t& copy) : match_iterator_t(copy) { }
    iterator(match_iterator_t&& copy) : match_iterator_t(std::move(copy)) { }
    iterator(const iterator&) = default;
    iterator(iterator&&) = default;

    iterator& operator=(const match_iterator_t& other)
    {
      match_iterator_t::operator=(other);
      return *this;
    }
    iterator& operator=(match_iterator_t&& other)
    {
      match_iterator_t::operator=(std::move(other));
      return *this;
    }

    iterator& operator=(const iterator&) = default;
    iterator& operator=(iterator&&) = default;

    typedef l_cojoint_t value_type;
    typedef const l_cojoint_t& reference;
    typedef const l_cojoint_t& const_reference;

    reference operator*() { return (**static_cast<match_iterator_t*>(this))->cell; }
    const_reference operator*() const { return (**static_cast<const match_iterator_t*>(this))->cell; }
  };

  typedef iterator const_iterator;

  typedef l_cojoint_t value_type;
  typedef const l_cojoint_t* pointer;
  typedef const l_cojoint_t* const_pointer;
  typedef const l_cojoint_t& reference;
  typedef const l_cojoint_t& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  Cojoints(const CellComplex* C,
           const l_cell_t& c)
  {
    QueryType Q;
    flips = &C->match(Q, Q, Q, c);
  }

  Cojoints(const CellComplex& CC,
         const ncell_t<N1,CellComplex>& cc)
    : Cojoints(&CC, cc)
  { }

  Cojoints(const Cojoints&) = default;
  Cojoints(Cojoints&&) = default;
  Cojoints& operator=(const Cojoints&) = default;
  Cojoints& operator=(Cojoints&&) = default;

  iterator begin() { return flips->begin(); }
  iterator end() { return flips->end(); }

  const_iterator begin() const { return flips->begin(); }
  const_iterator end() const { return flips->end(); }

  const_iterator cbegin() const { return flips->cbegin(); }
  const_iterator cend() const { return flips->cend(); }

  bool empty() const
  {
    return flips->empty();
  }

private:
  const match_t* flips = 0;
};

template <int N2, int N1, typename CellComplex>
class HighCobounds : public Generator<HighCobounds<N2,N1,CellComplex> >
{
public:

  typedef ncell_t<N1,CellComplex> l_cell_t;
  typedef ncell_t<N2,CellComplex> l_coface_t;

  typedef l_coface_t value_type;
  typedef const l_coface_t* pointer;
  typedef const l_coface_t* const_pointer;
  typedef const l_coface_t& reference;
  typedef const l_coface_t& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  typedef std::size_t position_t;

  HighCobounds(const CellComplex* CC,
               const ncell_t<N1,CellComplex>& cc)
    : C(CC)
    , c(cc)
    , lower(C, c)
    , lower_it(lower.begin())
  {
    findNext();
  }

  HighCobounds(const CellComplex& CC,
               const ncell_t<N1,CellComplex>& cc)
    : HighCobounds(&CC, cc)
  { }

  HighCobounds(const HighCobounds&) = default;
  HighCobounds(HighCobounds&&) = default;
  HighCobounds& operator=(const HighCobounds&) = default;
  HighCobounds& operator=(HighCobounds&&) = default;

  bool hasNext(size_t i) const
  {
    if(i+1 < visited.size()) return true;
    return findNext();
  }

  const_reference operator[](size_t i) const
  {
    return visited[i];
  }

  const_pointer get(size_t i) const
  {
    return visited[i];
  }

  bool empty() const
  {
    return lower.empty();
  }

  position_t start() const { return (empty() ? finished() : 0); }
  position_t finished() const { return std::numeric_limits<position_t>::max(); }

private:
  bool findNext() const
  {
    bool found_next = false;
    while(not found_next and (lower_it != lower.end()))
    {
      if(not current)
      {
        current = make_unique<cojoints_t>(C, *lower_it);
        current_it = current->begin();
      }
      if(current_it != current->end())
      {
        auto f = *current_it;
        ++current_it;
        if(std::find(visited.begin(), visited.end(), f) == visited.end())
        {
          found_next = true;
          visited.push_back(f);
        }
      }
      else
      {
        current.release();
        ++lower_it;
      }
    }
    return found_next;
  }

  typedef typename Cobounds<N2-2,N1,CellComplex>::type lower_cobounds_t;
  typedef Cojoints<N2-2,CellComplex> cojoints_t;

  const CellComplex* C;
  l_cell_t c;

  lower_cobounds_t lower;
  mutable typename lower_cobounds_t::iterator lower_it;
  mutable std::unique_ptr<cojoints_t> current;
  mutable typename cojoints_t::iterator current_it;
  mutable std::vector<l_coface_t> visited;
};

template <int diff, int N1, typename CellComplex>
struct Cobounds_
{
  typedef HighCobounds<N1+diff, N1, CellComplex> type;
};

template <int N1, typename CellComplex>
struct Cobounds_<1,N1,CellComplex>
{
  typedef Cofaces<N1,CellComplex> type;
};

template <int N1, typename CellComplex>
struct Cobounds_<2,N1,CellComplex>
{
  typedef Cojoints<N1,CellComplex> type;
};

template <int N2, int N1, typename CellComplex>
struct Cobounds
{
  typedef typename Cobounds_<N2-N1,N1,CellComplex>::type type;
};

template <int N1, typename CellComplex>
class Coboundary : public Generator<Coboundary<N1,CellComplex> >
{
public:

  typedef oriented_ncell_t<N1,CellComplex> l_oriented_cell_t;
  typedef oriented_ncell_t<N1+1,CellComplex> l_oriented_coface_t;

  typedef l_oriented_coface_t value_type;
  typedef const l_oriented_coface_t* pointer;
  typedef const l_oriented_coface_t* const_pointer;
  typedef const l_oriented_coface_t& reference;
  typedef const l_oriented_coface_t& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  typedef std::size_t position_t;

  Coboundary(const CellComplex* CC,
         const oriented_ncell_t<N1,CellComplex>& cc)
    : C(CC)
    , oc(cc)
  {
    QueryType Q;
    flips = &C->match(Q, ~oc, Q, Q);
    flips_it = begin(*flips);
    end_flips = end(*flips);
    findNext();
  }

  Coboundary(const CellComplex& CC,
             const oriented_ncell_t<N1,CellComplex>& cc)
    : Coboundary(&CC, cc)
  { }

  Coboundary(const Coboundary&) = default;
  Coboundary(Coboundary&&) = default;
  Coboundary& operator=(const Coboundary&) = default;
  Coboundary& operator=(Coboundary&&) = default;

  bool hasNext(size_t i) const
  {
    if(i+1 < visited.size()) return true;
    return findNext();
  }

  const_reference operator[](size_t i) const
  {
    return visited[i];
  }

  const_pointer get(size_t i) const
  {
    return &visited[i];
  }

  bool empty() const
  {
    return flips->empty();
  }

  position_t start() const { return (empty() ? finished() : 0); }
  position_t finished() const { return std::numeric_limits<position_t>::max(); }

private:
  bool findNext() const
  {
    bool found_next = false;
    while(not found_next and (flips_it != end_flips))
    {
      auto pfl = *flips_it;
      auto first = begin(visited);
      auto last = end(visited);
      auto ocf = (oc.orientation() * C->relativeOrientation(pfl->cell, ~oc)) * pfl->cell;
      if(find(first, last, ocf) == last)
      {
        found_next = true;
        visited.push_back(ocf);
      }
      ++flips_it;
    }
    return found_next;
  }

  typedef face_flips_set_t<N1+1,CellComplex> match_t;

  const CellComplex* C;
  l_oriented_cell_t oc;
  mutable std::vector<l_oriented_coface_t> visited;
  const match_t* flips = 0;
  mutable typename match_t::const_iterator flips_it;
  typename match_t::const_iterator end_flips;
};


} // details namespace

template <int N1, typename CellComplex,
          EnableIf<(N1 > 0) and (N1 <= CellComplex::N)> = 0>
details::Boundary<N1,CellComplex> boundary(const CellComplex& C, const oriented_ncell_t<N1,CellComplex>& oc)
{
  return details::Boundary<N1,CellComplex>(C, oc);
}

template <int N1, typename CellComplex,
          EnableIf<(N1 == 0)> = 0>
Chain<ncell_t<-1,CellComplex>> boundary(const CellComplex& C, const oriented_ncell_t<N1,CellComplex>& oc)
{
  if(C.hasCell(~oc))
    return chain({oc.orientation()*C._});
  return Chain<ncell_t<-1,CellComplex>>{};
}

template <int N1, typename CellComplex,
          EnableIf<(N1 == CellComplex::N+1)> = 0>
Chain<ncell_t<CellComplex::N, CellComplex>> boundary(const CellComplex& C, const oriented_ncell_t<N1,CellComplex>& oc)
{
  return chain(oc.orientation(), C.cells());
}

template <int N1, typename CellComplex,
          EnableIf<(N1 < CellComplex::N) and (N1 > -1)> = 0>
details::Coboundary<N1,CellComplex> coboundary(const CellComplex& C, const oriented_ncell_t<N1,CellComplex>& oc)
{
  return details::Coboundary<N1,CellComplex>(C, oc);
}

template <int N1, typename CellComplex,
          EnableIf<(N1 == CellComplex::N)> = 0>
Chain<ncell_t<CellComplex::N+1, CellComplex>> coboundary(const CellComplex& C, const oriented_ncell_t<N1,CellComplex>& oc)
{
  if(C.hasCell(~oc))
    return chain({oc.orientation() * C.T});
  return Chain<ncell_t<CellComplex::N+1,CellComplex>>{};
}

template <int N1, typename CellComplex,
          EnableIf<(N1 == -1)> = 0>
Chain<ncell_t<0,CellComplex>> coboundary(const CellComplex& C, const oriented_ncell_t<N1,CellComplex>& oc)
{
  return chain(oc.orientation(), C.vertices());
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 == N1-1) && (N2 != -1) && (N1 != CellComplex::N+1)> = 0>
details::Faces<N1,CellComplex> bounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c)
{
  return details::Faces<N1,CellComplex>(C, c);
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 == N1-2) && (N2 != -1) && (N1 != CellComplex::N+1)> = 0>
details::Joints<N1,CellComplex> bounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c)
{
  return details::Joints<N1,CellComplex>(C, c);
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 != -1) && (N1 == CellComplex::N+1)> = 0>
auto bounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(C.template cells<N2>())
{
  static_assert(N1 > N2, "Bounds' dimension must be less than the input cell");
  return C.template cells<N2>();
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 == -1)> = 0>
std::vector<ncell_t<-1,CellComplex>> bounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c)
{
  static_assert(N1 > N2, "Bounds' dimension must be less than the input cell");
  if(C.hasCell(c))
    return std::vector<ncell_t<-1,CellComplex>>{C._};
  return std::vector<ncell_t<-1,CellComplex>>{};
}

template <int N1, typename CellComplex>
auto bounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(bounds<N1-1,N1,CellComplex>(C, c))
{
  return bounds<N1-1,N1,CellComplex>(C, c);
}

template <int N1, typename CellComplex>
auto joints(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(bounds<N1-2,N1,CellComplex>(C, c))
{
  return bounds<N1-2,N1,CellComplex>(C, c);
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 < N1-2) and (N2 != -1) and (N1 != CellComplex::N+1)> = 0>
details::LowBounds<N2,N1,CellComplex> bounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c)
{
  static_assert(N1 > N2, "Bounds' dimension must be less than the input cell");
  return details::LowBounds<N2,N1,CellComplex>(C, c);
}

template <int N1, typename CellComplex,
          EnableIf<(N1 > 1)> = 0>
auto edges(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(bounds<1,N1,CellComplex>(C,c))
{
  return bounds<1,N1,CellComplex>(C,c);
}

template <int N1, typename CellComplex>
auto vertices(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(bounds<0,N1,CellComplex>(C,c))
{
  return bounds<0,N1,CellComplex>(C,c);
}

template <int N1, typename CellComplex>
auto faces(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(bounds(C,c))
{
  return bounds(C,c);
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 == N1+1) && (N2 != CellComplex::N+1) && (N1 != -1)> = 0>
details::Cofaces<N1,CellComplex> cobounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c)
{
  return details::Cofaces<N1,CellComplex>(C, c);
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 == N1+2) && (N2 != CellComplex::N+1) && (N1 != -1)> = 0>
details::Cojoints<N1,CellComplex> cobounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c)
{
  return details::Cojoints<N1,CellComplex>(C, c);
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 > N1+2) and (N2 != CellComplex::N+1) and (N1 != -1)> = 0>
details::HighCobounds<N2,N1,CellComplex> cobounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c)
{
  return details::HighCobounds<N2,N1,CellComplex>(C, c);
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 == CellComplex::N+1)> = 0>
std::vector<ncell_t<N2,CellComplex> > cobounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c)
{
  if(C.hasCell(c))
    return std::vector<ncell_t<N2,CellComplex>>{C.T};
  return std::vector<ncell_t<N2,CellComplex>>{};
}

template <int N2, int N1, typename CellComplex,
          EnableIf<(N2 != CellComplex::N+1) and (N1 == -1)> = 0>
auto cobounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(C.template cell<N2>())
{
  static_assert(N2 > N1, "Cobounds' dimension must be greater than the cell");
  return C.template cell<N2>();
}

template <int N1, typename CellComplex>
auto cobounds(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(cobounds<N1+1,N1,CellComplex>(C, c))
{
  return cobounds<N1+1,N1,CellComplex>(C, c);
}

template <int N1, typename CellComplex>
auto cofaces(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(cobounds<N1+1,N1,CellComplex>(C, c))
{
  return cobounds<N1+1,N1,CellComplex>(C, c);
}

template <int N1, typename CellComplex>
auto cojoints(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(cobounds<N1+2,N1,CellComplex>(C, c))
{
  return cobounds<N1+2,N1,CellComplex>(C, c);
}

template <int N1, typename CellComplex,
          EnableIf<(N1 < 1)> = 0>
auto edges(const CellComplex& C, const ncell_t<N1,CellComplex>& c) -> decltype(cobounds<1,N1,CellComplex>(C,c))
{
  return cobounds<1,N1,CellComplex>(C,c);
}

}

#endif // CELLFLIPS_LAZY_H

