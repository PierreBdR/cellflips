#ifndef CELLTUPLES_H
#define CELLTUPLES_H

#include <exception>

namespace cellflips
{

namespace detail
{

template <int _N, typename CellComplex>
struct TupleConstructor_
{
  static_assert(_N>=0, "Dimension must be greater than 0");
  typedef typename TupleConstructor_<_N-1,CellComplex>::type prev_type;
  typedef typename util::PrependTuple<typename NTypes<CellComplex::N-_N, CellComplex>::cell_t, prev_type>::type type;
};

template <typename CellComplex>
struct TupleConstructor_<0, CellComplex>
{
  typedef std::tuple<typename NTypes<CellComplex::N,CellComplex>::cell_t> type;
};

struct EmptyCellTuple
{
  template <typename... Args>
  operator std::tuple<Args...>() const
  {
    return std::tuple<Args...>(Args(0)...);
  }
};

}

struct InvalidTuple : public std::exception
{
  virtual ~InvalidTuple() { }
  const char* what() const throw() override { return "Invalid arguments when creating tuple. Either missing or redundant cell."; }
};

/**
 * \ingroup main
 * \class CellTuple celltuples.h "celltuples.h"
 *
 * Class implementing the notion of cell tuple for a given cell complex
 */
template <typename CellComplex>
struct CellTuple
{
  enum {
    N = CellComplex::N ///< Dimension of the tuple
  };
  typedef typename detail::TupleConstructor_<N, CellComplex>::type tuple_type;
  tuple_type cell_tuple;

  template <int N1>
  using ncell_t = cellflips::ncell_t<N1,CellComplex>;

  typedef ncell_t<0> vertex_t;
  typedef ncell_t<1> edge_t;
  typedef ncell_t<N-1> face_t;
  typedef ncell_t<N> cell_t;

  /**
   * Creates an empty cell tuple
   */
  CellTuple()
    : cell_tuple(detail::EmptyCellTuple{})
  { }

  CellTuple(CellTuple& ) = default;
  CellTuple(const CellTuple& ) = default;
  CellTuple(CellTuple&&) = default;

  CellTuple& operator=(CellTuple&) = default;
  CellTuple& operator=(const CellTuple&) = default;
  CellTuple& operator=(CellTuple&&) = default;

  /**
   * Create an initialize cell tuple, with each dimension initialized.
   *
   * The cells must be given in increasing dimension, starting at 0.
   */
  template <typename... Args>
  CellTuple(Args&&... args)
    : cell_tuple(detail::EmptyCellTuple())
  {
    make_cells(std::forward<Args>(args)...);
  }

  /**
   * Set all the cells to null
   */
  void clear()
  {
    this->clear(IntConstant<N>());
  }

  /**
   * Returns true if the tuple is valid (i.e. all cells are valid)
   */
  explicit operator bool() const
  {
    return isValid();
  }

  /**
   * Return true if all the cells are empty
   */
  bool empty() const
  {
    return this->empty(IntConstant<N>());
  }

  /**
   * Returns true if the tuple is valid (i.e. all cells are valid)
   */
  bool isValid() const
  {
    return isValid(IntConstant<N>());
  }

  /**
   * Returns true if the tuple is valid for this cell complex (i.e. all N-cells are faces of (N+1)-cells)
   */
  bool isValid(const CellComplex& C) const
  {
    return isValid(IntConstant<N>(), C);
  }

  /**
   * Returns the tuple flipped on dimension N1
   */
  template <int N1>
  CellTuple flipped(const CellComplex& C) const
  {
    CellTuple copy(*this);
    if(copy.flip<N1>(C))
      return copy;
    return CellTuple();
  }

  /**
   * Flip the dimension N1 and returns true if the tuple exists
   */
  template <int N1>
  bool flip(const CellComplex& C)
  {
    static_assert(N1 >= 0 and N1 <= N, "Dimension must be from 0 to N (inclusive)");
    auto f = flip_(C, IntConstant<N1>());
    if(f)
    {
      std::get<N1>(cell_tuple) = f;
      return true;
    }
    return false;
  }

  /**
   * Returns the cell of dimension N1 in the tuple
   */
  template <int N1>
  const ncell_t<N1>& ncell() const
  {
    static_assert(N1 >= 0 and N1 <= N, "Dimension must be from 0 to N (inclusive)");
    return std::get<N1>(cell_tuple);
  }

  /**
   * Returns the cell of dimension N1 in the tuple
   */
  template <int N1>
  ncell_t<N1>& ncell()
  {
    static_assert(N1 >= 0 and N1 <= N, "Dimension must be from 0 to N (inclusive)");
    return std::get<N1>(cell_tuple);
  }

  /**
   * Returns the 0-cell in the tuple
   */
  const vertex_t& vertex() const
  {
    return ncell<0>();
  }

  /**
   * Returns the 0-cell in the tuple
   */
  vertex_t& vertex()
  {
    return ncell<0>();
  }

  /**
   * Returns the 1-cell in the tuple
   */
  const edge_t& edge() const
  {
    return ncell<1>();
  }

  /**
   * Returns the 1-cell in the tuple
   */
  edge_t& edge()
  {
    return ncell<1>();
  }

  /**
   * Returns the N-1-cell in the tuple
   */
  const face_t& face() const
  {
    return ncell<N-1>();
  }

  /**
   * Returns the N-1-cell in the tuple
   */
  face_t& face()
  {
    return ncell<N-1>();
  }

  /**
   * Returns the N-cell in the tuple
   */
  const cell_t& cell() const
  {
    return ncell<N>();
  }

  /**
   * Returns the N-cell in the tuple
   */
  cell_t& cell()
  {
    return ncell<N>();
  }

  /**
   * Returns the orientation of the cell tuple.
   *
   * The orientation of the cell tuple is the product of the relative orientation between each successive pair of cells.
   */
  RelativeOrientation orientation(const CellComplex& C) const
  {
    return C.relativeOrientation(C.T, cell()) * orientation_(C, IntConstant<N>());
  }

  /**
   * Returns the orientation of the cell tuple.
   *
   * The orientation of the cell tuple is the product of the relative orientation between each successive pair of cells.
   */
  template <int N1, int N2>
  RelativeOrientation relativeOrientation(const CellComplex& C) const
  {
    static_assert(N1<=N2, "The first dimension must be the smaller one");
    return relativeOrientation_<N1,N2>(C, TestType<(N1==N2)>());
  }

  bool operator==(const CellTuple& other) const
  {
    return equal_(other, IntConstant<N>());
  }

  bool operator!=(const CellTuple& other) const
  {
    return not equal_(other, IntConstant<N>());
  }

protected:

  bool equal_(const CellTuple& , const IntConstant<-1>&) const
  {
    return true;
  }

  template <int N1>
  bool equal_(const CellTuple& other, const IntConstant<N1>&) const
  {
    return (ncell<N1>() == other.ncell<N1>()) and equal_(other, IntConstant<N1-1>());
  }

  bool isValid(const IntConstant<-1>&) const
  {
    return true;
  }

  template <int N1>
  bool isValid(const IntConstant<N1>&) const
  {
    return bool(ncell<N1>()) and isValid(IntConstant<N1-1>());
  }

  bool isValid(const IntConstant<-1>&, const CellComplex& ) const
  {
    return true;
  }

  bool isValid(const IntConstant<0>&, const CellComplex& C) const
  {
    return bool(vertex()) and C.contains(vertex());
  }

  bool isValid(const IntConstant<1>&, const CellComplex& C) const
  {
    return bool(vertex()) and bool(edge()) and not C.match(edge(), vertex(), C.Q, C._).empty();
  }

  template <int N>
  bool isValid(const IntConstant<N>&, const CellComplex& C) const
  {
    return (isValid(IntConstant<N-2>(), C) and
            bool(ncell<N>()) and bool(ncell<N-1>()) and bool(ncell<N-2>()) and
            not C.match(ncell<N>(), ncell<N-1>(), C.Q, ncell<N-2>()).empty());
  }

  // case N1 == 0
  vertex_t flip_(const CellComplex& C, const IntConstant<0>& )
  {
    return C.flip(edge(), vertex(), C._);
  }

  // case N1 == N
  cell_t flip_(const CellComplex& C, const IntConstant<N>& )
  {
    return C.flip(C.T, cell(), face());
  }

  template <int N1>
  ncell_t<N1> flip_(const CellComplex& C, const IntConstant<N1>& )
  {
    return C.flip(ncell<N1+1>(), ncell<N1>(), ncell<N1-1>());
  }

  // Case N1 == 0
  RelativeOrientation orientation_(const CellComplex& C, const IntConstant<0>& ) const
  {
    return C.relativeOrientation(vertex(), C._);
  }

  template <int N1>
  RelativeOrientation orientation_(const CellComplex& C, const IntConstant<N1>& ) const
  {
    return C.relativeOrientation(ncell<N1>(), ncell<N1-1>()) * orientation_(C, IntConstant<N1-1>());
  }

  template <int N1, int N2>
  RelativeOrientation relativeOrientation_(const CellComplex& C, const false_type&) const
  {
    return C.relativeOrientation(ncell<N2>(), ncell<N2-1>())*relativeOrientation<N1,N2-1>(C);
  }

  template <int N1, int N2>
  RelativeOrientation relativeOrientation_(const CellComplex&, const true_type&) const
  {
    return pos;
  }

  bool empty(const IntConstant<0>& ) const
  {
    return vertex().isNull();
  }

  template <int N1>
  bool empty(const IntConstant<N1>& ) const
  {
    return ncell<N1>().isNull() and empty(IntConstant<N1-1>());
  }

  void clear(const IntConstant<-1>&)
  {
    // Nothing to do
  }

  template <int N1>
  void clear(const IntConstant<N1>&)
  {
    ncell<N1>() = ncell_t<N1>(0);
    clear(IntConstant<N1-1>());
  }

  void make_cells()
  {
  }

  template <typename Arg, typename... Args>
  void make_cells(Arg&& arg, Args&&... args)
  {
    ncell<dimension<Arg>::N>() = std::forward<Arg>(arg);
    make_cells(std::forward<Args>(args)...);
  }

};

/**
 * Create a new cell tuple
 *
 * \relates CellTuple
 */
template <typename CellComplex, typename... Args>
CellTuple<CellComplex> cellTuple(const CellComplex&, const Args&... cells)
{
  return CellTuple<CellComplex>(cells...);
}

namespace detail
{

// Once we reach vertices, we're good!
template <typename CellComplex>
bool findNextUp(CellTuple<CellComplex>&, const CellComplex&,
                const IntConstant<0>& )
{
  return true;
}

/*
 * Find the faces of the N cells, and try to complete the complex ...
 */
template <typename CellComplex, int N>
bool findNextUp(CellTuple<CellComplex>& res, const CellComplex& C,
                const IntConstant<N>& )
{
  auto& c = res.template ncell<N>();
  auto& f = res.template ncell<N-1>();
  if(f)
  {
    if(!C.isFace(c, f))
      return false;
    return findNextUp(res, C, IntConstant<N-1>());
  }
  for(const auto& try_f: C.faces(c))
  {
    f = try_f;
    if(findNextUp(res, C, IntConstant<N-1>()))
      return true;
  }
  f = ncell_t<N-1,CellComplex>::null;
  return false;
}

// If we reach 0, then any vertex will do!
template <typename CellComplex>
bool findNextDown(CellTuple<CellComplex>& t, const CellComplex& C,
                  const IntConstant<0>& )
{
  t.vertex() = C.anyVertex();
  return bool(t.vertex());
}

/*
 * Try to find the cell N based on what is ... lower!
 */
template <typename CellComplex, int N>
bool findNextDown(CellTuple<CellComplex>& res, const CellComplex& C,
                  const IntConstant<N>& )
{
  auto& c = res.template ncell<N>();
  if(c)
    return findNextUp(res, C, IntConstant<N>());
  if(findNextDown(res, C, IntConstant<N-1>()))
  {
    c = C.anyCoface(res.template ncell<N-1>());
    return bool(c);
  }
  return false;
}

} // namespace detail

/**
 * Fill in an incomplete tuple with valid cells, if possible.
 *
 * \relates CellTuple
 */
template <typename CellComplex>
bool findTuple(const CellComplex& C, CellTuple<CellComplex>& t)
{
  if(t.empty())
    t.cell() = C.anyCell();
  if(t.cell())
    return detail::findNextUp(t, C, IntConstant<CellComplex::N>());
  return detail::findNextDown(t, C, IntConstant<CellComplex::N>());
}

/**
 * Create a new cell tuple, replacing null cells (or query objects...) to create a valid tuple, if possible
 *
 * \relates CellTuple
 */
template <typename CellComplex, typename... Args>
CellTuple<CellComplex> findTuple(const CellComplex& C, const Args&... cells)
{
  auto res = cellTuple(C, cells...);
  if(!findTuple(C, res))
    res.clear();
  return res;
}

namespace detail {
template <int N1, typename CellComplex>
QString cell_tuple_toString__(const CellTuple<CellComplex>& t, const false_type&)
{
  return QString("%1,%2").arg(shortString(t.template ncell<N1>())).arg(cell_tuple_toString__<N1-1>(t));
}

template <int N1, typename CellComplex>
QString cell_tuple_toString__(const CellTuple<CellComplex>& t, const true_type&)
{
  return shortString(t.template ncell<0>());
}

template <int N1, typename CellComplex>
QString cell_tuple_toString__(const CellTuple<CellComplex>& t)
{
  return cell_tuple_toString__<N1>(t, TestType<(N1==0)>());
}
} // namespace detail

template <typename CellComplex>
QString toString(const CellTuple<CellComplex>& t)
{
  return QString("tuple{%1}").arg(detail::cell_tuple_toString__<CellComplex::N>(t));
}

template <typename CellComplex>
QTextStream& operator<<(QTextStream& s, const CellTuple<CellComplex>& t)
{
  s << toString(t);
  return s;
}

}

namespace std
{
template <typename T, typename Traits, typename CellComplex>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const cellflips::CellTuple<CellComplex>& t)
{
  ss << toString(t).toStdString();
  return ss;
}
}

#endif // CELLTUPLES_H

