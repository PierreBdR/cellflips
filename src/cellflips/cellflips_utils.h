#ifndef CELLFLIPS_UTILS_H
#define CELLFLIPS_UTILS_H

#include <type_traits>
#include <utility>
#include <memory>

#include "cellflips/cell.h"

namespace cellflips
{

  template<typename T, typename... Args>
  std::unique_ptr<T> make_unique(Args&&... args)
  {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
  }

  using std::true_type;
  using std::false_type;

  /**
   * \ingroup utility
   * \class TestType
   * Class used to test a condition at compile time
   */
  template <bool v>
  using TestType = std::integral_constant<bool, v>;

  /**
   * \ingroup utility
   * \class IntConstant
   * Shortcut for an integer integral constant
   */
  template <int N>
  using IntConstant = std::integral_constant<int, N>;

  /**
   * \ingroup utility
   * \vlass EnableIf
   * Shortcut to enable a method only if some conditions are fullfiled.
   *
   * To use it:
   * \code
   * template <..., EnableIf<cond> = 0> voif f() { }
   * \endcode
   */
  template <bool B>
  using EnableIf = typename std::enable_if<B,int>::type;


  /**
   * \ingroup utility
   * \vlass EnableIf
   * Shortcut to disable a method if some conditions are fullfiled.
   *
   * To use it:
   * \code
   * template <..., DisableIf<cond> = 0> voif f() { }
   * \endcode
   */
  template <bool B>
  using DisableIf = typename std::enable_if<not B,int>::type;

  /**
   * \ingroup utility
   * \class is_oriented_type
   * Class testing if the type as argument is oriented or not
   */
  template <typename T>
  struct is_oriented_type : public false_type
  {
  };

  template <typename T>
  struct is_oriented_type<OrientedObject<T> > : public true_type
  {
  };

  /**
   * \ingroup utility
   * \class remove_orientation
   * If the type is oriented, get the content, otherwise return the content.
   */
  template <typename T>
  struct remove_orientation
  {
    typedef T type;
  };

  template <typename T>
  struct remove_orientation<OrientedObject<T>>
  {
    typedef T type;
  };

  template <typename T>
  struct remove_orientation<const T>
  {
    typedef const typename remove_orientation<T>::type type;
  };

  template <typename T>
  struct remove_orientation<volatile T>
  {
    typedef volatile typename remove_orientation<T>::type type;
  };

  template <typename T>
  struct remove_orientation<const volatile T>
  {
    typedef const volatile typename remove_orientation<T>::type type;
  };

  /**
   * \class dimension
   * Class template used to extract the dimension of an object.
   *
   * By definition:
   *  - Cells have a dimension
   *  - OrientedObjects have a dimension
   *  - Cell flips have a dimension
   *  - Cell complexes have a dimension
   *  - Cell complex layers have a dimension
   *  - Containers of objects with a dimension has the dimension of the objects
   */
  template <typename T>
  struct dimension
  {
    typedef typename dimension<typename T::value_type>::type type;
    enum
    {
      N = type::N
    };
  };

  template <typename T>
  struct dimension<OrientedObject<T> >
  {
    typedef OrientedObject<T> type;
    enum
    {
      N = type::N
    };
  };

  template <int _N, typename T, typename Alloc>
  struct dimension<Cell<_N,T,Alloc> >
  {
    typedef Cell<_N,T,Alloc> type;
    enum
    {
      N = _N
    };
  };

  template <typename T>
  struct dimension<const T>
  {
    typedef typename dimension<T>::type type;
    enum
    {
      N = type::N
    };
  };

  template <typename T>
  struct dimension<T&>
  {
    typedef typename dimension<T>::type type;
    enum
    {
      N = type::N
    };
  };

  template <typename T>
  struct dimension<volatile T>
  {
    typedef typename dimension<T>::type type;
    enum
    {
      N = type::N
    };
  };

  /**
   * \ingroup internal
   * \class void_layer_types
   *
   * Structure defining all the types defined in a layer as void.
   *
   * \note This type is used to allow for the generic definition of methods with invalid dimensions. But code trying to call these methods will fail to compile.
   */
  struct void_layer_types
  {
    typedef void cell_content_t;
    typedef void cell_t;
    typedef void oriented_cell_t;
    typedef void cell_flip_t;

    typedef void cell_iterator;
    typedef void cell_range;
  };


  /**
   * \ingroup internal
   * \class ComplexNthLayer_internal
   *
   * This class is used to access the various layers.
   *
   * If the dimension is incorrect, the type will be \c void_layer_types
   */
  template <int N, typename CellComplex, bool value>
  struct ComplexNthLayer_internal;

  template <int N, typename CellComplex>
  struct ComplexNthLayer_internal<N,CellComplex,true>
  {
    typedef typename std::tuple_element<N+1,typename CellComplex::layers_t>::type type;
  };

  template <int N, typename CellComplex>
  struct ComplexNthLayer_internal<N,CellComplex,false>
  {
    typedef void_layer_types type;
  };

  /**
   * \ingroup internal
   * \class ComplexNthLayer
   *
   * This class recover the type and value of the layer handling cells and cell flips of dimension N.
   *
   * CellComplex can be either a cell complex or a cell complex layer. And if the dimension is invalid, the result
   * will be \c void_layer_types
   */
  template <int N, typename CellComplex>
  using ComplexNthLayer = typename ComplexNthLayer_internal<N,CellComplex,(N>=-1 and N <=CellComplex::N+1)>::type;

  /**
   * Helper function to retrieve the nth dimensional layer
   */
  template <int N1, typename CellComplex>
  ComplexNthLayer<N1,CellComplex>& get_layer(CellComplex& C)
  {
    static_assert(N1>=-1 and N1<=CellComplex::N+1, "The layer of a cell complex is between -1 and N+1");
    return std::get<N1+1>(C._layers);
  }

  /**
   * Helper function to retrieve the constant reference to the nth dimensional layer
   */
  template <int N1, typename CellComplex>
  const ComplexNthLayer<N1,CellComplex>& get_layer(const CellComplex& C)
  {
    static_assert(N1>=-1 and N1<=CellComplex::N+1, "The layer of a cell complex is between -1 and N+1");
    return std::get<N1+1>(C._layers);
  }

  template <int N1, typename CellComplex>
  using cell_flips_set_t = typename ComplexNthLayer<N1,CellComplex>::cell_flips_set_t;

  template <int N1, typename CellComplex>
  using face_flips_set_t = typename ComplexNthLayer<N1,CellComplex>::face_flips_set_t;

  template <int N1, typename CellComplex>
  using joint_flips_set_t = typename ComplexNthLayer<N1,CellComplex>::joint_flips_set_t;


  /**
   * \ingroup utility
   * \class NTypes
   * Retrieve useful types for the cells of dimension N
   */
  template <int N, typename CellComplex>
  struct NTypes
  {
    /// Type of the layer of dimension N
    typedef ComplexNthLayer<N,CellComplex> layer_complex_t;

    /// Type of the content of an N-cell
    typedef typename layer_complex_t::cell_content_t cell_content_t;
    /// Type of a N-cell
    typedef typename layer_complex_t::cell_t cell_t;
    /// Type of an oriented N-cell
    typedef typename layer_complex_t::oriented_cell_t oriented_cell_t;
    /**
     * Type of a N-flip
     * \note this type is \c void for N = 0 or -1
     */
    typedef typename layer_complex_t::cell_flip_t cell_flip_t;

    /// Type of an iterator on N-cells
    typedef typename layer_complex_t::cell_iterator cell_iterator;
    /// Type of a range of N-cells
    typedef typename layer_complex_t::cell_range cell_range;
  };

  template <typename CellComplex>
  struct NTypes<0,CellComplex>
  {
    typedef ComplexNthLayer<0,CellComplex> layer_complex_t;
    typedef typename layer_complex_t::cell_content_t cell_content_t;
    typedef typename layer_complex_t::cell_t cell_t;
    typedef typename layer_complex_t::oriented_cell_t oriented_cell_t;

    typedef typename layer_complex_t::cell_iterator cell_iterator;
    typedef typename layer_complex_t::cell_range cell_range;

    // No such thing in 0D
    typedef void cell_flip_t;
  };

  template <typename CellComplex>
  struct NTypes<-1,CellComplex>
  {
    typedef ComplexNthLayer<-1,CellComplex> layer_complex_t;
    typedef typename layer_complex_t::cell_content_t cell_content_t;
    typedef typename layer_complex_t::cell_t cell_t;
    typedef typename layer_complex_t::oriented_cell_t oriented_cell_t;

    typedef typename layer_complex_t::cell_iterator cell_iterator;
    typedef typename layer_complex_t::cell_range cell_range;
    // No such thing in -1D
    typedef void cell_flip_t;
  };

  /**
   * \ingroup internal
   * \class TopCellContent
   *
   * Content of the top cell. Empty, but different to not mix with bottom cell.
   */
  struct TopCellContent
  { };


}

#endif // CELLFLIPS_UTILS_H

