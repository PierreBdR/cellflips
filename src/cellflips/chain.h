// -*- c++ -*-
#ifndef CHAIN_H
#define CHAIN_H

/**
 * \file chain.h
 *
 * This files include the definition of \f$n\f$-chains and cell multi-sets.
 */

#include "cellflips/cell.h"
#include "cellflips/cellflips_utils.h"

#include <util/range.h>
#include <unordered_set>
#include <initializer_list>

namespace cellflips
{

namespace detail
{
template <typename Container>
using NonConstContent = typename std::remove_const<typename Container::value_type>::type;
}

/**
 * \class Chain
 * Class represented an oriented multi-set of oriented objects.
 *
 * The chain uses a multi-set of oriented objects to make the operation order-independent.
 */
template <typename Content, typename Compare = std::less<Content>,
          typename Alloc = std::allocator<Content>>
class Chain
{
  struct OrientedCompare
  {
    explicit OrientedCompare(const Compare& cmp = Compare())
      : object_compare(cmp)
    { }

    explicit OrientedCompare(Compare&& cmp)
      : object_compare(std::move(cmp))
    { }

    bool operator()(const OrientedObject<Content>& o1, const OrientedObject<Content>& o2) const
    {
      if(object_compare(~o1, ~o2)) return true;
      else if(object_compare(~o2, ~o1)) return false;
      return o1.orientation() < o2.orientation();
    }

    Compare object_compare;
  };
  public:
    /// Type of a non-oriented value
    typedef Content non_oriented_value_type;
    /// Type of an oriented value
    typedef OrientedObject<Content> value_type;
    /// Type of the underlying container (currently a multiset)
    typedef util::multiset_vector<value_type, OrientedCompare, Alloc> container_t;
    ///\name STL Order Sets types
    //@{
    /// Type of a constant iterator
    typedef typename container_t::const_iterator const_iterator;
    /// Type of an iterator
    typedef typename container_t::iterator iterator;
    /// Type of a constant iterator
    typedef typename container_t::const_reverse_iterator const_reverse_iterator;
    /// Type of an iterator
    typedef typename container_t::reverse_iterator reverse_iterator;
    /// Type of a size value
    typedef typename container_t::size_type size_type;
    /// Type of a reference
    typedef typename container_t::reference reference;
    /// Type of a constant reference
    typedef typename container_t::const_reference const_reference;
    /// Type of a pointer
    typedef typename container_t::pointer pointer;
    /// Type of a constant pointer
    typedef typename container_t::const_pointer const_pointer;
    /// Type of the key comparison
    typedef typename container_t::key_compare key_compare;
    /// Type of the value comparison
    typedef typename container_t::value_compare value_compare;
    /// Type of the allocator
    typedef typename container_t::allocator_type allocator_type;
    //@}

    ///\name Constructors
    //@{
    /// Constructor with an initializer list
    Chain(std::initializer_list<value_type> init,
          const key_compare& comp = key_compare(),
          const allocator_type& alloc = allocator_type())
      : _chain(init, comp, alloc)
    {
    }

    explicit Chain(const key_compare& comp = key_compare(), const allocator_type& alloc = allocator_type())
      : _chain(comp, alloc)
    { }

    /**
     * Default copy constructor
     */
    Chain(const Chain&) = default;

    /**
     * Default move constructor
     */
    Chain(Chain&&) = default;

    /**
     * Initialize from another container
     */
    template <typename Container,
              EnableIf<is_oriented_type<typename Container::value_type>::value> = 0>
    explicit Chain(const Container& init,
                   const key_compare& comp = key_compare(),
                   const allocator_type& alloc = allocator_type())
    : _chain(comp, alloc)
    {
      using std::begin;
      using std::end;
      _chain.insert(begin(init), end(init));
    }

    /**
     * Initialize from a pair of iterators
     */
    template <typename InputIterator>
    Chain(InputIterator first, InputIterator last,
          const key_compare& comp = key_compare(), const allocator_type& alloc = allocator_type())
      : _chain(first, last, comp, alloc)
    { }

    explicit Chain(const allocator_type& alloc)
      : _chain(alloc)
    { }

    Chain(const Chain& copy, const allocator_type& alloc)
      : _chain(copy, alloc)
    { }

    Chain(Chain&& copy, const allocator_type& alloc)
      : _chain(std::move(copy), alloc)
    { }

    /**
     * Initialize from a (possible not-oriented) container
     */
    template <typename Container>
    Chain(const RelativeOrientation& orient, const Container& cont)
    {
      for(const auto& c: cont)
        this->insert(orient*c);
    }

    explicit Chain(const value_type& oc)
      : _chain()
    {
      _chain.insert(oc);
    }

    Chain operator-() const
    {
      auto result = *this;
      result.reverse();
      /*
       *Chain result;
       *for(const auto& oc: _chain)
       *  result.insert(result.end(), -oc);
       */
      return result;
    }

    Chain& operator+()
    {
      return *this;
    }

    const Chain& operator+() const
    {
      return *this;
    }

    Chain& reverse()
    {
      // Warning, this is valid only if used with sorted container!
      for(const auto& oc: _chain)
        const_cast<value_type&>(oc).reverse();
      /*
       *Chain result = -(*this);
       *std::swap(*this, result);
       */
      return *this;
    }

    /**
     * Returns true if the cell was inserted, and false if the opposite cell has been removed instead
     */
    std::pair<iterator, bool> insert(const value_type& oc)
    {
      // Find the best place to insert the object
      auto found = _chain.lower_bound(oc);

      // The best best is at the end -> the object is not in the set
      if(found != this->end())
      {
        // Which object has been found
        const auto& val = *found;
        if(this->equiv(~val, ~oc))
        {
          if(val.orientation() == oc.orientation())
            return {_chain.insert(found, oc), true};
          return {_chain.erase(found), false};
        }
      }
      if(found == _chain.begin())
        return {_chain.insert(found, oc), true};

      const auto& prev_val = *(found-1);
      if(this->equiv(~prev_val, ~oc))
        return {_chain.erase(found-1), false};
      return {_chain.insert(found, oc), true};

      /*
       *auto found = _chain.find(-oc);
       *if(found == _chain.end())
       *{
       *  return {_chain.insert(oc), true};
       *}
       *else
       *{
       *  _chain.erase(found);
       *  return {_chain.end(), false};
       *}
       */
    }

    template <typename Iterator>
    void insert(Iterator first, Iterator last)
    {
      for(Iterator it = first ; it != last ; ++it)
        insert(*it);
    }

    size_type erase(const non_oriented_value_type& c)
    {
      auto low = value_type(c, neg);
      auto high = value_type(c, pos);
      auto find_low = _chain.lower_bound(low);
      auto find_high = _chain.upper_bound(high);
      auto val = std::distance(find_low, find_high);
      _chain.erase(find_low, find_high);
      return val;
    }

    size_type erase(const value_type& oc)
    {
      return _chain.erase(oc);
    }

    iterator erase(const iterator& oc)
    {
      return _chain.erase(oc);
    }

    Chain& operator=(const Chain&) = default;
    Chain& operator=(Chain&&) = default;

    bool operator==(const Chain& other) const
    {
      return _chain == other._chain;
    }

    bool operator!=(const Chain& other) const
    {
      return _chain != other._chain;
    }

    Chain& operator&=(const Chain& other)
    {
      for(iterator it1 = begin(), it2 = other.begin() ; it1 != end() and it2 != other.end() ; )
      {
        if(*it1 == *it2)
        {
          ++it1; ++it2;
        }
        else if(this->compare(*it1, *it2))
          it1 = erase(it1);
        else
          ++it2;
      }

      /*
       *for(iterator it = begin() ; it != end() ; )
       *{
       *  if(!other.contains(*it))
       *    it = erase(it);
       *  else
       *    ++it;
       *}
       */
      return *this;
    }

    Chain& operator|=(const Chain& other)
    {
      Compare comp;
      iterator it1 = this->begin(), it2 = other.begin();
      for( ; it1 != this->end() and it2 != other.end() ; )
      {
        const auto& v1 = *it1;
        const auto& v2 = *it2;
        if(comp(~v1, ~v2))
          ++it1;
        else
        {
          if(comp(~v2, ~v1))
          {
            it1 = _chain.insert(it1, *it2);
            ++it1;
          }
          else if(v1.orientation() * v2.orientation() == neg) // if orientation are opposite
            it1 = _chain.erase(it1);
          else
          {
            it1 = _chain.insert(it1, *it2);
            ++it1;
          }
          ++it2;
        }
      }
      for( ; it2 != other.end() ; ++it2)
        _chain.insert(_chain.end(), *it2);
      /*
       *for(const auto& oc: other)
       *{
       *  if(contains(-oc))
       *    erase(-oc);
       *  else
       *    insert(oc);
       *}
       */
      return *this;
    }

    const_reference any() const { return *cbegin(); }

    ///\name STL container methods
    //@{
    iterator begin() noexcept { return _chain.begin(); }
    iterator end() noexcept { return _chain.end(); }

    const_iterator begin() const noexcept { return _chain.cbegin(); }
    const_iterator end() const noexcept { return _chain.cend(); }

    const_iterator cbegin() const noexcept { return _chain.cbegin(); }
    const_iterator cend() const noexcept { return _chain.cend(); }

    reverse_iterator rbegin() noexcept { return _chain.rbegin(); }
    reverse_iterator rend() noexcept { return _chain.rend(); }

    const_reverse_iterator rbegin() const noexcept { return _chain.rbegin(); }
    const_reverse_iterator rend() const noexcept { return _chain.rend(); }

    const_reverse_iterator crbegin() const noexcept { return _chain.crbegin(); }
    const_reverse_iterator crend() const noexcept { return _chain.crend(); }

    const_iterator find(const_reference oc) const
    {
      return _chain.find(oc);
    }

    iterator find(const_reference oc)
    {
      return _chain.find(oc);
    }

    size_type size() const { return _chain.size(); }

    bool empty() const { return _chain.empty(); }

    void clear()
    {
      _chain.clear();
    }
    //@}

    ///\name Extended container methods
    //@{

    bool contains(const_reference oc) const
    {
      return _chain.find(oc) != _chain.end();
    }

    //@}

    ///\name Method with non-oriented cells
    //@{
    const_iterator find(const non_oriented_value_type& c) const
    {
      auto found = _chain.lower_bound(neg*c);
      if(found == this->end()) return this->end();
      if(this->equiv(~(*found), c)) return found;
      return this->end();
      /*
       *const_iterator found = _chain.find(+c);
       *if(found == _chain.end())
       *  return _chain.find(-c);
       *return found;
       */
    }

    iterator find(const non_oriented_value_type& c)
    {
      auto found = const_cast<const Chain*>(this)->find(c);
      return this->begin() + (found - this->cbegin());
      /*
       *iterator found = _chain.find(+c);
       *if(found == _chain.end())
       *  return _chain.find(-c);
       *return found;
       */
    }

    bool contains(const non_oriented_value_type& c) const
    {
      auto found = _chain.lower_bound(neg * c);
      if(found == this->end()) return false;
      return this->equiv(~(*found), c);
      /*
       *return (_chain.find(+c) != _chain.end() or
       *        _chain.find(-c) != _chain.end());
       */
    }
    //@}

    ///\name Global operations
    //@{

    Chain& operator*=(const RelativeOrientation& ro)
    {
      container_t new_chain;
      for(const auto& oc: _chain)
        new_chain.insert(ro*oc);
      std::swap(new_chain, _chain);
      return *this;
    }
    //@}

    void swap(Chain& other)
    {
      using std::swap;
      swap(_chain, other._chain);
    }

    key_compare key_comp() const
    {
      return _chain.key_comp();
    }

    value_compare value_comp() const
    {
      return _chain.value_comp();
    }


    friend Chain operator&(const Chain& c1, const Chain& c2)
    {
      auto result = Chain{};
      std::set_intersection(c1.begin(), c1.end(),
                            c2.begin(), c2.end(),
                            std::inserter(result._chain, result._chain.end()),
                            result.key_comp());
      return result;
      /*
       *auto result = c1;
       *result &= c2;
       *return result;
       */
    }

    friend Chain operator|(const Chain& c1, const Chain& c2)
    {
      using std::begin;
      using std::end;
      auto result = Chain{};
      auto it1 = begin(c1), it2 = begin(c2);
      auto last1 = end(c1), last2 = end(c2);
      while(it1 != last1 and it2 != last2)
      {
        const auto& ov1 = *it1;
        const auto& ov2 = *it2;
        if(result.compare(~ov1, ~ov2))
        {
          result._chain.insert(result.end(), ov1);
          ++it1;
        }
        else if(result.compare(~ov2, ~ov1))
        {
          result._chain.insert(result.end(), ov2);
          ++it2;
        }
        else // Same values for ov1 and ov2
        {
          if(ov1.orientation() == ov2.orientation())
          {
            result._chain.insert(result.end(), ov1);
            result._chain.insert(result.end(), ov2);
          }
          ++it1;
          ++it2;
        }
      }
      while(it1 != last1)
        result._chain.insert(result.end(), *it1++);
      while(it2 != last2)
        result._chain.insert(result.end(), *it2++);
      return result;
      /*
       *auto result = c1;
       *result |= c2;
       *return result;
       */
    }


protected:
  container_t _chain;

  bool compare(const value_type& v1, const value_type& v2) const
  {
    const auto& comp = key_comp();
    return comp(v1, v2);
  }

  bool compare(const non_oriented_value_type& v1, const non_oriented_value_type& v2) const
  {
    const auto& comp = key_comp();
    return comp.object_compare(v1, v2);
  }

  bool not_equiv(const value_type& v1, const value_type& v2) const
  {
    const auto& comp = key_comp();
    return comp(v1, v2) or comp(v2, v1);
  }

  bool equiv(const value_type& v1, const value_type& v2) const
  {
    return not this->not_equiv(v1, v2);
  }

  bool not_equiv(const non_oriented_value_type& v1, const non_oriented_value_type& v2) const
  {
    const auto& comp = key_comp().object_compare;
    return comp(v1, v2) or comp(v2, v1);
  }

  bool equiv(const non_oriented_value_type& v1, const non_oriented_value_type& v2) const
  {
    return not this->not_equiv(v1, v2);
  }

};

template <typename Content, typename Comp, typename Alloc>
Chain<Content, Comp, Alloc> operator*(const RelativeOrientation& ro, const Chain<Content, Comp, Alloc>& chain)
{
  switch(ro)
  {
    case pos:
      return chain;
    case neg:
      return -chain;
    default:
      return Chain<Content, Comp, Alloc>{};
  }
}

template <typename Content, typename Comp, typename Alloc>
Chain<Content, Comp, Alloc> operator*(const RelativeOrientation& ro, Chain<Content, Comp, Alloc>&& chain)
{
  switch(ro)
  {
    case pos:
      return chain;
    case neg:
      {
        chain.reverse();
        return chain;
      }
    default:
      return Chain<Content, Comp, Alloc>{};
  }
}

template <typename Content, typename Comp, typename Alloc>
Chain<Content, Comp, Alloc> operator*(const Chain<Content, Comp, Alloc>& chain, const RelativeOrientation& ro)
{
  return ro * chain;
}

template <typename Content, typename Comp, typename Alloc>
Chain<Content, Comp, Alloc> operator*(Chain<Content, Comp, Alloc>&& chain, const RelativeOrientation& ro)
{
  return ro * std::move(chain);
}

template <typename Content>
Chain<Content> chain(std::initializer_list<OrientedObject<Content> > cont)
{
  return Chain<Content>(cont);
}

template <typename Iterator,
          typename ItContent = typename std::iterator_traits<Iterator>::value_type,
          typename Content = typename std::remove_cv<typename ItContent::content_t>::type,
          EnableIf<is_oriented_type<ItContent>::value> = 0>
Chain<Content> chain(Iterator first, Iterator last)
{
  return Chain<Content>(first, last);
}

template <typename Container,
          typename Content = typename std::remove_cv<typename Container::value_type>::type,
          EnableIf<is_oriented_type<Content>::value> = 0>
Chain<typename Content::content_t> chain(const Container& cont)
{
  return Chain<typename Content::content_t>(cont);
}

template <typename Container,
          typename Content = typename std::remove_cv<typename Container::value_type>::type,
          EnableIf<is_oriented_type<Content>::value> = 0>
Chain<typename Content::content_t> chain(Container&& cont)
{
  return Chain<typename Content::content_t>(cont);
}

template <typename Container,
          typename Content = typename std::remove_cv<typename Container::value_type>::type,
          typename ChainContent = typename remove_orientation<Content>::type>
Chain<ChainContent> chain(const RelativeOrientation& orient, const Container& cont)
{
  return Chain<ChainContent>(orient, cont);
}

/**
 * \class Set
 */
template <typename Content,
          typename Compare = std::less<Content>,
          typename Alloc = std::allocator<Content> >
class Set : public util::set_vector<Content, Compare, Alloc>
{
  protected:
  public:
    typedef util::set_vector<Content, Compare, Alloc> super_type;

    typedef typename super_type::value_type value_type;
    typedef typename super_type::iterator iterator;
    typedef typename super_type::const_iterator const_iterator;
    typedef typename super_type::reference reference;
    typedef typename super_type::const_reference const_reference;

    typedef Chain<Content> chain_t;

    Set()
      : super_type()
    { }

    Set(const value_type& init)
    {
      this->insert(init);
    }

    Set(std::initializer_list<value_type> init)
      : super_type(init)
    {
    }

    template <typename Container>
    explicit Set(const Container& init)
      : super_type(begin(init), end(init))
    {
    }

    template <typename Iterator>
    Set(Iterator first, Iterator last)
      : super_type(first, last)
    {
    }

    /**
     * Default copy constructor
     */
    Set(const Set&) = default;

    /**
     * Default move constructor
     */
    Set(Set&& other)
      : super_type(std::move(other))
    { }

    chain_t operator+() const
    {
      chain_t result;
      for(const auto& c: *this)
        result.insert(+c);
      return result;
    }

    chain_t operator-() const
    {
      chain_t result;
      for(const auto& c: *this)
        result.insert(-c);
      return result;
    }

    Set& operator=(const Set&) = default;
    Set& operator=(Set&&) = default;

    Set operator&(const Set& other) const
    {
      Set result;
      for(const auto& c: other)
        if(this->contains(c))
          result.insert(c);
      return result;
    }

    Set& operator&=(const Set& other)
    {
      for(iterator it = this->begin() ; it != this->end() ; )
      {
        if(!other.contains(*it))
          it = this->erase(it);
        else
          ++it;
      }
      return *this;
    }

    Set operator|(const Set& other) const
    {
      Set result(*this);
      result.insert(other.begin(), other.end());
      return result;
    }

    Set& operator|=(const Set& other)
    {
      this->insert(other.begin(), other.end());
      return *this;
    }

    const_reference any() const { return *(this->begin()); }

    bool contains(const value_type& oc) const
    {
      return this->find(oc) != this->end();
    }
};

template <typename Content, typename Compare, typename Alloc>
const Set<Content>& set(const Set<Content,Compare,Alloc>& init)
{
  return init;
}

template <typename Content>
Set<Content> set(Set<Content>&& init)
{
  return init;
}

template <typename Content>
Set<Content> set(std::initializer_list<Content> init)
{
  return Set<Content>(init);
}

template <typename Container>
Set<detail::NonConstContent<Container>> set(const Container& cnt)
{
  using std::begin;
  using std::end;
  return Set<detail::NonConstContent<Container>>(begin(cnt), end(cnt));
}

template <typename Iterator>
Set<typename std::iterator_traits<Iterator>::value_type> set(const Iterator& first, const Iterator& last)
{
  return Set<typename std::iterator_traits<Iterator>::value_type>(first, last);
}

template <typename Content>
Chain<Content> operator*(const RelativeOrientation& ro, const Set<Content>& set)
{
  return Chain<Content>(ro, set);
}

template <typename Content>
Chain<Content> operator*(const Set<Content>& set, const RelativeOrientation& ro)
{
  return Chain<Content>(ro, set);
}

template <typename Content>
Set<Content> operator~(const Chain<Content>& chain)
{
  Set<Content> result;
  for(const auto& oc: chain)
    result.insert(~oc);
  return result;
}

template <typename Content>
QTextStream& operator<<(QTextStream& ts, const Set<Content>& b)
{
  ts << QString("{%1|").arg(Content::N);
  bool first = true;
  for(const Content& oc: b)
  {
    if(first)
      first = false;
    else
      ts << " ";
    ts << shortString(oc);
  }
  ts << "}";
  return ts;
}

template <typename Content>
QTextStream& operator<<(QTextStream& ts, const Chain<Content>& b)
{
  ts << QString("{%1|").arg(Content::N);
  bool first = true;
  for(const auto& oc: b)
  {
    if(first)
      first = false;
    else
      ts << " ";
    ts << shortString(oc);
  }
  ts << "}";
  return ts;
}

}

#endif // CHAIN_H

