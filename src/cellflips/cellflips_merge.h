#ifndef CELLFLIPS_MERGE_H
#define CELLFLIPS_MERGE_H

#include "cellflips/cellflips.h"
#include <type_traits>

namespace cellflips
{

namespace detail
{

template <typename Complex>
void mergeJointFlips(const true_type&, Complex&, const typename Complex::cell_t&, const typename Complex::cell_t&, const typename Complex::cell_t&)
{
  // Nothing to do as there is no such joint flips
}

template <typename Complex, int N1>
void mergeJointFlips(const false_type&, Complex& C,
                     const ncell_t<Complex, N1>& c1, const ncell_t<Complex, N1>& c2,
                     const ncell_t<Complex, N1>& nc)
{
  typedef ncell_flip_t<Complex,N1+2> l_cojoint_flip_t;
  auto new_cojoint_flips = std::vector<l_cojoint_flip_t>();
  auto del_cojoint_flips = std::vector<l_cojoint_flip_t*>();
  // First, deal with the N1+2 flips
  for(auto pfl: C.flipsFromJoint(c1))
  {
    auto fl = *pfl;
    fl.joint = nc;
    new_cojoint_flips.push_back(fl);
    del_cojoint_flips.push_back(pfl);
  }
  for(auto pfl: C.flipsFromJoint(c2))
    del_cojoint_flips.push_back(pfl);
  for(auto pdf: del_cojoint_flips)
    C.removeFlip(pdf);
  for(auto fl: new_cojoint_flips)
    C.addFlip(fl);
}

template <typename Complex, int N1>
void mergeJointFlips(Complex& C,
                     const ncell_t<Complex, N1>& c1, const ncell_t<Complex, N1>& c2,
                     const ncell_t<Complex, N1>& nc)
{
  mergeJointFlips(TestType<(Complex::N == N1)>(), C, c1, c2, nc);
}

}

/**
 * Merge two adjacent cells that share all their co-faces
 *
 * For two cells to be merged they need to be:
 *  - adjacent (i.e. neighbors and co-neighbors)
 *  - have a contiguous membrane (or the union is not homomorphic to a ball)
 *
 * \param c1 One of the cell to merge
 * \param c2 The other cell to merge
 * \param nc If non-null, this is the name of the new cell
 *
 * Algorithm:
 *
 * By principle, the two cells must share all their co-faces and their common faces must be contiguous. Otherwise,
 * the merge is simply not possible.
 *
 * Processing of the N+2 flips. When they exists (i.e. not for N-cells), the N+2 flips must be the same for c1 and c2.
 * We need to create a new set of N+2 flips from one of the sets, with the new cell, and remove both sets with c1
 * and c2.
 *
 * Processing of the N+1 flips. There are two kind of flips: < ? c1 c2 ? > and < ? [c1,c2] ? ? >. The first type of
 * flips must be removed and they allow us to identify the membrane between the two cells. The second kind must be
 * updated, replacing c1 or c2 with nc.
 *
 * Processing of the N flips. There are two kind of N-flips: < [c1,c2] f ? j > where f is in the membrane and where f is
 * not in the membrane. If f is in the membrane, the flip must be removed (i.e. we detach the membrane from the
 * rest) and the other face (if not in the membrane) should be associated to j and the cell. Otherwise, c1 or c2
 * must be replaced with nc. If addition, if c2 is oriented against c1, then a flip with c2 must be re-oriented. In
 * the end, all the junctions associated to a given joint, but to the "other" cell become adjacent.
 *
 * At last, we must delete the cells in the membrane.
 *
 * \returns the merged cell, which will have the same orientation as c1
 * \relates CellComplex
 */
template <typename Complex, int N1>
ncell_t<Complex,N1> merge(Complex& C, const ncell_t<Complex, N1>& c1, const ncell_t<Complex, N1>& c2,
                          ncell_t<Complex, N1> nc = ncell_t<Complex, N1>::null)
{
  QueryType Q;
  typedef ncell_t<Complex,N1> l_cell_t;
  typedef ncell_t<Complex,N1-1> l_face_t;
  typedef ncell_t<Complex,N1-2> l_joint_t;
  typedef ncell_flip_t<Complex,N1> l_cell_flip_t;
  typedef ncell_flip_t<Complex,N1+1> l_coface_flip_t;
  if(not nc) nc = l_cell_t();
  auto membrane = CellSet<l_face_t>{};
  auto faces_c1 = CellSet<l_face_t>{};
  auto faces_c2 = CellSet<l_face_t>{};

  RelativeOrientation c2orient = C.adjacentOrientation(c1, c2);

  // Update the flips of dimension N+2 (if any)
  detail::mergeJointFlips(C, c1, c2, nc);

  // Update the flips of dimension N+1, gathering information on the membrane at the same time
  auto coface_adjacent_flips = C.match(Q, c1, c2, Q);
  auto del_coface_flips = std::vector<l_coface_flip_t*>{};
  auto new_coface_flips = std::vector<l_coface_flip_t>{};
  for(auto pfl: coface_adjacent_flips)
  {
    membrane.insert(pfl->joint);
    C.removeFlip(pfl);
  }
  del_coface_flips.clear();
  // find which co-face fflips need to be added / removed
  auto mergeCofaceFlip = [&new_coface_flips, &del_coface_flips, &nc](l_coface_flip_t *pfl, const l_cell_t& c) {
    auto fl = *pfl;
    fl.replaceFace(c, nc);
    new_coface_flips.push_back(fl);
    del_coface_flips.push_back(pfl);
  };
  for(auto pfl: C.flipsFromFace(c1))
    mergeCofaceFlip(pfl, c1);
  for(auto pfl: C.flipsFromFace(c2))
    mergeCofaceFlip(pfl, c2);

  // Update N-flips
  auto del_cell_flips = std::vector<l_cell_flip_t*>();
  auto new_cell_flips = std::vector<l_cell_flip_t>();

  auto c1_new_adjacent = std::unordered_map<l_joint_t, std::vector<l_cell_flip_t>>();
  auto c2_new_adjacent = std::unordered_map<l_joint_t, std::vector<l_face_t>>();

  for(auto pfl: C.flipsFromCell(c1))
  {
    del_cell_flips.push_back(pfl);
    bool mem_face1 = membrane.contains(pfl->face1);
    bool mem_face2 = membrane.contains(pfl->face2);
    if(not mem_face1) faces_c1.insert(pfl->face1);
    if(not mem_face2) faces_c1.insert(pfl->face2);
    if(mem_face1 or mem_face2)
    {
      auto fl = *pfl;
      fl.cell = nc;
      if(not mem_face1)
        fl.face2 = Q;
      else if(not mem_face2)
        fl.face1 = Q;
      c1_new_adjacent[fl.joint].push_back(fl);
      out << "Adding flip " << fl << " for c1" << endl;
    }
    else
    {
      auto fl = *pfl;
      fl.cell = nc;
      new_cell_flips.push_back(fl);
    }
  }

  for(auto pfl: C.flipsFromCell(c2))
  {
    del_cell_flips.push_back(pfl);
    bool mem_face1 = membrane.contains(pfl->face1);
    bool mem_face2 = membrane.contains(pfl->face2);
    if(not mem_face1) faces_c2.insert(pfl->face1);
    if(not mem_face2) faces_c2.insert(pfl->face2);
    if(mem_face1 or mem_face2)
    {
      if(not mem_face1)
        c2_new_adjacent[pfl->joint].push_back(pfl->face1);
      else if(not mem_face2)
        c2_new_adjacent[pfl->joint].push_back(pfl->face2);
      out << "Adding face " << c2_new_adjacent[pfl->joint].back() << " to joint " << pfl->joint << " for c2" << endl;
    }
    else
    {
      auto fl = *pfl;
      fl *= c2orient;
      fl.cell = nc;
      new_cell_flips.push_back(fl);
    }
  }

  // Add new adjacent cells
  for(const auto& val: c1_new_adjacent)
  {
    out << "Cheching joint " << val.first << endl;
    auto found = c2_new_adjacent.find(val.first);
    if(found != c2_new_adjacent.end())
    {
      out << " ... Found joint in c2!" << endl;
      for(auto fl: val.second)
      {
        bool update_second = bool(fl.face1);
        for(auto f2: found->second)
        {
          out << "   Updating " << fl << " with " << f2 << endl;
          if(update_second)
            fl.face2 = f2;
          else
            fl.face1 = f2;
          out << "      => " << fl << endl;
          C.addFlip(fl);
        }
      }
    }
  }

  // Delete co-face flips
  for(auto pfl: del_coface_flips)
    C.removeFlip(pfl);

  // Add coface flips
  for(auto fl: new_coface_flips)
    C.addFlip(fl);

  // Delete cell flips
  for(auto pfl: del_cell_flips)
    C.removeFlip(pfl);

  // Add cell flips
  for(auto fl: new_cell_flips)
    C.addFlip(fl);

  // Update orientations
  for(auto f: faces_c1)
  {
    auto re = C.relativeOrientation(c1, f);
    C.removeRelativeOrientation(c1, f);
    C.setRelativeOrientation(nc, f, re);
  }
  for(auto f: faces_c2)
  {
    auto re = C.relativeOrientation(c2, f);
    C.removeRelativeOrientation(c2, f);
    C.setRelativeOrientation(nc, f, c2orient*re);
  }

  // Delete the membrane
  for(auto f: membrane)
    removeCell(C, f);

  return nc;
}

namespace detail
{

template <typename Complex,
          EnableIf<(Complex::N>1)> = 0>
void collapseEdge_remove3Cells(Complex& C, ncell_t<Complex,1> e)
{
  typedef ncell_flip_t<Complex,3> l_cell_flip_t;
  QueryType Q;
  const auto& tds = C.match(Q, Q, Q, e);
  auto to_delete = std::vector<l_cell_flip_t*>(tds.begin(), tds.end());
  for(auto pfl: to_delete)
    C.removeFlip(pfl);
}

template <typename Complex,
          EnableIf<(Complex::N == 1)> = 0>
void collapseEdge_remove3Cells(Complex& , ncell_t<Complex,1> )
{
  // Nothing to do here
}

}

/**
 * Collapse an edge into a vertex in a cell complex.
 *
 * All the faces containing the edge must have at least 2 other edges. Otherwise, the resulting face would have a unique
 * edge as contour, and that's not valid.
 *
 * \param C Cell complex containing the edge
 * \param e Edge to collapse
 * \param v If provided, vertex replacing the edge. The vertex must not be in the cell complex already.
 *
 * Algorithm:
 *
 *  - 3 flips : <* * * e> --> to delete
 *  - 2 flips : <f e e1 v1> and <f e2 e v2> --> <f e2 e1 nv>
 *  - 1 flips: <e v1 v2 _> -> delete
 *             <e1 v1 v1' _> -> <e1 nv v1' _>
 *             <e2 v2 v2' _> -> <e2 nv v2' _>
 *
 * Orientations:
 *  - s(e, *) -> delete
 *  - s(*,e) -> delete
 *  - s(e1,v1) = s(e1,nv)
 *  - s(e2,v2) = s(e2,nv)
 */
template <typename Complex>
ncell_t<Complex,0> collapseEdge(Complex& C, ncell_t<Complex,1> e, ncell_t<Complex,0> v = ncell_t<Complex,0>::null)
{
  typedef ncell_t<Complex,0> l_vertex_t;
  typedef ncell_t<Complex,1> l_edge_t;
  typedef ncell_t<Complex,2> l_face_t;
  typedef ncell_flip_t<Complex,2> l_face_flip_t;
  typedef ncell_flip_t<Complex,1> l_edge_flip_t;
  if(v and C.contains(v))
  {
    C.error = Complex::CELL_ALREADY_IN_COMPLEX;
    return l_vertex_t::null;
  }
  auto nv = l_vertex_t(v, true);
  auto v1 = l_vertex_t(0), v2 = l_vertex_t(0);
  tie(v1, v2) = C.orderedVertices(+e);

  QueryType Q;

  // Remove 3-cell flips if they exist
  detail::collapseEdge_remove3Cells(C, e);

  std::unordered_map<l_face_t, std::pair<l_face_flip_t*, l_edge_t> > face_to_replace;
  CellSet<l_face_t> cobounds;
  std::vector<l_face_flip_t*> face_to_delete;

  // Keep information on boundaries and collect 2-cell flips information

  Chain<l_edge_t> edges_v1, edges_v2;

  for(auto pfl: C.match(Q, e, Q, Q))
  {
    cobounds.insert(pfl->cell);
    l_edge_t oe = pfl->otherFace(e);
    if(pfl->joint == v1)
      edges_v1.insert(C.relativeOrientation(oe, v1)*oe);
    else
      edges_v2.insert(C.relativeOrientation(oe, v2)*oe);
    l_face_t f = pfl->cell;
    auto found = face_to_replace.find(f);
    if(found != face_to_replace.end())
    {
      found->second.second = oe;
      face_to_delete.push_back(pfl);
    }
    else
    {
      face_to_replace[f] = {pfl, l_edge_t(0)};
    }
  }

  // Delete face flips
  for(auto pfl: face_to_delete)
    C.removeFlip(pfl);

  // Replace other flips
  for(auto rep: face_to_replace)
    C.updateFlip(rep.second.first, rep.second.second, e);

  // Collect 1-cell flip information

  std::vector<l_edge_flip_t*> edge_to_delete;
  std::vector<l_edge_flip_t*> edge_to_replace_v1, edge_to_replace_v2;

  for(auto pfl: C.match(Q, v1, Q, Q))
  {
    if(pfl->otherFace(v1) == v2)
      edge_to_delete.push_back(pfl);
    else
      edge_to_replace_v1.push_back(pfl);
  }

  for(auto pfl: C.match(Q, v2, Q, Q))
    if(pfl->otherFace(v2) != v1)
      edge_to_replace_v2.push_back(pfl);

  for(auto pfl: edge_to_delete)
    C.removeFlip(pfl);

  for(auto pfl: edge_to_replace_v1)
    C.updateFlip(pfl, nv, v1);

  for(auto pfl: edge_to_replace_v2)
    C.updateFlip(pfl, nv, v2);

  // At last, update orientations
  C.removeRelativeOrientation(e, v1);
  C.removeRelativeOrientation(e, v2);

  for(auto f: cobounds)
    C.removeRelativeOrientation(f, e);

  for(auto oe1: edges_v1)
    C.setRelativeOrientation(~oe1, nv, oe1.orientation());

  for(auto oe2: edges_v2)
    C.setRelativeOrientation(~oe2, nv, oe2.orientation());

  return nv;
}


}

#endif // CELLFLIPS_MERGE_H

