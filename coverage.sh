#!/bin/bash
init_coverage() {
  echo "Initialize coverage"
  lcov --zerocounters "${lcov_dirs[@]}" || exit $?
  echo "Capture initial coverage for folders:"
  lcov --capture --initial "${lcov_dirs[@]}" --output-file .coverage.base || exit $?
}

compute_coverage() {
  echo "Capture coverage"
  lcov --capture "${lcov_dirs[@]}" --output-file .coverage.run || exit $?
  echo "Combine with initial values"
  lcov --add-tracefile .coverage.base --add-tracefile .coverage.run --output-file .coverage.total || exit $?
}

generate_html() {
  genhtml --no-branch-coverage --output-directory cov .coverage.total || exit $?
}

clean_coverage() {
  rm -f .coverage.run .coverage.base .coverage.total
}

if [[ $# < 2 ]]; then
  echo "Usage: $0 TEST_FOLDER TEST_NAME_1 [TEST_NAME_2 ... TEST_NAME_N]" > 2
  exit 2
fi

TEST_FOLDER="$1"
shift
TESTS="$@"

CUR_DIR=$PWD

lcov_dirs=()
for TEST in ${TESTS}; do
  lcov_dirs+=(--directory "$CUR_DIR/${TEST_FOLDER}/CMakeFiles/${TEST}.dir")
done


make || exit $?

init_coverage || exit $?

make test || exit $?

compute_coverage || exit $?
generate_html || exit $?
clean_coverage

