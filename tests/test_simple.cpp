#include "figures.h"
#include <sstream>
#include <cellflips/cellflips_lazy.h>

#include <QTextStream>
#include <stdio.h>
QTextStream out(stdout);

static cellflips::QueryType Q;
using cellflips::bounds;
using cellflips::cobounds;
using cellflips::OrientedObject;
using cellflips::Chain;
using cellflips::Set;

template <typename T>
std::string toString(const std::vector<T>& vs)
{
  std::ostringstream ss;
  ss << "{";
  for(auto it = begin(vs) ; it != end(vs) ; )
  {
    ss << *it;
    if(++it != end(vs))
      ss << ",";
  }
  ss << "}";
  return ss.str();
}

template <typename Content>
::testing::AssertionResult sameCyclicVector(std::vector<Content> v1, std::vector<Content> v2)
{
  auto found = std::find(v1.begin(), v1.end(), v2[0]);
  if(found == v1.end()) return ::testing::AssertionFailure() << v2[0] << " not found in expected ";
  std::rotate(v1.begin(), found, v1.end());
  if(v1 != v2) return ::testing::AssertionFailure() << " result (" << toString(v2) << ") doesn't match expected.";
  return ::testing::AssertionSuccess();
}

TEST(construction, empty_structure1d)
{
  CellComplex1d C;
  EXPECT_TRUE(C.empty());
  EXPECT_EQ(C.nbVertices(), 0u);
  EXPECT_EQ(C.nbCells(), 0u);
}

TEST(construction, empty_structure2d)
{
  CellComplex2d C;
  EXPECT_TRUE(C.empty());
  EXPECT_EQ(C.nbVertices(), 0u);
  EXPECT_EQ(C.nbEdges(), 0u);
  EXPECT_EQ(C.nbCells(), 0u);
}

TEST(construction, empty_structure3d)
{
  CellComplex2d C;
  EXPECT_TRUE(C.empty());
  EXPECT_EQ(C.nbVertices(), 0u);
  EXPECT_EQ(C.nbEdges(), 0u);
  EXPECT_EQ(C.nbFaces(), 0u);
  EXPECT_EQ(C.nbCells(), 0u);
}

struct TestChain : public ::testing::Test
{
  struct content_t
  {
    enum
    {
      N = 1
    };

    content_t(int i = 0)
      : value(i)
    { }

    content_t(const content_t&) = default;
    content_t(content_t&&) = default;

    content_t& operator=(const content_t&) = default;
    content_t& operator=(content_t&&) = default;

    int value;

    OrientedObject<content_t> operator+() const
    {
      return OrientedObject<content_t>(*this, cellflips::pos);
    }

    OrientedObject<content_t> operator-() const
    {
      return OrientedObject<content_t>(*this, cellflips::neg);
    }
  };

  TestChain()
  {
    for(size_t i = 0 ; i < os.size() ; ++i)
      os[i].value = i;
  }

  std::array<content_t, 10> os;
};

bool operator<(const TestChain::content_t& v1, const TestChain::content_t& v2)
{
  return v1.value < v2.value;
}

bool operator>(const TestChain::content_t& v1, const TestChain::content_t& v2)
{
  return v1.value > v2.value;
}

bool operator>=(const TestChain::content_t& v1, const TestChain::content_t& v2)
{
  return v1.value >= v2.value;
}

bool operator<=(const TestChain::content_t& v1, const TestChain::content_t& v2)
{
  return v1.value <= v2.value;
}

bool operator==(const TestChain::content_t& v1, const TestChain::content_t& v2)
{
  return v1.value == v2.value;
}

bool operator!=(const TestChain::content_t& v1, const TestChain::content_t& v2)
{
  return v1.value != v2.value;
}

template <typename T, typename Traits>
std::basic_ostream<T,Traits>& operator<<(std::basic_ostream<T,Traits>& ss, const TestChain::content_t& v)
{
  ss << "<" << v.value << ">";
  return ss;
}

template <typename T, typename Traits>
std::basic_ostream<T,Traits>& operator<<(std::basic_ostream<T,Traits>& ss, const OrientedObject<TestChain::content_t>& v)
{
  ss << "<" << v.orientation() << (~v).value << ">";
  return ss;
}

TEST_F(TestChain, construction)
{
  auto cs = chain({+os[1], -os[2]});
  EXPECT_EQ(2, cs.size());

  EXPECT_TRUE((cellflips::invalid * cs).empty());
  EXPECT_TRUE((cs * cellflips::invalid).empty());

  EXPECT_EQ(chain({-os[1], +os[2]}), -cs);
  EXPECT_EQ(chain({+os[1], -os[2]}), +cs);

  EXPECT_EQ(chain({+os[1], -os[2], -os[3]}), -chain({-os[1], +os[2], +os[3]}));
  EXPECT_EQ(chain({-os[1], +os[2], +os[3]}), +chain({-os[1], +os[2], +os[3]}));

  EXPECT_EQ(chain({+os[1], -os[2], -os[3]}), cellflips::neg * chain({-os[1], +os[2], +os[3]}));
  EXPECT_EQ(chain({-os[1], +os[2], +os[3]}), cellflips::pos * chain({-os[1], +os[2], +os[3]}));
  EXPECT_TRUE((cellflips::invalid * chain({-os[1], +os[2], +os[3]})).empty());

  EXPECT_TRUE(cs.contains(cs.any()));

  cs.insert(-os[1]);
  EXPECT_EQ(1, cs.size());
}

TEST_F(TestChain, Union)
{
  auto cs = chain({+os[1], -os[2]});
  {
    auto cu = cs | chain({+os[3], -os[4], +os[5]});
    EXPECT_EQ(5, cu.size());
  }

  {
    auto cu = cs | chain({+os[3], -os[4], +os[5], -os[2]});
    EXPECT_EQ(6, cu.size());
    EXPECT_EQ(chain({+os[1], -os[2], -os[2], +os[3], -os[4], +os[5]}), cu);
  }

  {
    auto cu = cs | chain({+os[3], -os[4], +os[5], +os[2]});
    EXPECT_EQ(4, cu.size());
    EXPECT_EQ(chain({+os[1], +os[3], -os[4], +os[5]}), cu);
  }

  {
    auto cu = cs;
    cu |= chain({+os[2], -os[3]});
    cu |= chain({+os[2], -os[3]});
    EXPECT_EQ(4, cu.size());
    EXPECT_EQ(chain({+os[1], +os[2], -os[3], -os[3]}), cu);

    cu.clear();
    EXPECT_TRUE(cu.empty());
  }

  {
    auto cu = Chain<content_t>{};
    auto res = cu | Chain<content_t>{};
    EXPECT_TRUE(res.empty());

    res = cu | chain({+os[1], +os[1], -os[5]});
    EXPECT_EQ(chain({+os[1], +os[1], -os[5]}), res);

    EXPECT_TRUE(res.find(os[8]) == res.end());
    EXPECT_TRUE(res.find(os[3]) == res.end());
  }

  {
    auto c1 = chain({+os[1], +os[4], +os[8]});
    auto c2 = chain({+os[2], -os[4], +os[6], +os[8], +os[10]});
    auto c = c1 | c2;
    EXPECT_EQ(chain({+os[1], +os[8], +os[2], +os[6], +os[8], +os[10]}), c);

    c = c2 | c1;
    EXPECT_EQ(chain({+os[1], +os[8], +os[2], +os[6], +os[8], +os[10]}), c);
  }

}

TEST_F(TestChain, Intersection)
{
  auto cs = chain({+os[1], -os[2]});
  {
    auto cu = cs & chain({+os[3], -os[4], +os[5]});
    EXPECT_TRUE(cu.empty());
  }

  {
    auto cu = cs & chain({+os[3], -os[4], +os[5], -os[2]});
    EXPECT_EQ(1, cu.size());
  }

  {
    auto cu = cs & chain({+os[3], -os[4], +os[5], +os[2]});
    EXPECT_TRUE(cu.empty());
  }

  {
    auto cu = cs & chain({+os[3], -os[4], +os[5], +os[2], +os[1], +os[1]});
    EXPECT_EQ(1, cu.size());
  }

}

TEST_F(TestChain, single_modif)
{
  auto cs = chain({+os[1], -os[2]});

  ASSERT_FALSE(cs.insert(+os[2]).second);
  ASSERT_TRUE(cs.insert(+os[2]).second);
  ASSERT_TRUE(cs.insert(+os[2]).second);
  ASSERT_TRUE(cs.insert(+os[2]).second);

  ASSERT_TRUE(cs.contains(+os[2]));

  ASSERT_EQ(3, cs.erase(os[2]));

  ASSERT_TRUE(cs.insert(-os[2]).second);
  ASSERT_TRUE(cs.insert(-os[2]).second);

  ASSERT_EQ(2, cs.erase(os[2]));

  ASSERT_FALSE(cs.contains(os[2]));
  ASSERT_FALSE(cs.contains(-os[2]));
  ASSERT_FALSE(cs.contains(+os[2]));
}

struct TestSet : public ::testing::Test
{
  struct content_t
  {
    enum
    {
      N = 1
    };

    content_t(int i = 0)
      : value(i)
    { }

    content_t(const content_t&) = default;
    content_t(content_t&&) = default;

    content_t& operator=(const content_t&) = default;
    content_t& operator=(content_t&&) = default;

    int value;

    OrientedObject<content_t> operator+() const
    {
      return OrientedObject<content_t>(*this, cellflips::pos);
    }

    OrientedObject<content_t> operator-() const
    {
      return OrientedObject<content_t>(*this, cellflips::neg);
    }
  };

  TestSet()
  {
    for(size_t i = 0 ; i < os.size() ; ++i)
      os[i].value = i;
  }

  std::array<content_t, 10> os;
};

bool operator<(const TestSet::content_t& v1, const TestSet::content_t& v2)
{
  return v1.value < v2.value;
}

bool operator>(const TestSet::content_t& v1, const TestSet::content_t& v2)
{
  return v1.value > v2.value;
}

bool operator>=(const TestSet::content_t& v1, const TestSet::content_t& v2)
{
  return v1.value >= v2.value;
}

bool operator<=(const TestSet::content_t& v1, const TestSet::content_t& v2)
{
  return v1.value <= v2.value;
}

bool operator==(const TestSet::content_t& v1, const TestSet::content_t& v2)
{
  return v1.value == v2.value;
}

bool operator!=(const TestSet::content_t& v1, const TestSet::content_t& v2)
{
  return v1.value != v2.value;
}


TEST_F(TestSet, construction)
{
  auto cs = set({os[1], os[2]});
  EXPECT_EQ(2, cs.size());

  EXPECT_TRUE(cs.contains(cs.any()));

  cs.insert(os[1]);
  EXPECT_EQ(2, cs.size());

  cs.insert(os[5]);
  EXPECT_EQ(3, cs.size());

  cs.erase(os[5]);
  EXPECT_EQ(2, cs.size());
}

struct TestBrokenCircle : public ::testing::Test, public BrokenCircle
{
  virtual void SetUp()
  {
    create();
  }

  void testConstruction()
  {
    EXPECT_EQ(4, CC.nbVertices());
    EXPECT_EQ(4, CC.nbCells());

    // Test bounds of vertices
    for(auto v: CC.vertices())
      EXPECT_EQ(set({CC._}), CC.bounds(v));

    EXPECT_EQ(v1, CC.flip(c1, v2, CC._));
    EXPECT_EQ(v2, CC.flip(c1, v1, CC._));
    EXPECT_EQ(v2, CC.flip(c2, v3, CC._));
    EXPECT_EQ(v3, CC.flip(c2, v2, CC._));
    EXPECT_EQ(v3, CC.flip(c3, v4, CC._));
    EXPECT_EQ(v4, CC.flip(c3, v3, CC._));
    EXPECT_EQ(v4, CC.flip(c4, v1, CC._));
    EXPECT_EQ(v1, CC.flip(c4, v4, CC._));

    EXPECT_EQ(c1, CC.flip(CC.T, c2, v2));
    EXPECT_EQ(c2, CC.flip(CC.T, c1, v2));
    EXPECT_EQ(c2, CC.flip(CC.T, c3, v3));
    EXPECT_EQ(c3, CC.flip(CC.T, c2, v3));
    EXPECT_EQ(c3, CC.flip(CC.T, c4, v4));
    EXPECT_EQ(c4, CC.flip(CC.T, c3, v4));
    EXPECT_EQ(c4, CC.flip(CC.T, c1, v1));
    EXPECT_EQ(c1, CC.flip(CC.T, c4, v1));

    EXPECT_EQ(vertex_t::null, CC.flip(c3, v2, CC._));
    EXPECT_EQ(cell_t::null, CC.flip(CC.T, c3, v1));

    EXPECT_EQ(set({v1, v2}), CC.bounds(c1));
    EXPECT_EQ(set({v2, v3}), CC.bounds(c2));
    EXPECT_EQ(set({v3, v4}), CC.bounds(c3));
    EXPECT_EQ(set({v4, v1}), CC.bounds(c4));

    EXPECT_EQ(set({v1, v2}), set(bounds(CC,c1)));
    EXPECT_EQ(set({v2, v3}), set(bounds(CC,c2)));
    EXPECT_EQ(set({v3, v4}), set(bounds(CC,c3)));
    EXPECT_EQ(set({v4, v1}), set(bounds(CC,c4)));

    EXPECT_EQ(chain({-v1, +v2}), CC.boundary(+c1));
    EXPECT_EQ(chain({-v2, +v3}), CC.boundary(+c2));
    EXPECT_EQ(chain({-v3, +v4}), CC.boundary(+c3));
    EXPECT_EQ(chain({-v4, +v1}), CC.boundary(+c4));

    EXPECT_EQ(chain({-v1, +v2}), chain(boundary(CC, +c1)));
    EXPECT_EQ(chain({-v2, +v3}), chain(boundary(CC, +c2)));
    EXPECT_EQ(chain({-v3, +v4}), chain(boundary(CC, +c3)));
    EXPECT_EQ(chain({-v4, +v1}), chain(boundary(CC, +c4)));

    // Check orientations
    EXPECT_EQ(cellflips::pos, CC.relativeOrientation(CC.T, c1));
    EXPECT_EQ(cellflips::pos, CC.relativeOrientation(c1, v2));
    EXPECT_EQ(cellflips::neg, CC.relativeOrientation(c1, v1));
    EXPECT_EQ(cellflips::pos, CC.relativeOrientation(v1, CC._));
    EXPECT_EQ(cellflips::pos, CC.relativeOrientation(c1, c2));
    EXPECT_EQ(cellflips::neg, CC.relativeOrientation(+v1, +v2));
    EXPECT_EQ(cellflips::pos, CC.adjacentOrientation(-c2, -c3));
    EXPECT_EQ(cellflips::neg, CC.adjacentOrientation(v2, v3));
  }
};

TEST_F(TestBrokenCircle, construction)
{
  testConstruction();

  EXPECT_NE(vertex_t::null, CC.anyVertex());
  EXPECT_NE(cell_t::null, CC.anyCell());
  EXPECT_EQ(CC._, CC.any<-1>());
  EXPECT_EQ(CC.T, CC.any<2>());

  EXPECT_EQ(set({v1, v2, v3, v4}), set(CC.vertices()));
  EXPECT_EQ(set({c1, c2, c3, c4}), set(CC.cells()));
}

TEST_F(TestBrokenCircle, basic_edition)
{
  ASSERT_EQ(vertex_t::null, C.addVertex(v1));
  ASSERT_EQ(vertex_t::null, C.addVertex(v2));
  ASSERT_FALSE(C.removeVertex(v1));
  ASSERT_FALSE(C.removeVertex(v2));
}

TEST_F(TestBrokenCircle, simple_queries)
{
  EXPECT_TRUE(CC.hasCell(CC._));
  EXPECT_TRUE(CC.hasCell(CC.T));
  EXPECT_TRUE(CC.hasVertex(v1));
  EXPECT_TRUE(CC.hasCell(c2));
  EXPECT_FALSE(CC.hasVertex(vertex_t()));
  EXPECT_FALSE(CC.hasEdge(cell_t()));

  EXPECT_EQ(c1, CC.edge(v1, v2));
  EXPECT_NE(c3, CC.edge(v2, v3));
  EXPECT_EQ(cell_t::null, CC.edge(v1, v3));
}

TEST_F(TestBrokenCircle, double_construction)
{
  C.clear();
  create();
  testConstruction();
}

TEST_F(TestBrokenCircle, bounds)
{
  EXPECT_EQ(set({c4, c1}), CC.cobounds(v1));
  EXPECT_EQ(set({c1, c2}), CC.cobounds(v2));
  EXPECT_EQ(set({c2, c3}), CC.cobounds(v3));
  EXPECT_EQ(set({c3, c4}), CC.cobounds(v4));

  EXPECT_EQ(3, CC.nbBounds(set({c1, c2})));
  EXPECT_EQ(4, CC.nbBounds(set({c1, c3})));

  EXPECT_TRUE(CC.isBound(c1, v1));
  EXPECT_FALSE(CC.isBound(c1, v3));

  EXPECT_EQ(2, CC.nbCobounds(v1));
  EXPECT_EQ(3, CC.nbCobounds(set({v1,v2})));
}

TEST_F(TestBrokenCircle, neighbors)
{
  EXPECT_EQ(2, CC.nbNeighbors(v1));
  EXPECT_EQ(2, CC.nbNeighbors(c1));

  EXPECT_EQ(set({v4, v2}), CC.neighbors(v1));
  EXPECT_EQ(set({c4, c2}), CC.neighbors(c1));

  EXPECT_TRUE(CC.areNeighbors(v3, v4));
  EXPECT_TRUE(CC.areNeighbors(c3, c4));

  EXPECT_TRUE(set({v2, v4}).contains(CC.anyNeighbor(v1)));
  EXPECT_TRUE(set({c2, c4}).contains(CC.anyNeighbor(c1)));
}

TEST_F(TestBrokenCircle, ordered_topology)
{
  EXPECT_EQ(chain({+c4, -c1}), CC.coboundary(+v1));
  EXPECT_EQ(chain({+c1, -c2}), CC.coboundary(+v2));
  EXPECT_EQ(chain({+c2, -c3}), CC.coboundary(+v3));
  EXPECT_EQ(chain({+c3, -c4}), CC.coboundary(+v4));

  EXPECT_EQ(chain({+c4, -c1}), chain(coboundary(CC,+v1)));
  EXPECT_EQ(chain({+c1, -c2}), chain(coboundary(CC,+v2)));
  EXPECT_EQ(chain({+c2, -c3}), chain(coboundary(CC,+v3)));
  EXPECT_EQ(chain({+c3, -c4}), chain(coboundary(CC,+v4)));

  EXPECT_EQ(chain({+c1, +c3}), CC.orientedAdjacentCells(+c2));
  EXPECT_EQ(chain({-c2, -c4}), CC.orientedAdjacentCells(-c3));
}

TEST_F(TestBrokenCircle, updateVertexFlip)
{
  auto nv = C.addVertex();
  EXPECT_NE(vertex_t::null, nv);

  {
    auto flips = C.match(Q, v2, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nv, v2));
  }

  {
    auto flips = C.match(Q, Q, Q, v2);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nv));
  }

  C.removeVertex(v2);
  ASSERT_TRUE(computeOrientations(C));
  v2 = nv;
  testConstruction();
}

TEST_F(TestBrokenCircle, updateCellFlip)
{
  auto nc = cell_t();
  EXPECT_NE(cell_t::null, nc);

  {
    auto flips = C.match(Q, c2, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nc, c2));
  }

  {
    auto flips = C.match(c2, Q, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nc));
  }

  ASSERT_TRUE(computeOrientations(C));
  c2 = nc;
  testConstruction();
}

TEST_F(TestBrokenCircle, global_queries)
{
  EXPECT_FALSE(CC.border(v1));
  EXPECT_FALSE(CC.border(v2));
  EXPECT_FALSE(CC.border(v3));
  EXPECT_FALSE(CC.border(v4));
  EXPECT_FALSE(CC.border(c1));
  EXPECT_FALSE(CC.border(c2));
  EXPECT_FALSE(CC.border(c3));
  EXPECT_FALSE(CC.border(c4));
}

struct TestBrokenLine : public ::testing::Test, public BrokenLine
{
  virtual void SetUp()
  {
    create();
  }

  void testConstruction()
  {
    EXPECT_EQ(4, CC.nbVertices());
    EXPECT_EQ(3, CC.nbCells());

    // Test bounds of vertices
    for(auto v: CC.vertices())
      EXPECT_EQ(set({CC._}), CC.bounds(v));

    EXPECT_EQ(v1, CC.flip(c1, v2, CC._));
    EXPECT_EQ(v2, CC.flip(c1, v1, CC._));
    EXPECT_EQ(v2, CC.flip(c2, v3, CC._));
    EXPECT_EQ(v3, CC.flip(c2, v2, CC._));
    EXPECT_EQ(v3, CC.flip(c3, v4, CC._));
    EXPECT_EQ(v4, CC.flip(c3, v3, CC._));

    EXPECT_EQ(c1, CC.flip(CC.T, c2, v2));
    EXPECT_EQ(c2, CC.flip(CC.T, c1, v2));
    EXPECT_EQ(c2, CC.flip(CC.T, c3, v3));
    EXPECT_EQ(c3, CC.flip(CC.T, c2, v3));

    EXPECT_EQ(vertex_t::null, CC.flip(c3, v2, CC._));
    EXPECT_EQ(cell_t::null, CC.flip(CC.T, c3, v1));

    EXPECT_EQ(set({v1, v2}), CC.bounds(c1));
    EXPECT_EQ(set({v2, v3}), CC.bounds(c2));
    EXPECT_EQ(set({v3, v4}), CC.bounds(c3));

    EXPECT_EQ(chain({-v1, +v2}), CC.boundary(+c1));
    EXPECT_EQ(chain({-v2, +v3}), CC.boundary(+c2));
    EXPECT_EQ(chain({-v3, +v4}), CC.boundary(+c3));

    // Check orientations
    EXPECT_EQ(cellflips::pos, CC.relativeOrientation(CC.T, c1));
    EXPECT_EQ(cellflips::pos, CC.relativeOrientation(c1, v2));
    EXPECT_EQ(cellflips::neg, CC.relativeOrientation(c1, v1));
    EXPECT_EQ(cellflips::pos, CC.relativeOrientation(v1, CC._));
    EXPECT_EQ(cellflips::pos, CC.relativeOrientation(c1, c2));
    EXPECT_EQ(cellflips::neg, CC.relativeOrientation(v1, v2));
    EXPECT_EQ(cellflips::pos, CC.adjacentOrientation(c2, c3));
    EXPECT_EQ(cellflips::neg, CC.adjacentOrientation(v2, v3));
  }
};

TEST_F(TestBrokenLine, construction)
{
  testConstruction();

  EXPECT_NE(vertex_t::null, CC.anyVertex());
  EXPECT_NE(cell_t::null, CC.anyCell());
  EXPECT_EQ(CC._, CC.any<-1>());
  EXPECT_EQ(CC.T, CC.any<2>());

  EXPECT_EQ(set({v1, v2, v3, v4}), set(CC.vertices()));
  EXPECT_EQ(set({c1, c2, c3}), set(CC.cells()));
}

TEST_F(TestBrokenLine, simple_queries)
{
  EXPECT_TRUE(CC.contains(CC._));
  EXPECT_TRUE(CC.contains(CC.T));
  EXPECT_TRUE(CC.hasVertex(v1));
  EXPECT_TRUE(CC.hasCell(c2));
  EXPECT_FALSE(CC.hasVertex(vertex_t()));
  EXPECT_FALSE(CC.hasEdge(cell_t()));

  EXPECT_EQ(c1, CC.edge(v1, v2));
  EXPECT_NE(c3, CC.edge(v2, v3));
  EXPECT_EQ(cell_t::null, CC.edge(v1, v3));
}

TEST_F(TestBrokenLine, double_construction)
{
  C.clear();
  create();
  testConstruction();
}

TEST_F(TestBrokenLine, bounds)
{
  EXPECT_EQ(set({c1}), CC.cobounds(v1));
  EXPECT_EQ(set({c1, c2}), CC.cobounds(v2));
  EXPECT_EQ(set({c2, c3}), CC.cobounds(v3));
  EXPECT_EQ(set({c3}), CC.cobounds(v4));
}

TEST_F(TestBrokenLine, ordered_topology)
{
  EXPECT_EQ(chain({-c1}), CC.coboundary(+v1));
  EXPECT_EQ(chain({+c1, -c2}), CC.coboundary(+v2));
  EXPECT_EQ(chain({+c2, -c3}), CC.coboundary(+v3));
  EXPECT_EQ(chain({+c3}), CC.coboundary(+v4));
}

TEST_F(TestBrokenLine, updateVertexFlip)
{
  auto nv = C.addVertex();
  EXPECT_NE(vertex_t::null, nv);

  {
    auto flips = C.match(Q, v1, Q, Q);
    for(auto pfl: flips)
    {
      ASSERT_FALSE(C.updateFlip(pfl, nv, v3));
      ASSERT_TRUE(C.updateFlip(pfl, nv, v1));
    }
  }

  {
    auto flips = C.match(Q, Q, Q, v1);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nv));
  }

  C.removeVertex(v1);
  ASSERT_TRUE(computeOrientations(C));
  v1 = nv;
  testConstruction();
}


TEST_F(TestBrokenLine, global_queries)
{
  EXPECT_TRUE(CC.border(v1));
  EXPECT_FALSE(CC.border(v2));
  EXPECT_FALSE(CC.border(v3));
  EXPECT_TRUE(CC.border(v4));

  EXPECT_TRUE(CC.border(c1));
  EXPECT_FALSE(CC.border(c2));
  EXPECT_TRUE(CC.border(c3));
}

struct TestDoubleTriangle : public ::testing::Test, public DoubleTriangle
{
  virtual void SetUp()
  {
    create();
  }

  void testConstruction()
  {
    EXPECT_EQ(4, CC.nbVertices());
    EXPECT_EQ(5, CC.nbEdges());
    EXPECT_EQ(2, CC.nbCells());

    // Test bounds of vertices
    for(auto v: CC.vertices())
      EXPECT_EQ(set({CC._}), CC.bounds(v));

    EXPECT_EQ(v1, CC.flip(e1, v2, CC._));
    EXPECT_EQ(v2, CC.flip(e1, v1, CC._));
    EXPECT_EQ(e2, CC.flip(c2, e4, v3));
    EXPECT_EQ(e4, CC.flip(c2, e2, v3));
    EXPECT_EQ(c1, CC.flip(CC.T, c2, e2));
    EXPECT_EQ(c2, CC.flip(CC.T, c1, e2));
    EXPECT_EQ(vertex_t::null, CC.flip(e3, v2, CC._));
    EXPECT_EQ(edge_t::null, CC.flip(c1, e1, v3));
    EXPECT_EQ(edge_t::null, CC.flip(c2, e1, v1));
    EXPECT_EQ(cell_t::null, CC.flip(CC.T, c1, e1));

    // Test bounds of edges
    EXPECT_EQ(set({v2, v1}), CC.bounds(e1));
    EXPECT_EQ(set({v3, v2}), CC.bounds(e2));
    EXPECT_EQ(set({v3, v1}), CC.bounds(e3));
    EXPECT_EQ(set({v3, v4}), CC.bounds(e4));
    EXPECT_EQ(set({v2, v4}), CC.bounds(e5));

    // Test bounds of cells
    EXPECT_EQ(set({e1, e2, e3}), CC.bounds(c1));
    EXPECT_EQ(set({e2, e4, e5}), CC.bounds(c2));

    // Test boundaries of edges
    EXPECT_EQ(chain({+v2, -v1}), CC.boundary(+e1));
    EXPECT_EQ(chain({+v3, -v2}), CC.boundary(+e2));
    EXPECT_EQ(chain({+v3, -v1}), CC.boundary(+e3));
    EXPECT_EQ(chain({+v3, -v4}), CC.boundary(+e4));
    EXPECT_EQ(chain({+v4, -v2}), CC.boundary(+e5));

    // Test boundaries of cells
    EXPECT_EQ(chain({+e1, +e2, -e3}), CC.boundary(+c1));
    EXPECT_EQ(chain({+e5, +e4, -e2}), CC.boundary(+c2));

    EXPECT_EQ(chain({+e1, +e2, -e3}), chain(boundary(CC, +c1)));
    EXPECT_EQ(chain({+e5, +e4, -e2}), chain(boundary(CC, +c2)));
  }
};

TEST_F(TestDoubleTriangle, construction)
{
  testConstruction();

  EXPECT_NE(vertex_t::null, CC.anyVertex());
  EXPECT_NE(edge_t::null, CC.anyEdge());
  EXPECT_NE(edge_t::null, CC.anyFace());
  EXPECT_NE(cell_t::null, CC.anyCell());
  EXPECT_EQ(CC._, CC.any<-1>());
  EXPECT_EQ(CC.T, CC.any<3>());

  EXPECT_EQ(set({v1, v2, v3, v4}), set(CC.vertices()));
  EXPECT_EQ(set({e1, e2, e3, e4, e5}), set(CC.edges()));
  EXPECT_EQ(set({c1, c2}), set(CC.cells()));
}

TEST_F(TestDoubleTriangle, simple_queries)
{
  EXPECT_TRUE(CC.contains(CC._));
  EXPECT_TRUE(CC.contains(CC.T));
  EXPECT_TRUE(CC.hasVertex(v1));
  EXPECT_TRUE(CC.hasEdge(e1));
  EXPECT_TRUE(CC.hasCell(c2));
  EXPECT_FALSE(CC.hasVertex(vertex_t()));
  EXPECT_FALSE(CC.hasFace(edge_t()));
  EXPECT_FALSE(CC.hasCell(cell_t()));

  EXPECT_EQ(e1, CC.edge(v1, v2));
  EXPECT_NE(e3, CC.edge(v2, v3));
  EXPECT_EQ(edge_t::null, CC.edge(v1, v4));
}

TEST_F(TestDoubleTriangle, test_match)
{
  auto f = CC.cellFlip(c1, Q, Q, Q);
  auto flips = CC.match(f);

  auto c0 = cell_t::null;
  auto e0 = edge_t::null;
  auto v0 = vertex_t::null;
  auto b0 = bottom_t::null;
  auto t0 = top_t::null;

  // Test all the possible combination of matching ...
  EXPECT_EQ(2, CC.match(CC.cellFlip(Q, e2, Q, Q), flips).size());
  EXPECT_EQ(0, CC.match(CC.cellFlip(Q, e4, Q, Q), flips).size());
  EXPECT_EQ(1, CC.match(CC.cellFlip(Q, Q, Q, v1), flips).size());
  EXPECT_EQ(0, CC.match(CC.cellFlip(c2, Q, Q, v1), flips).size());
  EXPECT_EQ(1, CC.match(CC.cellFlip(c1, e2, Q, v2), flips).size());
  EXPECT_EQ(1, CC.match(-CC.cellFlip(c1, e2, e1, v2), flips).size());
  EXPECT_EQ(1, CC.match(-CC.cellFlip(c1, e2, e1, Q), flips).size());
  EXPECT_EQ(2, CC.match(+CC.cellFlip(c1, Q, e1, Q), flips).size());
  EXPECT_EQ(1, CC.match(+CC.cellFlip(Q, e2, e1, v2), flips).size());
  EXPECT_EQ(1, CC.match(CC.cellFlip(Q, e2, Q, v2), flips).size());
  EXPECT_EQ(1, CC.match(CC.cellFlip(Q, e2, e1, Q), flips).size());
  EXPECT_EQ(2, CC.match(CC.cellFlip(Q, Q, e1, Q), flips).size());
  EXPECT_EQ(1, CC.match(CC.cellFlip(Q, Q, e1, v2), flips).size());
  EXPECT_EQ(1, CC.match(CC.cellFlip(Q, Q, Q, v2), flips).size());

  auto cf1 = CC.cellFlip(c1, e2, e1, v2);
  auto cf2 = cf1;
  cf2 *= cellflips::neg;
  EXPECT_EQ(cf1.face1, cf2.face2);
  EXPECT_EQ(cf2.face1, cf1.face2);
  EXPECT_NE(cf1, cf2);
  EXPECT_NE(cf1, CC.cellFlip(c1, e2, e3, v2));
  EXPECT_TRUE(cf1 < cf2 or cf2 < cf1);
  auto cf3 = CC.cellFlip(c1, e2, e1, v1);
  EXPECT_TRUE(cf1 < cf3 or cf3 < cf1);
  cf1.reverse();
  EXPECT_EQ(cf1, cf2);
  cf2 = cellflips::pos * (cellflips::neg * CC.cellFlip(c1, e2, e1, v2)) * cellflips::pos;
  EXPECT_EQ(cf1, cf2);
  EXPECT_FALSE(bool(cellflips::invalid*cf1));
  EXPECT_EQ(cellflips::pos, cf1.pathOrientation(cf1.face1));
  EXPECT_EQ(cellflips::neg, cf1.pathOrientation(cf1.face2));

  EXPECT_EQ(3, CC.match(c1, Q, Q, Q).size());
  EXPECT_EQ(3, CC.match(c1, e0, Q, Q).size());
  EXPECT_EQ(3, CC.match(c1, e0, e0, Q).size());
  EXPECT_EQ(3, CC.match(c1, e0, e0, v0).size());

  EXPECT_EQ(1, CC.match(e1, Q, v1, CC._).size());
  EXPECT_EQ(1, CC.match(e1, v0, v1, CC._).size());
  EXPECT_EQ(1, CC.match(e1, v1, Q, CC._).size());
  EXPECT_EQ(1, CC.match(e1, v1, v0, CC._).size());

  EXPECT_EQ(1, CC.match(e1, v1, Q, Q).size());
  EXPECT_EQ(1, CC.match(e1, v1, v0, Q).size());
  EXPECT_EQ(1, CC.match(e1, v1, v0, b0).size());
  EXPECT_EQ(1, CC.match(e1, v1, Q, b0).size());
  EXPECT_EQ(0, CC.match(e1, v3, Q, Q).size());
  EXPECT_EQ(2, CC.match(c1, Q, e2, Q).size());
  EXPECT_EQ(2, CC.match(c1, e0, e2, Q).size());
  EXPECT_EQ(1, CC.match(c1, e1, e2, Q).size());
  EXPECT_EQ(1, CC.match(c1, e1, e2, v2).size());
  EXPECT_EQ(0, CC.match(c1, e1, e2, v4).size());
  EXPECT_EQ(2, CC.match(c1, e0, e2, v0).size());
  EXPECT_EQ(0, CC.match(c1, Q, e5, Q).size());
  EXPECT_EQ(0, CC.match(c1, e4, e5, Q).size());
  EXPECT_EQ(0, CC.match(c1, e4, e5, v0).size());
  EXPECT_EQ(0, CC.match(c1, e4, e5, v3).size());
  EXPECT_EQ(1, CC.match(c2, Q, Q, v2).size());
  EXPECT_EQ(1, CC.match(c2, e0, Q, v2).size());
  EXPECT_EQ(1, CC.match(c2, Q, e0, v2).size());
  EXPECT_EQ(1, CC.match(c2, e0, e0, v2).size());

  EXPECT_EQ(1, CC.match(c1, Q, Q, v1).size());
  EXPECT_EQ(1, CC.match(c1, e0, Q, v1).size());
  EXPECT_EQ(1, CC.match(c1, e0, e0, v1).size());
  EXPECT_EQ(0, CC.match(c1, Q, Q, v4).size());

  EXPECT_EQ(0, CC.match(Q, v1, v1, Q).size());
  EXPECT_EQ(0, CC.match(Q, v1, v1, b0).size());
  EXPECT_EQ(0, CC.match(e0, v1, v1, b0).size());
  EXPECT_EQ(0, CC.match(e0, v1, v1, Q).size());

  EXPECT_EQ(4, CC.match(c0, e2, Q, Q).size());
  EXPECT_EQ(4, CC.match(Q, e2, Q, Q).size());
  EXPECT_EQ(4, CC.match(c0, e2, e0, Q).size());
  EXPECT_EQ(4, CC.match(c0, e2, e0, v0).size());
  EXPECT_EQ(4, CC.match(Q, e2, e0, Q).size());
  EXPECT_EQ(4, CC.match(Q, e2, Q, v0).size());
  EXPECT_EQ(4, CC.match(Q, e2, e0, v0).size());
  EXPECT_EQ(1, CC.match(Q, e2, e1, Q).size());
  EXPECT_EQ(0, CC.match(Q, e4, e1, Q).size());

  EXPECT_EQ(2, CC.match(c0, e2, Q, v2).size());
  EXPECT_EQ(2, CC.match(c0, e2, e0, v2).size());
  EXPECT_EQ(2, CC.match(Q, e2, Q, v2).size());
  EXPECT_EQ(2, CC.match(Q, e2, e0, v2).size());
  EXPECT_EQ(0, CC.match(Q, e2, Q, v1).size());

  EXPECT_EQ(1, CC.match(Q, c1, c2, e2).size());
  EXPECT_EQ(0, CC.match(Q, c1, c2, e1).size());

  EXPECT_EQ(0, CC.match(Q, c1, c1, e1).size());
  EXPECT_EQ(0, CC.match(t0, c1, c1, e1).size());

  EXPECT_EQ(2, CC.match(e0, Q, v1, Q).size());
  EXPECT_EQ(2, CC.match(Q, v0, v1, Q).size());
  EXPECT_EQ(2, CC.match(e0, v0, v1, Q).size());
  EXPECT_EQ(2, CC.match(Q, Q, v1, Q).size());
  EXPECT_EQ(2, CC.match(Q, Q, v1, CC._).size());
  EXPECT_EQ(0, CC.match(Q, Q, e1, v3).size());
  EXPECT_EQ(2, CC.match(Q, Q, e1, v0).size());
  EXPECT_EQ(2, CC.match(c0, Q, e1, v0).size());
  EXPECT_EQ(2, CC.match(c0, e0, e1, v0).size());
  EXPECT_EQ(2, CC.match(Q, e0, e1, v0).size());

  EXPECT_EQ(1, CC.match(t0, c0, Q, e2).size());
  EXPECT_EQ(1, CC.match(t0, c0, c0, e2).size());
  EXPECT_EQ(1, CC.match(t0, Q, c0, e2).size());
  EXPECT_EQ(1, CC.match(t0, Q, Q, e2).size());
  EXPECT_EQ(1, CC.match(Q, c0, Q, e2).size());
  EXPECT_EQ(1, CC.match(Q, c0, c0, e2).size());
  EXPECT_EQ(1, CC.match(Q, Q, c0, e2).size());
  EXPECT_EQ(1, CC.match(Q, Q, Q, e2).size());
  EXPECT_EQ(0, CC.match(Q, Q, Q, e5).size());
}

TEST_F(TestDoubleTriangle, double_construction)
{
  C.clear();
  create();
  testConstruction();
}

TEST_F(TestDoubleTriangle, basic_edition)
{
  // Should not work because the flip is already there
  EXPECT_EQ(nullptr, C.addFlip(c1, e4, e5, v2));

  // Should not work because the cells are unrelated
  EXPECT_FALSE(C.removeRelativeOrientation(c1, e5));

  ASSERT_FALSE(C.removeFlip(C.cellFlip(c1, e1, e2, v3)));
  ASSERT_EQ(1, C.match(c1, e1, e2, v2).size());
  ASSERT_TRUE(C.removeFlip(C.cellFlip(c1, e1, e2, v2))); // Must restore right after
  ASSERT_TRUE(C.addFlip(C.cellFlip(c1, e1, e2, v2))); // Restore the flip

  testConstruction(); // Nothing should have changed!
}

TEST_F(TestDoubleTriangle, bounds)
{
  EXPECT_EQ(set({v1, v2, v3}), CC.joints(c1));
  EXPECT_EQ(set({v2, v3, v4}), CC.joints(c2));

  EXPECT_EQ(set({v1, v2, v3}), set(joints(CC,c1)));
  EXPECT_EQ(set({v2, v3, v4}), set(joints(CC,c2)));

  EXPECT_EQ(set({e1, e3}), CC.cobounds(v1));
  EXPECT_EQ(set({e1, e2, e5}), CC.cobounds(v2));
  EXPECT_EQ(set({e2, e3, e4}), CC.cobounds(v3));
  EXPECT_EQ(set({e4, e5}), CC.cobounds(v4));

  EXPECT_EQ(set({c1}), CC.cojoints(v1));
  EXPECT_EQ(set({c1, c2}), CC.cojoints(v2));
  EXPECT_EQ(set({c1, c2}), CC.cojoints(v3));
  EXPECT_EQ(set({c2}), CC.cojoints(v4));
}

TEST_F(TestDoubleTriangle, neighbors)
{
  EXPECT_EQ(3, CC.nbNeighbors(v2));
  EXPECT_EQ(3, CC.nbNeighbors(e1));
  EXPECT_EQ(1, CC.nbNeighbors(c1));

  EXPECT_EQ(set({v1, v3, v4}), CC.neighbors(v2));
  EXPECT_EQ(set({e3, e2, e5}), CC.neighbors(e1));
  EXPECT_EQ(set({c2}), CC.neighbors(c1));

  EXPECT_TRUE(set({v1, v3, v4}).contains(CC.anyNeighbor(v2)));
  EXPECT_TRUE(set({e3, e2, e5}).contains(CC.anyNeighbor(e1)));
  EXPECT_TRUE(set({c2}).contains(CC.anyNeighbor(c1)));
  EXPECT_TRUE(set({c1}).contains(CC.anyNeighbor(c2)));

  EXPECT_EQ(2, CC.nbConeighbors(e1));
  EXPECT_EQ(set({e2, e3}), CC.coneighbors(e1));
}

TEST_F(TestDoubleTriangle, ordered_topology)
{
  EXPECT_EQ(chain({+e1, +e2, -e3}), CC.boundary(+c1));
  EXPECT_EQ(chain({+e1, -e3, -e4, -e5}), CC.orientedAdjacentCells(+e2));
  EXPECT_EQ(chain({+v3, -v1}), CC.boundary(chain({+e1, +e2})));

  EXPECT_EQ(cellflips::pos, CC.relativeOrientation(c1, c2));
  EXPECT_EQ(cellflips::neg, CC.relativeOrientation(v1, v2));
  EXPECT_EQ(cellflips::invalid, CC.relativeOrientation(e3, e5));
}

TEST_F(TestDoubleTriangle, updateVertexFlip)
{
  auto nv = C.addVertex();
  EXPECT_NE(vertex_t::null, nv);

  {
    auto flips = C.match(Q, v3, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nv, v3));
  }

  {
    auto flips = C.match(Q, Q, Q, v3);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nv));
  }

  C.removeVertex(v3);
  ASSERT_TRUE(computeOrientations(C));
  v3 = nv;

  testConstruction();
}

TEST_F(TestDoubleTriangle, updateEdgeFlip)
{
  auto ne = edge_t{};

  {
    auto flips = C.match(e2, Q, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, ne));
  }

  {
    auto flips = C.match(Q, e2, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, ne, e2));
  }

  {
    auto flips = C.match(Q, Q, Q, e2);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, ne));
  }

  ASSERT_TRUE(computeOrientations(C));
  e2 = ne;

  testConstruction();
}

TEST_F(TestDoubleTriangle, updateCellFlip)
{
  auto nc = cell_t{};

  {
    auto flips = C.match(c1, Q, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nc));
  }

  {
    auto flips = C.match(Q, c1, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nc, c1));
  }

  ASSERT_TRUE(computeOrientations(C));
  c1 = nc;

  testConstruction();
}

TEST_F(TestDoubleTriangle, global_queries)
{
  EXPECT_TRUE(CC.border(v1));
  EXPECT_TRUE(CC.border(v2));
  EXPECT_TRUE(CC.border(v3));
  EXPECT_TRUE(CC.border(v4));

  EXPECT_TRUE(CC.border(e1));
  EXPECT_FALSE(CC.border(e2));
  EXPECT_TRUE(CC.border(e3));
  EXPECT_TRUE(CC.border(e4));
  EXPECT_TRUE(CC.border(e5));

  EXPECT_TRUE(CC.border(c1));
  EXPECT_TRUE(CC.border(c2));
}

TEST_F(TestDoubleTriangle, failed_addcell)
{
  ASSERT_TRUE(::addCellFailure(C, {+e1, +e2, +e4}, c1, CellComplex2d::CELL_ALREADY_IN_COMPLEX));
  ASSERT_TRUE(::addCellFailure(C, {+e2, -e3, +e1}, cell_t::null, CellComplex2d::FACE_ALREADY_BETWEEN_CELLS));
  ASSERT_TRUE(::addCellFailure(C, {+e1, +e5, +e4, +e3}, cell_t::null, CellComplex2d::WRONG_BOUNDARY_ORIENTATION));
  ASSERT_TRUE(::addCellFailure(C, {+e1, +e5}, cell_t::null, CellComplex2d::CELL_BOUNDARY_NOT_CLOSED));
  ASSERT_EQ(cell_t::null, addCell(C, std::vector<oriented_edge_t>{+e1, +e5, +e4, +e3}));
}


struct TestStomata : public ::testing::Test, public OrientedStomata
{
  virtual void SetUp()
  {
    create();
  }

  void testConstruction()
  {
    EXPECT_EQ(4, CC.nbVertices());
    EXPECT_EQ(6, CC.nbEdges());
    EXPECT_EQ(3, CC.nbCells());

    // Test bound vertices
    for(auto v: CC.vertices())
      EXPECT_EQ(set({CC._}), CC.bounds(v));

    EXPECT_EQ(v1, CC.flip(e1, v2, CC._));
    EXPECT_EQ(v2, CC.flip(e1, v1, CC._));
    EXPECT_EQ(e5, CC.flip(c2, e4, v3));
    EXPECT_EQ(e4, CC.flip(c2, e5, v3));
    EXPECT_EQ(c3, CC.flip(CC.T, c2, e5));
    EXPECT_EQ(c2, CC.flip(CC.T, c3, e5));
    EXPECT_EQ(vertex_t::null, CC.flip(e3, v2, CC._));
    EXPECT_EQ(edge_t::null, CC.flip(c1, e1, v3));
    EXPECT_EQ(edge_t::null, CC.flip(c2, e4, v1));
    EXPECT_EQ(cell_t::null, CC.flip(CC.T, c2, e1));
    EXPECT_EQ(cell_t::null, CC.flip(CC.T, c2, e3));

    // Check bounds
    EXPECT_EQ(set({v1, v2}), CC.bounds(e1));
    EXPECT_EQ(set({v4, v3}), CC.bounds(e2));
    EXPECT_EQ(set({v1, v4}), CC.bounds(e3));
    EXPECT_EQ(set({v2, v3}), CC.bounds(e4));
    EXPECT_EQ(set({v3, v2}), CC.bounds(e5));
    EXPECT_EQ(set({v4, v1}), CC.bounds(e6));

    EXPECT_EQ(set({e1, e4, e2, e3}), CC.bounds(c1));
    EXPECT_EQ(set({e4, e5}), CC.bounds(c2));
    EXPECT_EQ(set({e1, e5, e2, e6}), CC.bounds(c3));


    // Check boundaries
    EXPECT_EQ(chain({+v2, -v1}), CC.boundary(+e1));
    EXPECT_EQ(chain({+v4, -v3}), CC.boundary(+e2));
    EXPECT_EQ(chain({+v1, -v4}), CC.boundary(+e3));
    EXPECT_EQ(chain({+v2, -v3}), CC.boundary(+e4));
    EXPECT_EQ(chain({+v3, -v2}), CC.boundary(+e5));
    EXPECT_EQ(chain({+v4, -v1}), CC.boundary(+e6));

    EXPECT_EQ(chain({+e1, -e4, +e2, +e3}), CC.boundary(+c1));
    EXPECT_EQ(chain({+e4, +e5}), CC.boundary(+c2));
    EXPECT_EQ(chain({+e1, +e5, +e2, -e6}), CC.boundary(-c3));

    EXPECT_EQ(chain({+v2, -v1}), chain(boundary(CC, +e1)));
    EXPECT_EQ(chain({+v4, -v3}), chain(boundary(CC, +e2)));
    EXPECT_EQ(chain({+v1, -v4}), chain(boundary(CC, +e3)));
    EXPECT_EQ(chain({+v2, -v3}), chain(boundary(CC, +e4)));
    EXPECT_EQ(chain({+v3, -v2}), chain(boundary(CC, +e5)));
    EXPECT_EQ(chain({+v4, -v1}), chain(boundary(CC, +e6)));

    EXPECT_EQ(chain({+e1, -e4, +e2, +e3}), chain(boundary(CC, +c1)));
    EXPECT_EQ(chain({+e4, +e5}), chain(boundary(CC, +c2)));
    EXPECT_EQ(chain({+e1, +e5, +e2, -e6}), chain(boundary(CC, -c3)));
  }
};

TEST_F(TestStomata, construction)
{
  testConstruction();

  EXPECT_NE(vertex_t::null, CC.anyVertex());
  EXPECT_NE(edge_t::null, CC.anyEdge());
  EXPECT_NE(edge_t::null, CC.anyFace());
  EXPECT_NE(cell_t::null, CC.anyCell());
  EXPECT_EQ(CC._, CC.any<-1>());
  EXPECT_EQ(CC.T, CC.any<3>());

  EXPECT_EQ(set({v1, v2, v3, v4}), set(CC.vertices()));
  EXPECT_EQ(set({e1, e2, e3, e4, e5, e6}), set(CC.edges()));
  EXPECT_EQ(set({c1, c2, c3}), set(CC.cells()));
}

TEST_F(TestStomata, simple_queries)
{
  EXPECT_TRUE(CC.hasCell(CC._));
  EXPECT_TRUE(CC.hasCell(CC.T));
  EXPECT_TRUE(CC.hasVertex(v1));
  EXPECT_TRUE(CC.hasEdge(e1));
  EXPECT_TRUE(CC.hasCell(c2));
  EXPECT_FALSE(CC.hasVertex(vertex_t()));
  EXPECT_FALSE(CC.hasFace(edge_t()));
  EXPECT_FALSE(CC.hasCell(cell_t()));

  EXPECT_EQ(e1, CC.edge(v1, v2));
  EXPECT_NE(e3, CC.edge(v2, v3));
  EXPECT_EQ(edge_t::null, CC.edge(v1, v3));
}

TEST_F(TestStomata, double_construction)
{
  C.clear();
  create();
  testConstruction();
}

TEST_F(TestStomata, bounds)
{
  EXPECT_EQ(set({e1, e3, e6}), CC.cobounds(v1));
  EXPECT_EQ(set({e1, e4, e5}), CC.cobounds(v2));
  EXPECT_EQ(set({e4, e5, e2}), CC.cobounds(v3));
  EXPECT_EQ(set({e2, e3, e6}), CC.cobounds(v4));

  EXPECT_EQ(set({c1, c3}), CC.cojoints(v1));
  EXPECT_EQ(set({c1, c2, c3}), CC.cojoints(v2));
  EXPECT_EQ(set({c1, c2, c3}), CC.cojoints(v3));
  EXPECT_EQ(set({c1, c3}), CC.cojoints(v4));

  EXPECT_EQ(set({v1, v2, v3, v4}), CC.joints(c1));
  EXPECT_EQ(set({v2, v3}), CC.joints(c2));
  EXPECT_EQ(set({v1, v2, v3, v4}), CC.joints(c3));
}

TEST_F(TestStomata, global_queries)
{
  EXPECT_TRUE(CC.border(v1));
  EXPECT_FALSE(CC.border(v2));
  EXPECT_FALSE(CC.border(v3));
  EXPECT_TRUE(CC.border(v4));

  EXPECT_FALSE(CC.border(e1));
  EXPECT_FALSE(CC.border(e2));
  EXPECT_TRUE(CC.border(e3));
  EXPECT_FALSE(CC.border(e4));
  EXPECT_FALSE(CC.border(e5));
  EXPECT_TRUE(CC.border(e6));

  EXPECT_TRUE(CC.border(c1));
  EXPECT_FALSE(CC.border(c2));
  EXPECT_TRUE(CC.border(c3));
}

TEST_F(TestStomata, ordered_topology)
{
  EXPECT_EQ(chain({+e4, +e5}), CC.boundary(+c2));
  EXPECT_EQ(chain({+e6, -e2, -e1, -e5, -e5, -e4}), CC.boundary(chain({+c3, -c2})));
  EXPECT_EQ(chain({+v4, -v2}), CC.boundary(chain({+e2, +e5})));
  EXPECT_EQ(+e1, CC.orientedEdge(v1, v2));
  EXPECT_EQ(-e1, CC.orientedEdge(v2, v1));
  EXPECT_FALSE(bool(CC.orientedEdge(v1, v3)));

  EXPECT_TRUE(sameCyclicVector({v2, v3, v4, v1}, CC.orderedVertices(+c1)));
  EXPECT_TRUE(sameCyclicVector({v2, v3, v4, v1}, CC.orderedJoints(+c1, CC._)));
  EXPECT_TRUE(sameCyclicVector({c1, c3, c2}, CC.orderedCojoints(+v2, CC.T)));

  EXPECT_EQ(std::make_pair(v2, v1), CC.orderedVertices(+e1));
  EXPECT_EQ(std::make_pair(v3, v2), CC.orderedVertices(-e4));
  EXPECT_EQ(std::make_pair(vertex_t::null, vertex_t::null), CC.orderedVertices(-e_out));

  EXPECT_EQ(v2, CC.source(-e1));
  EXPECT_EQ(v1, CC.target(-e1));
  EXPECT_EQ(v3, CC.source(+e4));
  EXPECT_EQ(v2, CC.target(+e4));
  EXPECT_EQ(vertex_t::null, CC.target(cellflips::invalid * e1));
  EXPECT_EQ(vertex_t::null, CC.source(cellflips::invalid * e4));
  EXPECT_EQ(vertex_t::null, CC.source(+e_out));
  EXPECT_EQ(vertex_t::null, CC.target(-e_out));

  EXPECT_EQ(chain({+CC.T}), CC.coboundary(+c1));
  EXPECT_EQ(chain({-CC.T}), CC.coboundary(-c1));

  EXPECT_EQ(chain({+CC.T}), chain(coboundary(CC,+c1)));
  EXPECT_EQ(chain({-CC.T}), chain(coboundary(CC,-c1)));

}

struct TestDoubleTetrahedron : public ::testing::Test, public DoubleTetrahedron
{
  virtual void SetUp()
  {
    create();
  }

  void testConstruction()
  {
    EXPECT_EQ(5, CC.nbVertices());
    EXPECT_EQ(9, CC.nbEdges());
    EXPECT_EQ(7, CC.nbFaces());
    EXPECT_EQ(2, CC.nbCells());

    // Test bounds of vertices
    for(auto v: CC.vertices())
      EXPECT_EQ(set({CC._}), CC.bounds(v));

    EXPECT_EQ(v1, CC.flip(e1, v2, CC._));
    EXPECT_EQ(v2, CC.flip(e1, v1, CC._));
    EXPECT_EQ(e2, CC.flip(f2, e4, v2));
    EXPECT_EQ(e4, CC.flip(f2, e2, v2));
    EXPECT_EQ(f3, CC.flip(c2, f6, e6));
    EXPECT_EQ(f6, CC.flip(c2, f3, e6));
    EXPECT_EQ(c2, CC.flip(CC.T, c1, f3));
    EXPECT_EQ(c1, CC.flip(CC.T, c2, f3));

    EXPECT_EQ(vertex_t::null, CC.flip(e3, v2, CC._));
    EXPECT_EQ(edge_t::null, CC.flip(f1, e1, v3));
    EXPECT_EQ(edge_t::null, CC.flip(f2, e4, v3));
    EXPECT_EQ(face_t::null, CC.flip(c1, f1, e7));
    EXPECT_EQ(face_t::null, CC.flip(c2, f4, e3));
    EXPECT_EQ(cell_t::null, CC.flip(CC.T, c2, f2));
    EXPECT_EQ(cell_t::null, CC.flip(CC.T, cell_t(), f3));

    // Test bounds of edges
    EXPECT_EQ(set({v2, v1}), CC.bounds(e1));
    EXPECT_EQ(set({v3, v2}), CC.bounds(e2));
    EXPECT_EQ(set({v3, v1}), CC.bounds(e3));
    EXPECT_EQ(set({v2, v4}), CC.bounds(e4));
    EXPECT_EQ(set({v1, v4}), CC.bounds(e5));
    EXPECT_EQ(set({v3, v4}), CC.bounds(e6));
    EXPECT_EQ(set({v1, v5}), CC.bounds(e7));
    EXPECT_EQ(set({v5, v3}), CC.bounds(e8));
    EXPECT_EQ(set({v5, v4}), CC.bounds(e9));

    // Test bounds of faces
    EXPECT_EQ(set({e1, e4, e5}), CC.bounds(f1));
    EXPECT_EQ(set({e2, e4, e6}), CC.bounds(f2));
    EXPECT_EQ(set({e3, e5, e6}), CC.bounds(f3));
    EXPECT_EQ(set({e1, e2, e3}), CC.bounds(f4));
    EXPECT_EQ(set({e9, e7, e5}), CC.bounds(f5));
    EXPECT_EQ(set({e8, e9, e6}), CC.bounds(f6));
    EXPECT_EQ(set({e3, e7, e8}), CC.bounds(f7));

    // Test bounds of cells
    EXPECT_EQ(set({f1, f2, f3, f4}), CC.bounds(c1));
    EXPECT_EQ(set({f3, f5, f6, f7}), CC.bounds(c2));

    // Test boundaries of edges
    EXPECT_EQ(chain({+v2, -v1}), CC.boundary(+e1));
    EXPECT_EQ(chain({+v3, -v2}), CC.boundary(+e2));
    EXPECT_EQ(chain({-v3, +v1}), CC.boundary(+e3));
    EXPECT_EQ(chain({-v2, +v4}), CC.boundary(+e4));
    EXPECT_EQ(chain({+v1, -v4}), CC.boundary(+e5));
    EXPECT_EQ(chain({-v3, +v4}), CC.boundary(+e6));
    EXPECT_EQ(chain({-v1, +v5}), CC.boundary(+e7));
    EXPECT_EQ(chain({-v5, +v3}), CC.boundary(+e8));
    EXPECT_EQ(chain({-v5, +v4}), CC.boundary(+e9));

    // Test boundaries of faces
    EXPECT_EQ(chain({+e1, +e4, +e5}), CC.boundary(+f1));
    EXPECT_EQ(chain({+e2, -e4, +e6}), CC.boundary(+f2));
    EXPECT_EQ(chain({+e3, -e5, -e6}), CC.boundary(+f3));
    EXPECT_EQ(chain({-e1, -e2, -e3}), CC.boundary(+f4));
    EXPECT_EQ(chain({-e9, -e7, -e5}), CC.boundary(+f5));
    EXPECT_EQ(chain({-e8, +e9, -e6}), CC.boundary(+f6));
    EXPECT_EQ(chain({+e3, +e7, +e8}), CC.boundary(+f7));

    // Test boundaries of cells
    EXPECT_EQ(chain({+f1, +f2, +f3, +f4}), CC.boundary(+c1));
    EXPECT_EQ(chain({-f3, +f5, +f6, +f7}), CC.boundary(+c2));
  }
};


TEST_F(TestDoubleTetrahedron, construction)
{
  testConstruction();

  EXPECT_NE(vertex_t::null, CC.anyVertex());
  EXPECT_NE(edge_t::null, CC.anyEdge());
  EXPECT_NE(face_t::null, CC.anyFace());
  EXPECT_NE(cell_t::null, CC.anyCell());
  EXPECT_EQ(CC._, CC.any<-1>());
  EXPECT_EQ(CC.T, CC.any<4>());

  EXPECT_EQ(set({v1, v2, v3, v4, v5}), set(CC.vertices()));
  EXPECT_EQ(set({e1, e2, e3, e4, e5, e6, e7, e8, e9}), set(CC.edges()));
  EXPECT_EQ(set({f1, f2, f3, f4, f5, f6, f7}), set(CC.faces()));
  EXPECT_EQ(set({c1, c2}), set(CC.cells()));
}

TEST_F(TestDoubleTetrahedron, simple_queries)
{
  EXPECT_TRUE(CC.hasCell(CC._));
  EXPECT_TRUE(CC.hasCell(CC.T));
  EXPECT_TRUE(CC.hasVertex(v1));
  EXPECT_TRUE(CC.hasEdge(e1));
  EXPECT_TRUE(CC.hasFace(f1));
  EXPECT_TRUE(CC.hasCell(c2));
  EXPECT_FALSE(CC.hasVertex(vertex_t()));
  EXPECT_FALSE(CC.hasEdge(edge_t()));
  EXPECT_FALSE(CC.hasFace(face_t()));
  EXPECT_FALSE(CC.hasCell(cell_t()));

  EXPECT_EQ(e1, CC.edge(v1, v2));
  EXPECT_NE(e3, CC.edge(v2, v3));
  EXPECT_EQ(edge_t::null, CC.edge(v2, v5));
}

TEST_F(TestDoubleTetrahedron, double_construction)
{
  C.clear();
  create();
  testConstruction();
}

TEST_F(TestDoubleTetrahedron, bounds)
{
  EXPECT_EQ(set({e1, e5, e3, e7}), CC.cobounds(v1));
  EXPECT_EQ(set({e1, e2, e4}), CC.cobounds(v2));
  EXPECT_EQ(set({e2, e3, e6, e8}), CC.cobounds(v3));
  EXPECT_EQ(set({e4, e5, e6, e9}), CC.cobounds(v4));
  EXPECT_EQ(set({e7, e8, e9}), CC.cobounds(v5));
  EXPECT_TRUE(set({e7, e8, e9}).contains(CC.anyCobound(v5)));

  EXPECT_EQ(set({e1, e5, e3, e7}), set(cofaces(CC, v1)));
  EXPECT_EQ(set({e1, e2, e4}), set(cofaces(CC, v2)));
  EXPECT_EQ(set({e2, e3, e6, e8}), set(cofaces(CC, v3)));
  EXPECT_EQ(set({e4, e5, e6, e9}), set(cofaces(CC, v4)));
  EXPECT_EQ(set({e7, e8, e9}), set(cofaces(CC, v5)));

  EXPECT_EQ(set({f1, f3, f4, f5, f7}), CC.cojoints(v1));
  EXPECT_EQ(set({f1, f2, f4}), CC.cojoints(v2));
  EXPECT_EQ(set({f2, f3, f4, f6, f7}), CC.cojoints(v3));
  EXPECT_EQ(set({f1, f2, f3, f5, f6}), CC.cojoints(v4));
  EXPECT_EQ(set({f5, f6, f7}), CC.cojoints(v5));

  EXPECT_TRUE(set({f5, f6, f7}).contains(CC.anyCobound<2>(v5)));
  EXPECT_FALSE(set({f1, f2, f3, f4}).contains(CC.anyCobound<2>(v5)));

  EXPECT_TRUE(set({c1, c2}).contains(CC.anyCobound<3>(v1)));
  EXPECT_EQ(c2, CC.anyCobound<3>(v5));
  EXPECT_EQ(CC.T, CC.anyCobound(c1));
  EXPECT_EQ(CC.T, CC.anyCobound<4>(e1));
  EXPECT_EQ(top_t::null, CC.anyCobound<4>(e_out));
  EXPECT_EQ(c1, CC.anyCobound<3>(v2));

  EXPECT_TRUE(set({v1, v2, v3, v4}).contains(CC.anyBound<0>(c1)));
  EXPECT_EQ(CC._, CC.anyBound<-1>(e3));
  EXPECT_EQ(CC._, CC.anyBound<-1>(c2));
  EXPECT_EQ(bottom_t::null, CC.anyBound<-1>(c_out));
  EXPECT_EQ(bottom_t::null, CC.anyBound<-1>(f_out));

  EXPECT_EQ(set({c1, c2}), CC.cobounds<3>(v1));
  EXPECT_EQ(set({c1}), CC.cobounds<3>(v2));
  EXPECT_EQ(set({c1, c2}), CC.cobounds<3>(v3));
  EXPECT_EQ(set({c1, c2}), CC.cobounds<3>(v4));
  EXPECT_EQ(set({c2}), CC.cobounds<3>(v5));

  EXPECT_EQ(set({c1, c2}), set(cobounds<3>(CC, v1)));
  EXPECT_EQ(set({c1}), set(cobounds<3>(CC, v2)));
  EXPECT_EQ(set({c1, c2}), set(cobounds<3>(CC, v3)));
  EXPECT_EQ(set({c1, c2}), set(cobounds<3>(CC, v4)));
  EXPECT_EQ(set({c2}), set(cobounds<3>(CC, v5)));

  EXPECT_EQ(set({c1, c2}), CC.cobounds<3>(set({v4, v5})));
  EXPECT_EQ(2, CC.nbCobounds<3>(set({v4, v5})));

  EXPECT_TRUE(CC.isCobound(v4, c1));
  EXPECT_FALSE(CC.isCobound(v2, c2));

  EXPECT_TRUE(CC.isCoface(f1, c1));
  EXPECT_FALSE(CC.isCoface(f1, c2));

  EXPECT_EQ(set({v1, v2, v3, v4}), CC.vertices(c1));
  EXPECT_EQ(set({v1, v5, v3, v4}), CC.vertices(c2));

  EXPECT_TRUE(set(vertices(CC, c_out)).empty());
  EXPECT_TRUE(set(vertices(CC, f_out)).empty());
  EXPECT_TRUE(set(vertices(CC, e_out)).empty());
  EXPECT_EQ(set({v1, v2, v3, v4}), set(vertices(CC,c1)));
  EXPECT_EQ(set({v1, v5, v3, v4}), set(vertices(CC,c2)));
  EXPECT_EQ(set({v1, v2, v3, v4}), set(bounds<0>(CC,c1)));
  EXPECT_EQ(set({v1, v5, v3, v4}), set(bounds<0>(CC,c2)));

  EXPECT_EQ(3, CC.nbBounds(f1));
  EXPECT_EQ(3, CC.nbVertices(f1));
  EXPECT_EQ(4, CC.nbVertices(c1));
  EXPECT_EQ(4, CC.nbBounds<0>(c1));
}

TEST_F(TestDoubleTetrahedron, ordered_topology)
{
  EXPECT_EQ(chain({+e1, +e4, +e5}), CC.boundary(+f1));
  EXPECT_EQ(chain({+f2, +f4, +f3}), CC.orientedAdjacentCells(+f1));

  EXPECT_TRUE(sameCyclicVector({e3, e5, e1}, CC.orderedJoints(-c1, v1)));
  EXPECT_TRUE(sameCyclicVector({f1, f4, f3}, CC.orderedCojoints(-v1, c1)));
  EXPECT_TRUE(sameCyclicVector({v4, v3, v2}, CC.orderedVertices(-f2)));

  EXPECT_EQ(std::make_pair(v2, v1), CC.orderedVertices(+e1));
  EXPECT_EQ(std::make_pair(v2, v4), CC.orderedVertices(-e4));

  EXPECT_EQ(chain({+CC.T}), CC.coboundary(+c1));
  EXPECT_EQ(chain({-CC.T}), CC.coboundary(-c1));
  EXPECT_TRUE(CC.coboundary(+c_out).empty());
  EXPECT_EQ(+set(CC.vertices()), CC.coboundary(CC._));

  EXPECT_EQ(+set(CC.vertices()), chain(coboundary(CC, +CC._)));
  EXPECT_EQ(chain({-C.T}), chain(coboundary(CC,-c1)));
  EXPECT_TRUE(coboundary(CC, -c_out).empty());
  EXPECT_TRUE(coboundary(CC, +f_out).empty());
  EXPECT_TRUE(boundary(CC, +c_out).empty());
  EXPECT_EQ(-set(CC.cells()), chain(boundary(CC, -C.T)));
  EXPECT_EQ(chain({+C._}), chain(boundary(CC, +v1)));
  EXPECT_TRUE(boundary(CC, +v_out).empty());

  EXPECT_EQ(cellflips::pos, CC.relativeOrientation(+v1, -v2));
  EXPECT_EQ(cellflips::neg, CC.relativeOrientation(+c1, -c2));
  EXPECT_EQ(cellflips::pos, CC.relativeOrientation(+f1, +f2));
  EXPECT_EQ(cellflips::pos, CC.relativeOrientation(+e1, +e2));
}

TEST_F(TestDoubleTetrahedron, utilities)
{
  EXPECT_EQ(chain({+e1, +e4, -e9, -e7}), CC.orientCell({e1, e4, e9, e7}, +e1));

  {
    auto res = CC.orientCell({e1, e4, e9, e7});
    if(res.contains(-e1))
      res *= cellflips::neg;
    EXPECT_EQ(chain({+e1, +e4, -e9, -e7}), res);
  }
}

TEST_F(TestDoubleTetrahedron, updateVertexFlip)
{
  auto nv = C.addVertex();
  EXPECT_NE(vertex_t::null, nv);

  {
    auto flips = C.match(Q, v3, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nv, v3));
  }

  {
    auto flips = C.match(Q, Q, Q, v3);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nv));
  }

  C.removeVertex(v3);
  ASSERT_TRUE(computeOrientations(C));
  v3 = nv;

  testConstruction();
}

TEST_F(TestDoubleTetrahedron, updateEdgeFlip)
{
  auto ne = edge_t{};

  {
    auto flips = C.match(e2, Q, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, ne));
  }

  {
    auto flips = C.match(Q, e2, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, ne, e2));
  }

  {
    auto flips = C.match(Q, Q, Q, e2);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, ne));
  }

  ASSERT_TRUE(computeOrientations(C));
  e2 = ne;

  testConstruction();
}

TEST_F(TestDoubleTetrahedron, updateFaceFlip)
{
  auto nf = face_t{};

  {
    auto flips = C.match(f6, Q, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nf));
  }

  {
    auto flips = C.match(Q, f6, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nf, f6));
  }

  {
    auto flips = C.match(Q, Q, Q, f6);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nf));
  }

  ASSERT_TRUE(computeOrientations(C));
  f6 = nf;

  testConstruction();
}


TEST_F(TestDoubleTetrahedron, updateCellFlip)
{
  auto nc = cell_t{};

  {
    auto flips = C.match(c1, Q, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nc));
  }

  {
    auto flips = C.match(Q, c1, Q, Q);
    for(auto pfl: flips)
      ASSERT_TRUE(C.updateFlip(pfl, nc, c1));
  }

  ASSERT_TRUE(computeOrientations(C));
  c1 = nc;

  testConstruction();
}

TEST_F(TestDoubleTetrahedron, global_queries)
{
  EXPECT_TRUE(CC.border(v1));
  EXPECT_TRUE(CC.border(v2));
  EXPECT_TRUE(CC.border(v3));
  EXPECT_TRUE(CC.border(v4));
  EXPECT_TRUE(CC.border(v5));

  EXPECT_TRUE(CC.border(e1));
  EXPECT_TRUE(CC.border(e2));
  EXPECT_TRUE(CC.border(e3));
  EXPECT_TRUE(CC.border(e4));
  EXPECT_TRUE(CC.border(e5));
  EXPECT_TRUE(CC.border(e6));
  EXPECT_TRUE(CC.border(e7));
  EXPECT_TRUE(CC.border(e8));
  EXPECT_TRUE(CC.border(e9));

  EXPECT_TRUE(CC.border(f1));
  EXPECT_TRUE(CC.border(f2));
  EXPECT_FALSE(CC.border(f3));
  EXPECT_TRUE(CC.border(f4));
  EXPECT_TRUE(CC.border(f5));
  EXPECT_TRUE(CC.border(f6));
  EXPECT_TRUE(CC.border(f7));

  EXPECT_TRUE(CC.border(c1));
  EXPECT_TRUE(CC.border(c2));
}

struct TestDividedTetrahedron : public ::testing::Test, public DividedTetrahedron
{
  virtual void SetUp()
  {
    create();
  }

  void testConstruction()
  {
    EXPECT_EQ(5, CC.nbVertices());
    EXPECT_EQ(8, CC.nbEdges());
    EXPECT_EQ(8, CC.nbFaces());
    EXPECT_EQ(5, CC.nbCells());

    // Test bounds of vertices
    for(auto v: CC.vertices())
      EXPECT_EQ(set({CC._}), CC.bounds(v));

    EXPECT_EQ(8, CC.match(Q, Q, Q, CC._).size());

    EXPECT_EQ(v1, CC.flip(e1, v2, CC._));
    EXPECT_EQ(v3, CC.flip(e2, v2, CC._));
    EXPECT_EQ(v4, CC.flip(e4, v2, CC._));
    EXPECT_EQ(v4, CC.flip(e5, v1, CC._));
    EXPECT_EQ(v3, CC.flip(e6, v4, CC._));
    EXPECT_EQ(v3, CC.flip(e7, v5, CC._));
    EXPECT_EQ(v1, CC.flip(e8, v5, CC._));
    EXPECT_EQ(v5, CC.flip(e9, v4, CC._));

    EXPECT_EQ(3, CC.match(f1, Q, Q, Q).size());
    EXPECT_EQ(3, CC.match(f2, Q, Q, Q).size());
    EXPECT_EQ(4, CC.match(f4, Q, Q, Q).size());
    EXPECT_EQ(3, CC.match(f5, Q, Q, Q).size());
    EXPECT_EQ(3, CC.match(f6, Q, Q, Q).size());

    EXPECT_EQ(e1, CC.flip(f1, e4, v2));
    EXPECT_EQ(e5, CC.flip(f1, e4, v4));
    EXPECT_EQ(e1, CC.flip(f1, e5, v1));

    EXPECT_EQ(e2, CC.flip(f2, e6, v3));
    EXPECT_EQ(e6, CC.flip(f2, e4, v4));
    EXPECT_EQ(e2, CC.flip(f2, e4, v2));

    EXPECT_EQ(e7, CC.flip(f4, e2, v3));
    EXPECT_EQ(e2, CC.flip(f4, e1, v2));
    EXPECT_EQ(e8, CC.flip(f4, e1, v1));
    EXPECT_EQ(e7, CC.flip(f4, e8, v5));

    EXPECT_EQ(e7, CC.flip(f5, e9, v5));
    EXPECT_EQ(e6, CC.flip(f5, e9, v4));
    EXPECT_EQ(e6, CC.flip(f5, e7, v3));

    EXPECT_EQ(e5, CC.flip(f6, e8, v1));
    EXPECT_EQ(e5, CC.flip(f6, e9, v4));
    EXPECT_EQ(e8, CC.flip(f6, e9, v5));

    EXPECT_EQ(8, CC.match(CC.T, Q, Q, Q).size());

    EXPECT_EQ(f1, CC.flip(CC.T, f2, e4));
    EXPECT_EQ(f6, CC.flip(CC.T, f1, e5));
    EXPECT_EQ(f1, CC.flip(CC.T, f4, e1));
    EXPECT_EQ(f2, CC.flip(CC.T, f4, e2));
    EXPECT_EQ(f5, CC.flip(CC.T, f2, e6));
    EXPECT_EQ(f5, CC.flip(CC.T, f4, e7));
    EXPECT_EQ(f6, CC.flip(CC.T, f4, e8));
    EXPECT_EQ(f5, CC.flip(CC.T, f6, e9));
  }
};

TEST_F(TestDividedTetrahedron, construction)
{
  testConstruction();
}

TEST_F(TestDividedTetrahedron, neighbors)
{
  EXPECT_TRUE(CC.areNeighbors(e1, e4));
  EXPECT_TRUE(CC.areNeighbors(e1, e2));
  EXPECT_TRUE(CC.areConeighbors(e8, e2));

  EXPECT_FALSE(CC.areNeighbors(e1, e9));
  EXPECT_FALSE(CC.areNeighbors(e8, e2));
  EXPECT_FALSE(CC.areConeighbors(e8, e6));

  EXPECT_EQ(set({e8, e1, e2, e6, e9}), CC.coneighbors(e7));
  EXPECT_TRUE(set({e8, e1, e2, e4, e6, e9}).contains(CC.anyConeighbor(e7)));
  EXPECT_EQ(set({f1, f2, f4, f5}), CC.coneighbors(f6));
  EXPECT_TRUE(set({f1, f2, f4, f5}).contains(CC.anyConeighbor(f6)));

  EXPECT_EQ(4, CC.nbAdjacentCells(e8));
  EXPECT_EQ(set({e1, e5, e7, e9}), CC.adjacentCells(e8));
  EXPECT_TRUE(set({e1, e5, e7, e9}).contains(CC.anyAdjacentCell(e8)));

  EXPECT_EQ(cell_t::null, CC.anyConeighbor(f_out));
  EXPECT_EQ(cell_t::null, CC.anyAdjacentCell(f_out));
}

TEST_F(TestDividedTetrahedron, bounds)
{
  EXPECT_EQ(3, CC.nbBounds(f1));
  EXPECT_EQ(4, CC.nbBounds(f4));
  EXPECT_EQ(5, CC.nbBounds<0>(CC.T));
  EXPECT_EQ(5, CC.nbVertices(CC.T));
  EXPECT_EQ(8, CC.nbBounds<1>(CC.T));
  EXPECT_EQ(8, CC.nbJoints(CC.T));

  EXPECT_EQ(4, CC.nbBounds<0>(set({f1, f6})));
  EXPECT_EQ(5, CC.nbBounds(set({f1, f6})));
  EXPECT_EQ(4, CC.nbJoints(set({f1, f6})));

  EXPECT_TRUE(CC.isBound(f1, e4));
  EXPECT_TRUE(CC.isBound(f1, v2));
  EXPECT_FALSE(CC.isBound(f1, e7));
  EXPECT_FALSE(CC.isFace(f1, e7));
  EXPECT_FALSE(CC.isBound(f1, v5));

  EXPECT_EQ(set({CC._}), CC.joints(e2));
  EXPECT_EQ(set({v1, v2, v4}), CC.joints(f1));
  EXPECT_EQ(set({v2, v3, v4}), CC.vertices(f2));
  EXPECT_EQ(set({v1, v2, v4}), CC.bounds<0>(f1));
  EXPECT_EQ(set({v1, v2, v3, v4}), CC.joints(set({f1, f2})));
  EXPECT_EQ(set({v1, v2, v3, v4}), CC.vertices(set({f1, f2})));
  EXPECT_EQ(4, CC.nbVertices(set({f1, f2})));
  EXPECT_TRUE(CC.isVertex(f1, v1));
  EXPECT_FALSE(CC.isVertex(f1, v5));

  EXPECT_TRUE(CC.isJoint(e2, CC._));
  EXPECT_TRUE(CC.isJoint(f2, v3));
  EXPECT_FALSE(CC.isJoint(f2, v1));

  EXPECT_EQ(vertex_t::null, CC.anyJoint(f_out));
  EXPECT_EQ(bottom_t::null, CC.anyJoint(e_out));
  EXPECT_EQ(CC._, CC.anyJoint(e4));

  EXPECT_TRUE(set({e1, e2, e7, e8}).contains(CC.anyEdge(f4)));
  EXPECT_EQ(edge_t::null, CC.anyEdge(f_out));
  EXPECT_EQ(set({e1, e2, e7, e8}), CC.edges(f4));
  EXPECT_EQ(set({e1, e2, e7, e8}), CC.faces(f4));
  EXPECT_EQ(set({e1, e2, e7, e8, e9, e6}), CC.faces(set({f4, f5})));
  EXPECT_EQ(set({e1, e2, e7, e8, e9, e6}), CC.edges(set({f4, f5})));
  EXPECT_EQ(4, CC.nbEdges(f4));
  EXPECT_EQ(6, CC.nbEdges(set({f4, f5})));
  EXPECT_TRUE(CC.isEdge(f4, e2));
  EXPECT_FALSE(CC.isEdge(f4, e9));

  EXPECT_EQ(4, CC.nbFaces(f4));
  EXPECT_EQ(6, CC.nbFaces(set({f4, f5})));

  EXPECT_TRUE(set({v1, v2, v4}).contains(CC.anyBound<0>(f1)));
  EXPECT_TRUE(set({v1, v2, v4}).contains(CC.anyVertex(f1)));
  EXPECT_TRUE(set({v1, v2, v4}).contains(CC.anyJoint(f1)));
  EXPECT_TRUE(set({e1, e4, e5}).contains(CC.anyBound(f1)));
  EXPECT_TRUE(set({e1, e4, e5}).contains(CC.anyFace(f1)));

  EXPECT_EQ(CC._, CC.anyBound(v2));
  EXPECT_EQ(CC._, CC.anyBound<-1>(v3));
  EXPECT_EQ(edge_t::null, CC.anyBound(f_out));
  EXPECT_EQ(bottom_t::null, CC.anyBound(v_out));
}

TEST_F(TestDividedTetrahedron, cobounds)
{
  EXPECT_EQ(2, CC.nbCofaces(e8));
  EXPECT_EQ(2, CC.nbCobounds(e8));
  EXPECT_EQ(3, CC.nbCobounds<2>(v1));

  EXPECT_EQ(set({f6, f4}), CC.cofaces(e8));
  EXPECT_EQ(set({f6, f4}), CC.cobounds(e8));

  EXPECT_EQ(set({f6, f4}), set(cofaces(CC, e8)));
  EXPECT_EQ(set({f6, f4}), set(cobounds(CC, e8)));

  EXPECT_TRUE(set({f6, f4}).contains(CC.anyCoface(e8)));
  EXPECT_TRUE(set({f6, f4}).contains(CC.anyCobound(e8)));
  EXPECT_EQ(cell_t::null, CC.anyCobound(e_out));
  EXPECT_EQ(CC.T, CC.anyCobound(f2));
  EXPECT_EQ(top_t::null, CC.anyCobound(f_out));
  EXPECT_EQ(set({f1, f6, f4}), CC.cobounds<2>(v1));
  EXPECT_EQ(set({f1, f6, f4}), CC.cojoints(v1));
  EXPECT_EQ(set({f1, f6, f4}), set(cojoints(CC,v1)));
  EXPECT_EQ(set({f1, f2, f6, f4}), CC.cojoints(set({v1, v2})));
  EXPECT_TRUE(set({f1, f6, f4}).contains(CC.anyCojoint(v1)));
  EXPECT_EQ(set({CC.T}), CC.cojoints(e1));
  EXPECT_EQ(set({CC.T}), set(cojoints(CC,e1)));
  EXPECT_TRUE(CC.cojoints(e_out).empty());
  EXPECT_TRUE(cojoints(CC,e_out).empty());
  EXPECT_EQ(CC.T, CC.anyCojoint(e1));
  EXPECT_EQ(top_t::null, CC.anyCojoint(e_out));
  EXPECT_EQ(cell_t::null, CC.anyCojoint(v_out));

  EXPECT_EQ(0, CC.nbCojoints(e_out));
  EXPECT_EQ(3, CC.nbCojoints(v1));
  EXPECT_EQ(4, CC.nbCojoints(set({v1, v2})));

  EXPECT_TRUE(CC.isCojoint(v1, f6));
  EXPECT_FALSE(CC.isCojoint(v1, f2));
  EXPECT_FALSE(CC.isCojoint(e_out, CC.T));

  EXPECT_EQ(3, CC.nbCobounds(set({e8, e9})));
  EXPECT_EQ(set({f4, f6, f5}), CC.cobounds(set({e8, e9})));
  EXPECT_EQ(3, CC.nbCofaces(set({e8, e9})));
  EXPECT_EQ(set({f4, f6, f5}), CC.cofaces(set({e8, e9})));
}

TEST_F(TestDividedTetrahedron, global_queries)
{
  EXPECT_FALSE(CC.border(v1));
  EXPECT_FALSE(CC.border(e1));
  EXPECT_FALSE(CC.border(f1));
}

TEST_F(TestDividedTetrahedron, ordered_topology)
{
  EXPECT_EQ(cellflips::invalid, CC.relativeOrientation(+e1, -e_out));
  EXPECT_EQ(cellflips::neg, CC.relativeOrientation(+e1, -e7));
  EXPECT_EQ(cellflips::invalid, CC.adjacentOrientation(+e1, -e7));
  EXPECT_EQ(cellflips::neg, CC.adjacentOrientation(+e1, -e8));
  EXPECT_EQ(cellflips::pos, CC.relativeOrientation(+f1, +f5));
  EXPECT_EQ(cellflips::invalid, CC.adjacentOrientation(+f1, +f5));
  EXPECT_EQ(cellflips::invalid, CC.adjacentOrientation(+f1, +f_out));
  EXPECT_EQ(cellflips::invalid, CC.relativeOrientation(+f1, +f_out));
  EXPECT_EQ(cellflips::invalid, CC.adjacentOrientation(+v1, +v_out));
  EXPECT_EQ(cellflips::invalid, CC.relativeOrientation(+v1, +v_out));
}

