#include <gtest/gtest.h>
#include <util/set_vector.h>

#include <QTextStream>
#include <stdio.h>
QTextStream out(stdout);

struct Simple
{
  int a = 1;
  double b = 2.5;
};

bool operator<(const Simple& s1, const Simple& s2)
{
  if(s1.a < s2.a) return true;
  if(s1.a > s2.a) return false;
  return s1.b < s2.b;
}

TEST(TestSetVector, construction)
{
  {
    auto vs = util::set_vector<int>{};
    ASSERT_TRUE(vs.empty());
  }

  {
    auto vs = util::set_vector<int>{1,3,4,5,3,5,2,6};
    ASSERT_FALSE(vs.empty());
    ASSERT_EQ(6, vs.size());

    util::set_vector<int> vs1;
    vs1 = vs;
    ASSERT_FALSE(vs1.empty());
    ASSERT_EQ(6, vs1.size());

    vs1 = std::move(vs);
    ASSERT_FALSE(vs1.empty());
    ASSERT_EQ(6, vs1.size());

    vs1 = {1,2,3,4,5,6,7,5,4};
    ASSERT_FALSE(vs1.empty());
    ASSERT_EQ(7, vs1.size());
  }
}

TEST(TestSetVector, find)
{
  auto vvs = util::set_vector<int>{2,4,5,6,8,12,43,-4,-3};
  const auto& vs = vvs;

  ASSERT_EQ(5, *vvs.find(5));
  ASSERT_EQ(5, *vs.find(5));
  ASSERT_EQ(vs.end(), vs.find(100));
  ASSERT_EQ(vs.end(), vs.find(10));
  ASSERT_EQ(vs.end(), vs.find(-10));
  ASSERT_EQ(vs.end(), vs.find(-100));

  ASSERT_EQ(1, vvs.count(4));
  ASSERT_EQ(1, vs.count(4));
  ASSERT_EQ(1, vs.count(12));
  ASSERT_EQ(0, vs.count(10));

  ASSERT_EQ(5, *vvs.lower_bound(5));
  ASSERT_EQ(5, *vs.lower_bound(5));
  ASSERT_EQ(8, *vs.lower_bound(7));
  ASSERT_EQ(vs.end(), vs.lower_bound(700));
  ASSERT_EQ(vs.begin(), vs.lower_bound(-700));

  ASSERT_EQ(vs.end(), vvs.upper_bound(43));
  ASSERT_EQ(vs.end(), vs.upper_bound(43));
  ASSERT_EQ(6, *vs.upper_bound(5));
  ASSERT_EQ(8, *vs.upper_bound(7));

  {
    auto result = vs.equal_range(12);
    ASSERT_EQ(12, *result.first);
    ASSERT_EQ(43, *result.second);
  }

  {
    auto result = vs.equal_range(10);
    ASSERT_EQ(12, *result.first);
    ASSERT_EQ(12, *result.second);
  }

  ASSERT_EQ(std::make_pair(vs.end(), vs.end()), vs.equal_range(100));
  ASSERT_EQ(std::make_pair(vs.begin(), vs.begin()), vs.equal_range(-100));
  ASSERT_EQ(std::make_pair(vvs.begin(), vvs.begin()), vvs.equal_range(-100));
}

TEST(TestSetVector, modification)
{
  {
    auto vs = util::set_vector<int>{1,2,3,4,5,6};
    ASSERT_EQ(6, vs.size());

    {
      auto result = vs.insert(3);
      ASSERT_FALSE(result.second);
      ASSERT_EQ(3, *result.first);

      result = vs.insert(10);
      ASSERT_TRUE(result.second);
      ASSERT_EQ(10, *result.first);

      int val = -5;
      result = vs.insert(val);
      ASSERT_TRUE(result.second);
      ASSERT_EQ(-5, *result.first);

      ASSERT_EQ(8, vs.size());

      auto vals = {2,3,4};
      vs.insert(vals.begin(), vals.end());
      ASSERT_EQ(8, vs.size());

      vs.insert({2,3,4,5,6});
      ASSERT_EQ(8, vs.size());

    }

    {
      auto result = vs.insert(vs.find(10), 9);
      ASSERT_TRUE(result.second);
      ASSERT_EQ(9, *result.first);

      result = vs.insert(vs.begin(), -10);
      ASSERT_TRUE(result.second);
      ASSERT_EQ(-10, *result.first);

      result = vs.insert(vs.end()-1, 10);
      ASSERT_FALSE(result.second);
      ASSERT_EQ(10, *result.first);

      result = vs.insert(vs.end(), 12);
      ASSERT_TRUE(result.second);
      ASSERT_EQ(12, *result.first);

      int val = 3;
      result = vs.insert(vs.end(), val);
      ASSERT_FALSE(result.second);
      ASSERT_EQ(3, *result.first);

      val = -3;
      result = vs.insert(vs.end()-2, val);
      ASSERT_TRUE(result.second);
      ASSERT_EQ(-3, *result.first);

      result = vs.insert(vs.begin(), 8);
      ASSERT_TRUE(result.second);
      ASSERT_EQ(8, *result.first);

      result = vs.insert(vs.end()-1, 20);
      ASSERT_TRUE(result.second);
      ASSERT_EQ(20, *result.first);
    }

    auto s = vs.size();

    {
      ASSERT_EQ(1, vs.erase(-10));
      ASSERT_EQ(0, vs.count(-10));

      ASSERT_EQ(s-1, vs.size());

      ASSERT_EQ(0, vs.erase(-10));
      ASSERT_EQ(0, vs.erase(-100));

      auto found = vs.find(12);
      ASSERT_TRUE(found != vs.end());
      auto next = *(found+1);
      auto result = vs.erase(found);
      ASSERT_EQ(next, *result);
      ASSERT_TRUE(vs.find(next) == result);

      auto it1 = vs.find(3);
      auto it2 = vs.find(6);
      auto last = vs.erase(it1, it2);
      ASSERT_EQ(6, *last);
      ASSERT_TRUE(vs.count(3) == 0);
      ASSERT_TRUE(vs.count(4) == 0);
      ASSERT_TRUE(vs.count(5) == 0);
    }

    vs.clear();
  }

  {
    auto vs = util::set_vector<int>{};
    auto result = vs.insert(vs.end(), 2);

    ASSERT_TRUE(result.second);
    ASSERT_EQ(2, *result.first);
  }
}

TEST(TestSetVector, comparison)
{
  auto v1 = util::set_vector<int>{1,2,3,4,5,6};
  auto v2 = util::set_vector<int>{1,2,3,4,3,2,4,5,4,4,2,6};
  auto v3 = util::set_vector<int>{1,8,3,4,3,2,4,5,4,4,2,6};
  auto v4 = util::set_vector<int>{1,8,3,4,3,2,4,5,4,4,2};

  EXPECT_EQ(6, v1.size());
  EXPECT_EQ(6, v2.size());
  EXPECT_EQ(7, v3.size());
  EXPECT_EQ(6, v4.size());

  EXPECT_TRUE(v1 == v2);
  EXPECT_FALSE(v1 != v2);
  EXPECT_FALSE(v1 == v3);
  EXPECT_TRUE(v1 != v3);
  EXPECT_FALSE(v1 == v4);
  EXPECT_TRUE(v1 != v4);

  EXPECT_TRUE(v1 < util::set_vector<int>({1,2,3,4,5,6,7}));
  EXPECT_FALSE(v1 > util::set_vector<int>({1,2,3,4,5,6,7}));
  EXPECT_TRUE(v1 < util::set_vector<int>({1,2,3,4,6,7}));
  EXPECT_FALSE(v1 > util::set_vector<int>({1,2,3,4,6,7}));
  EXPECT_TRUE(v1 > util::set_vector<int>({1,2,3,4,5}));
  EXPECT_FALSE(v1 < util::set_vector<int>({1,2,3,4,5}));
  EXPECT_TRUE(v1 > util::set_vector<int>({-1,2,3,4,5}));
  EXPECT_FALSE(v1 < util::set_vector<int>({-1,2,3,4,5}));
  EXPECT_FALSE(v1 > util::set_vector<int>({1,2,3,4,5,6}));
  EXPECT_FALSE(v1 < util::set_vector<int>({1,2,3,4,5,6}));

  EXPECT_FALSE(v1 <= util::set_vector<int>({1,2,3,4,5}));
  EXPECT_TRUE(v1 >= util::set_vector<int>({1,2,3,4,5}));
  EXPECT_TRUE(v1 <= util::set_vector<int>({1,2,3,4,5,6,7}));
  EXPECT_TRUE(v1 <= util::set_vector<int>({1,2,3,4,5,6}));
  EXPECT_TRUE(v1 >= util::set_vector<int>({1,2,3,4,5,6}));
}

TEST(TestMultiSetVector, construction)
{
  {
    auto vs = util::multiset_vector<int>{};
    ASSERT_TRUE(vs.empty());
  }

  {
    auto vs = util::multiset_vector<int>{1,3,4,5,3,5,2,6};
    ASSERT_FALSE(vs.empty());
    ASSERT_EQ(8, vs.size());

    util::multiset_vector<int> vs1;
    vs1 = vs;
    ASSERT_FALSE(vs1.empty());
    ASSERT_EQ(8, vs1.size());

    vs1 = std::move(vs);
    ASSERT_FALSE(vs1.empty());
    ASSERT_EQ(8, vs1.size());

    vs1 = {1,2,3,4,5,6,7,5,4};
    ASSERT_FALSE(vs1.empty());
    ASSERT_EQ(9, vs1.size());
  }
}

TEST(TestMultiSetVector, find)
{
  auto vvs = util::multiset_vector<int>{2,4,5,6,8,12,8,5,43,-4,-3};
  const auto& vs = vvs;

  ASSERT_EQ(5, *vvs.find(5));
  ASSERT_EQ(5, *vs.find(5));
  ASSERT_EQ(vs.end(), vs.find(100));
  ASSERT_EQ(vs.end(), vs.find(10));
  ASSERT_EQ(vs.end(), vs.find(-10));
  ASSERT_EQ(vs.end(), vs.find(-100));

  ASSERT_EQ(1, vvs.count(4));
  ASSERT_EQ(1, vs.count(4));
  ASSERT_EQ(1, vs.count(12));
  ASSERT_EQ(0, vs.count(10));
  ASSERT_EQ(2, vs.count(5));

  ASSERT_EQ(5, *vvs.lower_bound(5));
  ASSERT_EQ(5, *vs.lower_bound(5));
  ASSERT_EQ(8, *vs.lower_bound(7));
  ASSERT_EQ(vs.end(), vs.lower_bound(700));
  ASSERT_EQ(vs.begin(), vs.lower_bound(-700));

  ASSERT_EQ(vs.end(), vvs.upper_bound(43));
  ASSERT_EQ(vs.end(), vs.upper_bound(43));
  ASSERT_EQ(6, *vs.upper_bound(5));
  ASSERT_EQ(8, *vs.upper_bound(7));

  {
    auto result = vs.equal_range(12);
    ASSERT_EQ(12, *result.first);
    ASSERT_EQ(43, *result.second);
  }

  {
    auto result = vs.equal_range(10);
    ASSERT_EQ(12, *result.first);
    ASSERT_EQ(12, *result.second);
  }

  {
    auto result = vs.equal_range(8);
    ASSERT_EQ(8, *result.first);
    ASSERT_EQ(12, *result.second);
    ASSERT_EQ(2, std::distance(result.first, result.second));
  }

  ASSERT_EQ(std::make_pair(vs.end(), vs.end()), vs.equal_range(100));
  ASSERT_EQ(std::make_pair(vs.begin(), vs.begin()), vs.equal_range(-100));
  ASSERT_EQ(std::make_pair(vvs.begin(), vvs.begin()), vvs.equal_range(-100));
}

struct IntIntComp
{
  bool operator()(const std::pair<int,int>& p1, const std::pair<int,int>& p2) const
  {
    return p1.first < p2.first;
  }
};

TEST(TestMultiSetVector, modification)
{
  {
    auto vs = util::multiset_vector<int>{1,2,3,4,5,6};
    ASSERT_EQ(6, vs.size());

    auto result = vs.insert(3);
    ASSERT_EQ(3, *result);

    result = vs.insert(10);
    ASSERT_EQ(10, *result);

    int val = -5;
    result = vs.insert(val);
    ASSERT_EQ(-5, *result);

    ASSERT_EQ(9, vs.size());

    auto vals = {2,3,4};
    vs.insert(vals.begin(), vals.end());
    ASSERT_EQ(12, vs.size());

    vs.insert({2,3,4,5,6});
    ASSERT_EQ(17, vs.size());
    ASSERT_EQ(3, vs.count(2));
  }

  {
    auto vs = util::multiset_vector<int>{1,2,3,4,5,6};
    ASSERT_EQ(6, vs.size());

    auto result = vs.insert(vs.find(10), 9);
    ASSERT_EQ(9, *result);

    result = vs.insert(vs.begin(), -10);
    ASSERT_EQ(-10, *result);

    result = vs.insert(vs.end()-1, 10);
    ASSERT_EQ(10, *result);

    result = vs.insert(vs.end(), 12);
    ASSERT_EQ(12, *result);

    int val = 3;
    result = vs.insert(vs.end(), val);
    ASSERT_EQ(3, *result);

    val = -3;
    result = vs.insert(vs.end()-2, val);
    ASSERT_EQ(-3, *result);

    result = vs.insert(vs.begin(), 8);
    ASSERT_EQ(8, *result);

    result = vs.insert(vs.end()-1, 20);
    ASSERT_EQ(20, *result);
  }

  {
    auto vs = util::multiset_vector<int>{1,2,3,4,5,6,10,10,11,12,16,2,3,2,3};
    auto s = vs.size();

    ASSERT_EQ(2, vs.erase(10));
    ASSERT_EQ(0, vs.count(10));

    ASSERT_EQ(s-2, vs.size());

    ASSERT_EQ(0, vs.erase(-10));
    ASSERT_EQ(0, vs.erase(-100));

    auto found = vs.find(2);
    ASSERT_TRUE(found != vs.end());
    auto next = *(found+1);
    auto result = vs.erase(found);
    ASSERT_EQ(next, *result);
    ASSERT_TRUE(vs.find(next) == result);

    auto it1 = vs.find(3);
    auto it2 = vs.find(6);
    auto last = vs.erase(it1, it2);
    ASSERT_EQ(6, *last);
    ASSERT_TRUE(vs.count(3) == 0);
    ASSERT_TRUE(vs.count(4) == 0);
    ASSERT_TRUE(vs.count(5) == 0);

    vs.clear();
  }

  {
    auto vs = util::multiset_vector<int>{};
    auto result = vs.insert(vs.end(), 2);

    ASSERT_EQ(2, *result);
  }

  {
    auto vs = util::multiset_vector<std::pair<int,int>,IntIntComp>{};
    vs.emplace(2,0);
    vs.emplace(4,0);
    vs.emplace(3,0);
    vs.emplace(5,0);

    auto it = vs.find(std::make_pair(2,1));
    ASSERT_EQ(std::make_pair(2,0), *it);

    it = vs.emplace_hint(it, 2, 1);
    ASSERT_EQ(std::make_pair(2,1), *it);
    ASSERT_EQ(std::make_pair(2,0), *(it+1));
    ++it;
    it = vs.emplace_hint(it, 2, 2);
    ASSERT_EQ(std::make_pair(2,2), *it);
    ASSERT_EQ(std::make_pair(2,0), *(it+1));
    ASSERT_EQ(std::make_pair(2,1), *(it-1));
  }
}

TEST(TestMultiSetVector, comparison)
{
  auto v1 = util::multiset_vector<int>{1,2,3,4,5,6};
  auto v2 = util::multiset_vector<int>{1,2,5,4,3,6};
  auto v3 = util::multiset_vector<int>{1,2,3,4,3,2,4,5,4,4,2,9};
  auto v4 = util::multiset_vector<int>{1,8,3,4,3,2,4,5,4,4,2,2};

  EXPECT_EQ(6, v1.size());
  EXPECT_EQ(6, v2.size());
  EXPECT_EQ(12, v3.size());
  EXPECT_EQ(12, v4.size());

  EXPECT_TRUE(v1 == v2);
  EXPECT_FALSE(v1 != v2);
  EXPECT_FALSE(v1 == v3);
  EXPECT_TRUE(v1 != v3);
  EXPECT_FALSE(v1 == v4);
  EXPECT_TRUE(v1 != v4);

  EXPECT_TRUE(v1 < util::multiset_vector<int>({1,2,3,4,5,6,7}));
  EXPECT_FALSE(v1 > util::multiset_vector<int>({1,2,3,4,5,6,7}));
  EXPECT_TRUE(v1 < util::multiset_vector<int>({1,2,3,4,6,7}));
  EXPECT_FALSE(v1 > util::multiset_vector<int>({1,2,3,4,6,7}));
  EXPECT_TRUE(v1 > util::multiset_vector<int>({1,2,3,4,5}));
  EXPECT_FALSE(v1 < util::multiset_vector<int>({1,2,3,4,5}));
  EXPECT_TRUE(v1 > util::multiset_vector<int>({-1,2,3,4,5}));
  EXPECT_FALSE(v1 < util::multiset_vector<int>({-1,2,3,4,5}));
  EXPECT_FALSE(v1 > util::multiset_vector<int>({1,2,3,4,5,6}));
  EXPECT_FALSE(v1 < util::multiset_vector<int>({1,2,3,4,5,6}));

  EXPECT_FALSE(v1 <= util::multiset_vector<int>({1,2,3,4,5}));
  EXPECT_TRUE(v1 >= util::multiset_vector<int>({1,2,3,4,5}));
  EXPECT_TRUE(v1 <= util::multiset_vector<int>({1,2,3,4,5,6,7}));
  EXPECT_TRUE(v1 <= util::multiset_vector<int>({1,2,3,4,5,6}));
  EXPECT_TRUE(v1 >= util::multiset_vector<int>({1,2,3,4,5,6}));

  EXPECT_TRUE(v3 > v4);
  EXPECT_TRUE(v3 >= v4);
  EXPECT_FALSE(v3 < v4);
  EXPECT_FALSE(v3 <= v4);
}

