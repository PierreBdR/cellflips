#ifndef TESTS_FIGURES_H
#define TESTS_FIGURES_H

#include "tests.h"

struct Content
{
  int i = 0;
};

inline int num(const Content& cnt)
{
  return cnt.i;
}

typedef cellflips::CellComplex<Content, Content> CellComplex1d;
typedef cellflips::CellComplex<Content, Content, Content> CellComplex2d;
typedef cellflips::CellComplex<Content, Content, Content, Content> CellComplex3d;

struct BrokenCircle
{
  typedef CellComplex1d::bottom_t bottom_t;
  typedef CellComplex1d::vertex_t vertex_t;
  typedef CellComplex1d::cell_t cell_t;
  typedef CellComplex1d::top_t top_t;

  BrokenCircle()
    : CC(C)
  { }

  void create()
  {
    v1->i = 1;
    v2->i = 2;
    v3->i = 3;
    v4->i = 4;

    C.addVertex(v1);
    C.addVertex(v2);
    C.addVertex(v3);
    C.addVertex(v4);

    c1->i = 1;
    c2->i = 2;
    c3->i = 3;
    c4->i = 4;

    ASSERT_NE(nullptr, C.addFlip(c1, v2, v1, C._));
    ASSERT_NE(nullptr, C.addFlip(c2, v3, v2, C._));
    ASSERT_NE(nullptr, C.addFlip(c3, v4, v3, C._));
    ASSERT_NE(nullptr, C.addFlip(c4, v1, v4, C._));

    ASSERT_NE(nullptr, C.addFlip(C.T, c1, c2, v2));
    ASSERT_NE(nullptr, C.addFlip(C.T, c2, c3, v3));
    ASSERT_NE(nullptr, C.addFlip(C.T, c3, c4, v4));
    ASSERT_NE(nullptr, C.addFlip(C.T, c4, c1, v1));

    ASSERT_TRUE(computeOrientations(C));
  }

  vertex_t v1, v2, v3, v4;
  cell_t c1, c2, c3, c4;
  CellComplex1d C;
  const CellComplex1d& CC;
};

struct BrokenLine
{
  typedef CellComplex1d::bottom_t bottom_t;
  typedef CellComplex1d::vertex_t vertex_t;
  typedef CellComplex1d::cell_t cell_t;
  typedef CellComplex1d::top_t top_t;

  BrokenLine()
    : CC(C)
  { }

  void create()
  {
    v1->i = 1;
    v2->i = 2;
    v3->i = 3;
    v4->i = 4;

    C.addVertex(v1);
    C.addVertex(v2);
    C.addVertex(v3);
    C.addVertex(v4);

    c1->i = 1;
    c2->i = 2;
    c3->i = 3;

    ASSERT_NE(nullptr, C.addFlip(c1, v2, v1, C._));
    ASSERT_NE(nullptr, C.addFlip(c2, v3, v2, C._));
    ASSERT_NE(nullptr, C.addFlip(c3, v4, v3, C._));

    ASSERT_NE(nullptr, C.addFlip(C.T, c1, c2, v2));
    ASSERT_NE(nullptr, C.addFlip(C.T, c2, c3, v3));

    ASSERT_TRUE(computeOrientations(C));
  }

  vertex_t v1, v2, v3, v4;
  cell_t c1, c2, c3;
  CellComplex1d C;
  const CellComplex1d& CC;
};

struct DoubleTriangle
{
  typedef CellComplex2d::bottom_t bottom_t;
  typedef CellComplex2d::vertex_t vertex_t;
  typedef CellComplex2d::edge_t edge_t;
  typedef CellComplex2d::oriented_edge_t oriented_edge_t;
  typedef CellComplex2d::cell_t cell_t;
  typedef CellComplex2d::top_t top_t;

  DoubleTriangle()
    : CC(C)
  { }

  void create()
  {
    v1->i = 1;
    v2->i = 2;
    v3->i = 3;
    v4->i = 4;

    // Add vertices
    C.addVertex(v1);
    C.addVertex(v2);
    C.addVertex(v3);
    C.addVertex(v4);

    e1->i = 1;
    e2->i = 2;
    e3->i = 3;
    e4->i = 4;
    e5->i = 5;

    // Add edge flips
    ASSERT_NE(nullptr, C.addFlip(e1, v2, v1, C._));
    ASSERT_NE(nullptr, C.addFlip(e2, v3, v2, C._));
    ASSERT_NE(nullptr, C.addFlip(e3, v3, v1, C._));
    ASSERT_NE(nullptr, C.addFlip(e4, v3, v4, C._));
    ASSERT_NE(nullptr, C.addFlip(e5, v4, v2, C._));

    c1->i = 1;
    c2->i = 2;

    // Add cell flips
    ASSERT_NE(nullptr, C.addFlip(c1, e1, e2, v2));
    ASSERT_NE(nullptr, C.addFlip(c1, e2, e3, v3));
    ASSERT_NE(nullptr, C.addFlip(c1, e3, e1, v1));

    ASSERT_NE(nullptr, C.addFlip(c2, e2, e5, v2));
    ASSERT_NE(nullptr, C.addFlip(c2, e5, e4, v4));
    ASSERT_NE(nullptr, C.addFlip(c2, e4, e2, v3));

    // Add top-flip
    ASSERT_NE(nullptr, C.addFlip(C.T, c1, c2, e2));

    ASSERT_TRUE(computeOrientations(C));
  }
  vertex_t v1, v2, v3, v4;
  edge_t e1, e2, e3, e4, e5;
  cell_t c1, c2;
  CellComplex2d C;
  const CellComplex2d& CC;
};

struct OrientedStomata
{
  typedef CellComplex2d::bottom_t bottom_t;
  typedef CellComplex2d::vertex_t vertex_t;
  typedef CellComplex2d::edge_t edge_t;
  typedef CellComplex2d::cell_t cell_t;
  typedef CellComplex2d::top_t top_t;

  OrientedStomata()
    : CC(C)
  { }

  void create()
  {
    v1->i = 1;
    v2->i = 2;
    v3->i = 3;
    v4->i = 4;

    // Add vertices
    C.addVertex(v1);
    C.addVertex(v2);
    C.addVertex(v3);
    C.addVertex(v4);

    e1->i = 1;
    e2->i = 2;
    e3->i = 3;
    e4->i = 4;
    e5->i = 5;
    e6->i = 5;

    // Add edge flips
    ASSERT_NE(nullptr, C.addFlip(e1, v2, v1, C._));
    ASSERT_NE(nullptr, C.addFlip(e2, v4, v3, C._));
    ASSERT_NE(nullptr, C.addFlip(e3, v1, v4, C._));
    ASSERT_NE(nullptr, C.addFlip(e4, v2, v3, C._));
    ASSERT_NE(nullptr, C.addFlip(e5, v3, v2, C._));
    ASSERT_NE(nullptr, C.addFlip(e6, v4, v1, C._));

    c1->i = 1;
    c2->i = 2;
    c3->i = 3;

    // Add cell flips
    ASSERT_NE(nullptr, C.addFlip(c1, e1, e4, v2));
    ASSERT_NE(nullptr, C.addFlip(c1, e4, e2, v3));
    ASSERT_NE(nullptr, C.addFlip(c1, e2, e3, v4));
    ASSERT_NE(nullptr, C.addFlip(c1, e3, e1, v1));

    ASSERT_NE(nullptr, C.addFlip(c2, e5, e4, v3));
    ASSERT_NE(nullptr, C.addFlip(c2, e4, e5, v2));

    ASSERT_NE(nullptr, C.addFlip(c3, e5, e1, v2));
    ASSERT_NE(nullptr, C.addFlip(c3, e2, e5, v3));
    ASSERT_NE(nullptr, C.addFlip(c3, e6, e2, v4));
    ASSERT_NE(nullptr, C.addFlip(c3, e1, e6, v1));

    // Add top-flip
    ASSERT_NE(nullptr, C.addFlip(C.T, c1, c3, e1));
    ASSERT_NE(nullptr, C.addFlip(C.T, c1, c3, e2));
    ASSERT_NE(nullptr, C.addFlip(C.T, c2, c1, e4));
    ASSERT_NE(nullptr, C.addFlip(C.T, c2, c3, e5));

    ASSERT_TRUE(computeOrientations(C));
  }
  vertex_t v1, v2, v3, v4;
  edge_t e1, e2, e3, e4, e5, e6, e_out;
  cell_t c1, c2, c3;
  CellComplex2d C;
  const CellComplex2d& CC;
};

struct DoubleTetrahedron
{
  typedef CellComplex3d::bottom_t bottom_t;
  typedef CellComplex3d::vertex_t vertex_t;
  typedef CellComplex3d::edge_t edge_t;
  typedef CellComplex3d::face_t face_t;
  typedef CellComplex3d::cell_t cell_t;
  typedef CellComplex3d::top_t top_t;

  DoubleTetrahedron()
    : CC(C)
  { }

  void create()
  {
    v1->i = 1;
    v2->i = 2;
    v3->i = 3;
    v4->i = 4;
    v5->i = 5;

    // Add vertices
    C.addVertex(v1);
    C.addVertex(v2);
    C.addVertex(v3);
    C.addVertex(v4);
    C.addVertex(v5);

    e1->i = 1;
    e2->i = 2;
    e3->i = 3;
    e4->i = 4;
    e5->i = 5;
    e6->i = 6;
    e7->i = 7;
    e8->i = 8;
    e9->i = 9;

    // Add edge flips
    ASSERT_NE(nullptr, C.addFlip(e1, v2, v1, C._));
    ASSERT_NE(nullptr, C.addFlip(e2, v3, v2, C._));
    ASSERT_NE(nullptr, C.addFlip(e3, v1, v3, C._));
    ASSERT_NE(nullptr, C.addFlip(e4, v4, v2, C._));
    ASSERT_NE(nullptr, C.addFlip(e5, v1, v4, C._));
    ASSERT_NE(nullptr, C.addFlip(e6, v4, v3, C._));
    ASSERT_NE(nullptr, C.addFlip(e7, v5, v1, C._));
    ASSERT_NE(nullptr, C.addFlip(e8, v3, v5, C._));
    ASSERT_NE(nullptr, C.addFlip(e9, v4, v5, C._));

    f1->i = 1;
    f2->i = 2;
    f3->i = 3;
    f4->i = 4;
    f5->i = 5;

    // Add face flips
    ASSERT_NE(nullptr, C.addFlip(f1, e1, e4, v2));
    ASSERT_NE(nullptr, C.addFlip(f1, e4, e5, v4));
    ASSERT_NE(nullptr, C.addFlip(f1, e5, e1, v1));

    ASSERT_NE(nullptr, C.addFlip(f2, e2, e6, v3));
    ASSERT_NE(nullptr, C.addFlip(f2, e6, e4, v4));
    ASSERT_NE(nullptr, C.addFlip(f2, e4, e2, v2));

    ASSERT_NE(nullptr, C.addFlip(f3, e3, e5, v1));
    ASSERT_NE(nullptr, C.addFlip(f3, e5, e6, v4));
    ASSERT_NE(nullptr, C.addFlip(f3, e6, e3, v3));

    ASSERT_NE(nullptr, C.addFlip(f4, e3, e2, v3));
    ASSERT_NE(nullptr, C.addFlip(f4, e2, e1, v2));
    ASSERT_NE(nullptr, C.addFlip(f4, e1, e3, v1));

    ASSERT_NE(nullptr, C.addFlip(f5, e7, e5, v1));
    ASSERT_NE(nullptr, C.addFlip(f5, e5, e9, v4));
    ASSERT_NE(nullptr, C.addFlip(f5, e9, e7, v5));

    ASSERT_NE(nullptr, C.addFlip(f6, e8, e9, v5));
    ASSERT_NE(nullptr, C.addFlip(f6, e9, e6, v4));
    ASSERT_NE(nullptr, C.addFlip(f6, e6, e8, v3));

    ASSERT_NE(nullptr, C.addFlip(f7, e3, e7, v1));
    ASSERT_NE(nullptr, C.addFlip(f7, e7, e8, v5));
    ASSERT_NE(nullptr, C.addFlip(f7, e8, e3, v3));

    c1->i = 1;
    c2->i = 2;

    // Add cell flips
    ASSERT_NE(nullptr, C.addFlip(c1, f1, f2, e4));
    ASSERT_NE(nullptr, C.addFlip(c1, f1, f3, e5));
    ASSERT_NE(nullptr, C.addFlip(c1, f1, f4, e1));
    ASSERT_NE(nullptr, C.addFlip(c1, f2, f3, e6));
    ASSERT_NE(nullptr, C.addFlip(c1, f2, f4, e2));
    ASSERT_NE(nullptr, C.addFlip(c1, f3, f4, e3));

    ASSERT_NE(nullptr, C.addFlip(c2, f3, f5, e5));
    ASSERT_NE(nullptr, C.addFlip(c2, f3, f6, e6));
    ASSERT_NE(nullptr, C.addFlip(c2, f7, f3, e3));
    ASSERT_NE(nullptr, C.addFlip(c2, f6, f5, e9));
    ASSERT_NE(nullptr, C.addFlip(c2, f7, f5, e7));
    ASSERT_NE(nullptr, C.addFlip(c2, f7, f6, e8));

    // Add top-flip
    ASSERT_NE(nullptr, C.addFlip(C.T, c1, c2, f3));

    ASSERT_TRUE(computeOrientations(C));
  }

  vertex_t v1, v2, v3, v4, v5, v_out;
  edge_t e1, e2, e3, e4, e5, e6, e7, e8, e9, e_out;
  face_t f1, f2, f3, f4, f5, f6, f7, f_out;
  cell_t c1, c2, c_out;
  CellComplex3d C;
  const CellComplex3d& CC;
};

struct DividedTetrahedron
{
  typedef CellComplex2d::bottom_t bottom_t;
  typedef CellComplex2d::vertex_t vertex_t;
  typedef CellComplex2d::edge_t edge_t;
  typedef CellComplex2d::cell_t cell_t;
  typedef CellComplex2d::top_t top_t;

  DividedTetrahedron()
    : CC(C)
  { }

  void create()
  {
    v1->i = 1;
    v2->i = 2;
    v3->i = 3;
    v4->i = 4;
    v5->i = 5;

    C.addVertex(v1);
    C.addVertex(v2);
    C.addVertex(v3);
    C.addVertex(v4);
    C.addVertex(v5);

    e1->i = 1;
    e2->i = 2;
    e4->i = 4;
    e5->i = 5;
    e6->i = 6;
    e7->i = 7;
    e8->i = 8;
    e9->i = 9;

    ASSERT_TRUE(::addCell(C, {-v1, +v2}, e1));
    ASSERT_TRUE(::addCell(C, {-v2, +v3}, e2));
    ASSERT_TRUE(::addCell(C, {-v2, +v4}, e4));
    ASSERT_TRUE(::addCell(C, {-v4, +v1}, e5));
    ASSERT_TRUE(::addCell(C, {-v3, +v4}, e6));
    ASSERT_TRUE(::addCell(C, {-v3, +v5}, e7));
    ASSERT_TRUE(::addCell(C, {-v5, +v1}, e8));
    ASSERT_TRUE(::addCell(C, {-v5, +v4}, e9));

    f1->i = 1;
    f2->i = 2;
    f4->i = 4;
    f5->i = 5;
    f6->i = 6;

    ASSERT_TRUE(::addCell(C, {+e1, +e4, +e5}, f1));
    ASSERT_TRUE(::addCell(C, {+e2, +e6, -e4}, f2));
    ASSERT_TRUE(::addCell(C, {-e8, -e7, -e2, -e1}, f4));
    ASSERT_TRUE(::addCell(C, {+e7, +e9, -e6}, f5));
    ASSERT_TRUE(::addCell(C, {-e5, -e9, +e8}, f6));
  }

  vertex_t v1, v2, v3, v4, v5, v_out;
  edge_t e1, e2, e4, e5, e6, e7, e8, e9, e_out;
  cell_t f1, f2, f4, f5, f6, f_out;
  CellComplex2d C;
  const CellComplex2d& CC;
};


#endif // TESTS_FIGURES_H
