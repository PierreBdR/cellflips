#include "test_regular.h"
#include <cellflips/celltuples.h>

#include <functional>

#include <QTextStream>
#include <stdio.h>
QTextStream out(stdout);

static const size_t sizes[] = { 10, 20, 25 };
static const size_t small_sizes[] = { 10, 12, 15 };

struct TestLine : public ::testing::TestWithParam<size_t>, public LineGrid
{
  virtual void SetUp()
  {
    create(GetParam());
  }

  void testConstruction()
  {
    EXPECT_EQ(n+1, vs.size());
    EXPECT_EQ(n, cs.size());

    ASSERT_NE(vertex_t::null, vs[n]);
    for(size_t i = 0 ; i < n ; ++i)
    {
      ASSERT_NE(vertex_t::null, vs[i]);
      ASSERT_NE(cell_t::null, cs[i]);
      EXPECT_EQ(vs[i+1], C.flip(cs[i], vs[i], C._));
      EXPECT_EQ(cellflips::pos, C.relativeOrientation(cs[i], vs[i+1]));
      EXPECT_EQ(cellflips::neg, C.relativeOrientation(cs[i], vs[i]));
    }

    for(size_t i = 0 ; i < n-1 ; ++i)
      EXPECT_EQ(cs[i+1], C.flip(C.T, cs[i], vs[i+1]));
  }

};

TEST_P(TestLine, construction)
{
  testConstruction();
}

INSTANTIATE_TEST_CASE_P(TestLineParams, TestLine, ::testing::ValuesIn(sizes));

struct TestSquareGrid : public ::testing::TestWithParam<size_t>, public SquareGrid
{
  virtual void SetUp()
  {
    size_t n = GetParam();
    create(n, n+10);
  }

  void testConstruction()
  {
    ASSERT_EQ((n1+1)*(n2+1), C.nbVertices());
    ASSERT_EQ((n1*(n2+1) + (n1+1)*n2), C.nbEdges());
    ASSERT_EQ(n1*n2, C.nbCells());

    for(const auto& v: vs)
      ASSERT_NE(vertex_t::null, v);
    for(const auto& e: es)
      ASSERT_NE(edge_t::null, e);
    for(const auto& c: cs)
    {
      ASSERT_NE(cell_t::null, c);
      ASSERT_EQ(4, C.nbFaces(c));
      ASSERT_EQ(4, C.nbVertices(c));
      size_t nn = 4;
      if(c->i == 0 or c->i == n1-1)
        nn--;
      if(c->j == 0 or c->j == n2-1)
        nn--;
      ASSERT_EQ(nn, C.nbNeighbors(c));
    }
  }

};

TEST_P(TestSquareGrid, construction)
{
  testConstruction();
}

TEST_P(TestSquareGrid, destruction)
{
  auto nv = C.nbVertices();
  auto ne = C.nbEdges();
  auto nc = C.nbCells();

  auto b44 = C.boundary(+cell(4,4));
  auto b66 = C.boundary(+cell(6,6));

  // Remove a cell inside
  {
    auto c = cell(4,4);
    ASSERT_TRUE(removeCell(C, c));
    ASSERT_FALSE(removeCell(C, c));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nc-1, C.nbCells());
  }

  // Remove another, disconnected, cell
  {
    auto c = cell(6,6);
    ASSERT_TRUE(removeCell(C, c));
    ASSERT_FALSE(removeCell(C, c));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nc-2, C.nbCells());
  }

  // Try badly to add the cell back
  {
    auto c = cell(4,4);
    ASSERT_TRUE(::addCellFailure(C, { +edge2(4,4), +edge2(5,4), +edge1(4,4), +edge1(4, 5) }, c, CellComplex2d::WRONG_BOUNDARY_ORIENTATION));
  }

  // Check orientation of the cell
  {
    auto cs = ~b44;
    ASSERT_EQ(b44, C.orientCell(cs));
    cs = ~b66;
    ASSERT_EQ(b66, C.orientCell(cs));
  }

  // Add the cell back
  {
    auto c = cell(4,4);
    ASSERT_TRUE(::addCell(C, b44, c));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nc-1, C.nbCells());
  }

  {
    auto c = cell(6,6);
    ASSERT_TRUE(::addCell(C, b66, c));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nc, C.nbCells());
  }

  // Try (and fail!) to remove used faces
  {
    auto e = edge2(4,4);
    ASSERT_FALSE(removeCell(C, e));
    auto v = vertex(7,7);
    ASSERT_FALSE(removeCell(C, v));
  }


  // Remove a cell in the corner
  {
    auto c = cell(0,0);
    ASSERT_TRUE(removeCell(C, c));
    ASSERT_EQ(nv-1, C.nbVertices());
    ASSERT_EQ(ne-2, C.nbEdges());
    ASSERT_EQ(nc-1, C.nbCells());
  }

}

TEST_P(TestSquareGrid, splitting)
{
  auto nv = C.nbVertices();
  auto ne = C.nbEdges();
  auto nc = C.nbCells();

  {
    auto e77 = edge2(7,7);
    auto res77 = splitEdge(C, e77);
    ASSERT_TRUE(bool(res77));
    res77.membrane->j = 7;
    res77.membrane->i = 7.5;
    res77.left->i = 7;
    res77.left->j = 7;
    res77.right->i = 7;
    res77.right->j = 7.5;
    ASSERT_EQ(nv+1, C.nbVertices());
    ASSERT_EQ(ne+1, C.nbEdges());
    ASSERT_EQ(nc, C.nbCells());

    ASSERT_FALSE(bool(splitEdge(C, e77)));

    auto e87 = edge2(8,7);
    auto res87 = splitEdge(C, e87);
    ASSERT_TRUE(bool(res87));
    res87.membrane->j = 8;
    res87.membrane->i = 7.5;
    res87.left->i = 8;
    res87.left->j = 7;
    res87.right->i = 8;
    res87.right->j = 7.5;
    ASSERT_EQ(nv+2, C.nbVertices());
    ASSERT_EQ(ne+2, C.nbEdges());
    ASSERT_EQ(nc, C.nbCells());

    ASSERT_FALSE(bool(splitEdge(C, e87)));

    // Now split the cell
    auto c = cell(7,7);
    auto rc = splitCell(C, c, {+res87.membrane, -res77.membrane});
    ASSERT_TRUE(bool(rc));

    rc.membrane->i = 7.5;
    rc.membrane->j = 7.5;

    ASSERT_EQ(nv+2, C.nbVertices());
    ASSERT_EQ(ne+3, C.nbEdges());
    ASSERT_EQ(nc+1, C.nbCells());

    ASSERT_EQ(set({rc.membrane}), C.bounds(rc.left) & C.bounds(rc.right));

    ASSERT_FALSE(bool(splitCell(C, c, {+res87.membrane, -res77.membrane})));
  }

}

TEST_P(TestSquareGrid, replace)
{
  auto nv = C.nbVertices();
  auto ne = C.nbEdges();
  auto nc = C.nbCells();

  // Replace an edge with a subdivided one
  {
    auto e1 = edge1(3,3);

    auto v1 = vertex_t{Grid2DContent{3.5,3,-1}};
    ASSERT_TRUE(bool(C.addVertex(v1)));
    auto e1l = edge_t{Grid2DContent{3.,3,-1}};
    ASSERT_TRUE(::addCell(C, {+v1, -vertex(3,3)}, e1l));
    auto e1r = edge_t{Grid2DContent{3.5,3,-1}};
    ASSERT_TRUE(::addCell(C, {+vertex(4,3), -v1}, e1r));

    ASSERT_TRUE(::replace(C, chain({+e1}), chain({+e1l, +e1r})));

    EXPECT_EQ(nv+1, C.nbVertices());
    EXPECT_EQ(ne+2, C.nbEdges());

    ASSERT_TRUE(removeCell(C, e1));

    EXPECT_EQ(nv+1, C.nbVertices());
    EXPECT_EQ(ne+1, C.nbEdges());

    auto e2 = edge1(3,4);
    auto v2 = vertex_t{Grid2DContent{3.5,3,-1}};
    ASSERT_TRUE(bool(C.addVertex(v2)));
    auto e2l = edge_t{Grid2DContent{3.,3,-1}};
    ASSERT_TRUE(::addCell(C, {+v2, -vertex(3,4)}, e2l));
    auto e2r = edge_t{Grid2DContent{3.5,4,-1}};
    ASSERT_TRUE(::addCell(C, {+vertex(4,4), -v2}, e2r));

    ASSERT_TRUE(::replace(C, chain({+e2}), chain({+e2l, +e2r})));

    EXPECT_EQ(nv+2, C.nbVertices());
    EXPECT_EQ(ne+3, C.nbEdges());

    ASSERT_TRUE(removeCell(C, e2));

    EXPECT_EQ(nv+2, C.nbVertices());
    EXPECT_EQ(ne+2, C.nbEdges());

    auto em = edge_t{Grid2DContent{3.5,3,-2}};
    ASSERT_TRUE(::addCell(C, {+v2, -v1}, em));

    EXPECT_EQ(ne+3, C.nbEdges());

  // Replace a cell with a subdivided one
    auto c = cell(3,3);

    auto cl = cell_t{Grid2DContent{3.,3}};
    ASSERT_TRUE(::addCell(C, {+em, +e1l, -e2l, -edge2(3,3)}, cl, false));
    auto cr = cell_t{Grid2DContent{3.5,3}};
    ASSERT_TRUE(::addCell(C, {-em, +e1r, -e2r, +edge2(4,3)}, cr, false));

    // Check the opposite fails
    ASSERT_FALSE(::replace(C, chain({+cl, +cr}), chain({+c})));

    // Check incorrect boundaries
    ASSERT_FALSE(::replace(C, chain({+c}), chain({+cl, -cr})));

    ASSERT_TRUE(::replace(C, chain({+c}), chain({+cl, +cr})));

    EXPECT_EQ(nv+2, C.nbVertices());
    EXPECT_EQ(ne+3, C.nbEdges());
    EXPECT_EQ(nc+2, C.nbCells());

    ASSERT_TRUE(removeCell(C, c));

    EXPECT_EQ(nv+2, C.nbVertices());
    EXPECT_EQ(ne+3, C.nbEdges());
    EXPECT_EQ(nc+1, C.nbCells());
  }
}

INSTANTIATE_TEST_CASE_P(TestSquareGridParams, TestSquareGrid, ::testing::ValuesIn(sizes));

struct TestCubeGrid : public ::testing::TestWithParam<size_t>, public CubeGrid
{
  virtual void SetUp()
  {
    size_t n = GetParam();
    create(n, n+2, n-3);
  }

  void testConstruction()
  {
    ASSERT_EQ(vs.size(), C.nbVertices());
    ASSERT_EQ(es.size(), C.nbEdges());
    ASSERT_EQ(fs.size(), C.nbFaces());
    ASSERT_EQ(cs.size(), C.nbCells());

    for(const auto& v: vs)
    {
      ASSERT_TRUE(C.hasCell(v));
      ASSERT_NE(vertex_t::null, v);
    }

    /* // That is to debug the test ...
    for(size_t i = 0 ; i < n1+1 ; ++i)
      for(size_t j = 0 ; j < n2+1 ; ++j)
        for(size_t k = 0 ; k < n3+1 ; ++k)
        {
          auto v = vertex(i,j,k);
          ASSERT_EQ(std::make_tuple(i,j,k), std::make_tuple(v->i, v->j, v->k));
        }

    for(size_t i = 0 ; i < n1 ; ++i)
    {
      for(size_t j = 0 ; j < n2 ; ++j)
      {
        for(size_t k = 0 ; k < n3 ; ++k)
        {
          auto e1 = edge1(i,j,k);
          ASSERT_EQ(std::make_tuple(i,j,k), std::make_tuple(e1->i, e1->j, e1->k));
          auto e2 = edge2(i,j,k);
          ASSERT_EQ(std::make_tuple(i,j,k), std::make_tuple(e2->i, e2->j, e2->k));
          auto e3 = edge3(i,j,k);
          ASSERT_EQ(std::make_tuple(i,j,k), std::make_tuple(e3->i, e3->j, e3->k));
          auto f1 = face1(i,j,k);
          ASSERT_EQ(std::make_tuple(i,j,k), std::make_tuple(f1->i, f1->j, f1->k));
          auto f2 = face2(i,j,k);
          ASSERT_EQ(std::make_tuple(i,j,k), std::make_tuple(f2->i, f2->j, f2->k));
          auto f3 = face3(i,j,k);
          ASSERT_EQ(std::make_tuple(i,j,k), std::make_tuple(f3->i, f3->j, f3->k));
          auto c = cell(i,j,k);
          ASSERT_EQ(std::make_tuple(i,j,k), std::make_tuple(c->i, c->j, c->k));

          if(i == 0)
          { // for all i,k
            auto e2 = edge2(n1,j,k);
            ASSERT_EQ(std::make_tuple(n1,j,k), std::make_tuple(e2->i, e2->j, e2->k));
            auto e3 = edge3(n1,j,k);
            ASSERT_EQ(std::make_tuple(n1,j,k), std::make_tuple(e3->i, e3->j, e3->k));
            auto f1 = face1(n1,j,k);
            ASSERT_EQ(std::make_tuple(n1,j,k), std::make_tuple(f1->i, f1->j, f1->k));
            if(j == 0)
            { // for all k
              auto e3 = edge3(n1,n2,k);
              ASSERT_EQ(std::make_tuple(n1,n2,k), std::make_tuple(e3->i, e3->j, e3->k));
            }
          }
          if(j == 0)
          { // for all j,k
            auto e1 = edge1(i,n2,k);
            ASSERT_EQ(std::make_tuple(i,n2,k), std::make_tuple(e1->i, e1->j, e1->k));
            auto e3 = edge3(i,n2,k);
            ASSERT_EQ(std::make_tuple(i,n2,k), std::make_tuple(e3->i, e3->j, e3->k));
            auto f2 = face2(i,n2,k);
            ASSERT_EQ(std::make_tuple(i,n2,k), std::make_tuple(f2->i, f2->j, f2->k));
          }
        }
        { // for all i, j
          auto e1 = edge1(i,j,n3);
          ASSERT_EQ(std::make_tuple(i,j,n3), std::make_tuple(e1->i, e1->j, e1->k));
          auto e2 = edge2(i,j,n3);
          ASSERT_EQ(std::make_tuple(i,j,n3), std::make_tuple(e2->i, e2->j, e2->k));
          auto f3 = face3(i,j,n3);
          ASSERT_EQ(std::make_tuple(i,j,n3), std::make_tuple(f3->i, f3->j, f3->k));
        }
        if(i==0)
        { // for all j
          auto e2 = edge2(n1,j,n3);
          ASSERT_EQ(std::make_tuple(n1,j,n3), std::make_tuple(e2->i, e2->j, e2->k));
        }
      }
      { // for all i
        auto e1 = edge1(i,n2,n3);
        ASSERT_EQ(std::make_tuple(i,n2,n3), std::make_tuple(e1->i, e1->j, e1->k));
      }
    }
    */

    for(const auto& e: es)
    {
      ASSERT_TRUE(C.hasCell(e));
      ASSERT_NE(edge_t::null, e);
      size_t nbCofaces = 4;
      if(e->type != 1 and (e->i == 0 or e->i == n1))
        nbCofaces--;
      if(e->type != 2 and (e->j == 0 or e->j == n2))
        nbCofaces--;
      if(e->type != 3 and (e->k == 0 or e->k == n3))
        nbCofaces--;
      EXPECT_EQ(nbCofaces, C.nbCofaces(e));
    }

    for(const auto& f: fs)
    {
      ASSERT_TRUE(C.hasCell(f));
      ASSERT_NE(face_t::null, f);
      ASSERT_EQ(4, C.nbFaces(f));
      ASSERT_EQ(4, C.nbVertices(f));
      size_t nbCofaces = 2;
      if(f->type == 1 and (f->i == 0 or f->i == n1)) nbCofaces = 1;
      if(f->type == 2 and (f->j == 0 or f->j == n2)) nbCofaces = 1;
      if(f->type == 3 and (f->k == 0 or f->k == n3)) nbCofaces = 1;
      EXPECT_EQ(nbCofaces, C.nbCofaces(f));
    }

    for(const auto& c: cs)
    {
      ASSERT_TRUE(C.hasCell(c));
      ASSERT_NE(cell_t::null, c);
      ASSERT_EQ(6, C.nbFaces(c));
      ASSERT_EQ(12, C.nbEdges(c));
      ASSERT_EQ(8, C.nbVertices(c));

      // Check face neighbors
      size_t n = 6;
      if(c->i == 0 or c->i == n1-1) n--;
      if(c->j == 0 or c->j == n2-1) n--;
      if(c->k == 0 or c->k == n3-1) n--;
      EXPECT_EQ(n, C.nbNeighbors(c));
    }
  }

};

TEST_P(TestCubeGrid, construction)
{
  testConstruction();
}

TEST_P(TestCubeGrid, destruction)
{
  auto nv = C.nbVertices();
  auto ne = C.nbEdges();
  auto nf = C.nbFaces();
  auto nc = C.nbCells();

  auto c3 = cell(3,3,3);
  auto b3 = C.boundary(+c3);
  auto c5 = cell(5,5,5);
  auto b5 = C.boundary(+c5);

  // Fail to remove inside cells of lower dimensions
  ASSERT_FALSE(removeCell(C, face1(0,0,0)));
  ASSERT_FALSE(removeCell(C, face2(2,3,3)));
  ASSERT_FALSE(removeCell(C, edge1(2,3,3)));
  ASSERT_FALSE(removeCell(C, edge3(5,3,3)));
  ASSERT_FALSE(removeCell(C, vertex(5,3,3)));
  ASSERT_FALSE(removeCell(C, vertex(0,0,0)));

  // Remove cell inside
  {
    ASSERT_TRUE(removeCell(C, c3));
    ASSERT_FALSE(removeCell(C, c3));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nf, C.nbFaces());
    ASSERT_EQ(nc-1, C.nbCells());
  }

  { // Remove another, disconnected, cell
    ASSERT_TRUE(removeCell(C, c5));
    ASSERT_FALSE(removeCell(C, c5));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nf, C.nbFaces());
    ASSERT_EQ(nc-2, C.nbCells());
  }

  { // Check cell orientation
    auto cs = ~b3;
    ASSERT_EQ(b3, C.orientCell(cs));
    cs = ~b5;
    ASSERT_EQ(b5, C.orientCell(cs));
  }

  { // Add the cell back
    ASSERT_TRUE(::addCell(C, b3, c3));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nf, C.nbFaces());
    ASSERT_EQ(nc-1, C.nbCells());
  }

  { // Add the cell back
    ASSERT_TRUE(::addCell(C, b5, c5));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nf, C.nbFaces());
    ASSERT_EQ(nc, C.nbCells());
  }

  { // Remove a corner
    auto c = cell(0,0,0);
    auto b = C.boundary(+c);
    auto f1 = face1(0,0,0);
    auto bf1 = C.boundary(+f1);
    auto f2 = face2(0,0,0);
    auto bf2 = C.boundary(+f2);
    auto f3 = face3(0,0,0);
    auto bf3 = C.boundary(+f3);
    auto e1 = edge1(0,0,0);
    auto be1 = C.boundary(+e1);
    auto e2 = edge2(0,0,0);
    auto be2 = C.boundary(+e2);
    auto e3 = edge3(0,0,0);
    auto be3 = C.boundary(+e3);
    auto v = vertex(0,0,0);
    ASSERT_TRUE(removeCell(C, c));
    ASSERT_EQ(nv-1, C.nbVertices());
    ASSERT_EQ(ne-3, C.nbEdges());
    ASSERT_EQ(nf-3, C.nbFaces());
    ASSERT_EQ(nc-1, C.nbCells());
    // Put the corner back
    ASSERT_TRUE(bool(C.addVertex(v)));
    ASSERT_TRUE(::addCell(C, be1, e1));
    ASSERT_TRUE(::addCell(C, be2, e2));
    ASSERT_TRUE(::addCell(C, be3, e3));
    ASSERT_TRUE(::addCell(C, bf1, f1));
    ASSERT_TRUE(::addCell(C, bf2, f2));
    ASSERT_TRUE(::addCell(C, bf3, f3));
    ASSERT_TRUE(::addCell(C, b, c));
  }

  { // Remove an edge
    auto c = cell(0,3,0);
    auto b = C.boundary(+c);
    auto f1 = face1(0,3,0);
    auto bf1 = C.boundary(+f1);
    auto f3 = face3(0,3,0);
    auto bf3 = C.boundary(+f3);
    auto e2 = edge2(0,3,0);
    auto be2 = C.boundary(+e2);
    ASSERT_TRUE(removeCell(C, c));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne-1, C.nbEdges());
    ASSERT_EQ(nf-2, C.nbFaces());
    ASSERT_EQ(nc-1, C.nbCells());
    // Put the cell back
    ASSERT_TRUE(::addCell(C, be2, e2));
    ASSERT_TRUE(::addCell(C, bf1, f1));
    ASSERT_TRUE(::addCell(C, bf3, f3));
    ASSERT_TRUE(::addCell(C, b, c));
  }

  { // Remove a side
    auto c = cell(0, 3, 3);
    auto b = C.boundary(+c);
    auto f1 = face1(0, 3, 3);
    auto bf1 = C.boundary(+f1);
    ASSERT_TRUE(removeCell(C, c));
    ASSERT_EQ(nv, C.nbVertices());
    ASSERT_EQ(ne, C.nbEdges());
    ASSERT_EQ(nf-1, C.nbFaces());
    ASSERT_EQ(nc-1, C.nbCells());
    // Put the cell back
    ASSERT_TRUE(::addCell(C, bf1, f1));
    ASSERT_TRUE(::addCell(C, b, c));
  }
}

TEST_P(TestCubeGrid, splitting)
{
  auto nv = C.nbVertices();
  auto ne = C.nbEdges();
  auto nf = C.nbFaces();
  auto nc = C.nbCells();

  // First a few wrong checks
  {
    ASSERT_FALSE(bool(splitCell(C, cell(3,3,3),
                                           { +edge1(3,3,3), +edge1(3,3,4), -edge1(3,2,3) })));

    ASSERT_FALSE(bool(splitCell(C, cell_t::null, { +edge2(3,3,3), +edge2(3,3,3) })));
  }

  {
    auto e1_bf = edge1(3,3,3); // bottom front
    auto e1_tf = edge1(3,3,4); // top front
    auto e1_bb = edge1(3,4,3); // bottom back
    auto e1_tb = edge1(3,4,4); // top back

    auto res_bf = splitEdge(C, e1_bf);
    ASSERT_TRUE(bool(res_bf));

    *res_bf.membrane = Grid3DContent{3.5, 3, 3};
    *res_bf.left = Grid3DContent{3., 3, 3, 1};
    *res_bf.right = Grid3DContent{3.5, 3, 3, 1};

    EXPECT_TRUE(C.isBound(res_bf.left, vertex(3,3,3)));
    EXPECT_TRUE(C.isBound(res_bf.right, vertex(4,3,3)));

    ASSERT_FALSE(bool(splitEdge(C, e1_bf)));

    ASSERT_EQ(nv+1, C.nbVertices());
    ASSERT_EQ(ne+1, C.nbEdges());
    ASSERT_EQ(nf, C.nbFaces());
    ASSERT_EQ(nc, C.nbCells());

    auto res_tf = splitEdge(C, e1_tf);
    ASSERT_TRUE(bool(res_tf));
    *res_tf.membrane = Grid3DContent{3.5, 3, 4};
    *res_tf.left = Grid3DContent{3., 3, 4, 1};
    *res_tf.right = Grid3DContent{3.5, 3, 4, 1};

    EXPECT_TRUE(C.isBound(res_tf.left, vertex(3,3,4)));
    EXPECT_TRUE(C.isBound(res_tf.right, vertex(4,3,4)));

    ASSERT_EQ(nv+2, C.nbVertices());
    ASSERT_EQ(ne+2, C.nbEdges());
    ASSERT_EQ(nf, C.nbFaces());
    ASSERT_EQ(nc, C.nbCells());

    auto res_bb = splitEdge(C, e1_bb);
    ASSERT_TRUE(bool(res_bb));
    *res_bb.membrane = Grid3DContent{3.5, 4, 3};
    *res_bb.left = Grid3DContent{3., 4, 3, 1};
    *res_bb.right = Grid3DContent{3.5, 4, 3, 1};

    EXPECT_TRUE(C.isBound(res_bb.left, vertex(3,4,3)));
    EXPECT_TRUE(C.isBound(res_bb.right, vertex(4,4,3)));

    ASSERT_EQ(nv+3, C.nbVertices());
    ASSERT_EQ(ne+3, C.nbEdges());
    ASSERT_EQ(nf, C.nbFaces());
    ASSERT_EQ(nc, C.nbCells());

    auto res_tb = splitEdge(C, e1_tb);
    ASSERT_TRUE(bool(res_tb));
    *res_tb.membrane = Grid3DContent{3.5, 4, 4};
    *res_tb.left = Grid3DContent{3., 4, 4, 1};
    *res_tb.right = Grid3DContent{3.5, 4, 4, 1};

    EXPECT_TRUE(C.isBound(res_tb.left, vertex(3,4,4)));
    EXPECT_TRUE(C.isBound(res_tb.right, vertex(4,4,4)));

    ASSERT_EQ(nv+4, C.nbVertices());
    ASSERT_EQ(ne+4, C.nbEdges());
    ASSERT_EQ(nf, C.nbFaces());
    ASSERT_EQ(nc, C.nbCells());

    auto f3b = face3(3,3,3); // bottom
    auto res3b = splitCell(C, f3b, {+res_bb.membrane, -res_bf.membrane});
    ASSERT_TRUE(bool(res3b));
    *res3b.membrane = Grid3DContent{3.5, 3, 3, 3};
    *res3b.left = Grid3DContent{3., 3, 3, 3};
    *res3b.right = Grid3DContent{3.5, 3, 3, 3};

    EXPECT_TRUE(C.isBound(res3b.left, edge2(3,3,3)));
    EXPECT_TRUE(C.isBound(res3b.right, edge2(4,3,3)));

    auto f3t = face3(3,3,4); // top
    auto res3t = splitCell(C, f3t, {+res_tb.membrane, -res_tf.membrane});
    ASSERT_TRUE(bool(res3t));
    *res3t.membrane = Grid3DContent{3.5, 3, 4, 3};
    *res3t.left = Grid3DContent{3., 3, 4, 3};
    *res3t.right = Grid3DContent{3.5, 3, 4, 3};

    EXPECT_TRUE(C.isBound(res3t.left, edge2(3,3,4)));
    EXPECT_TRUE(C.isBound(res3t.right, edge2(4,3,4)));

    auto f2f = face2(3,3,3); // front
    auto res2f = splitCell(C, f2f, {+res_tf.membrane, -res_bf.membrane});
    ASSERT_TRUE(bool(res2f));
    *res2f.membrane = Grid3DContent{3.5, 3, 3, 2};
    *res2f.left = Grid3DContent{3., 3, 3, 2};
    *res2f.right = Grid3DContent{3.5, 3, 3, 2};

    EXPECT_TRUE(C.isBound(res2f.left, edge3(3,3,3)));
    EXPECT_TRUE(C.isBound(res2f.right, edge3(4,3,3)));

    auto f2b = face2(3,4,3); // back
    auto res2b = splitCell(C, f2b, {+res_tb.membrane, -res_bb.membrane});
    ASSERT_TRUE(bool(res2b));
    *res2b.membrane = Grid3DContent{3.5, 4, 3, 2};
    *res2b.left = Grid3DContent{3., 4, 3, 2};
    *res2b.right = Grid3DContent{3.5, 4, 3, 2};

    EXPECT_TRUE(C.isBound(res2b.left, edge3(3,4,3)));
    EXPECT_TRUE(C.isBound(res2b.right, edge3(4,4,3)));

    ASSERT_EQ(nv+4, C.nbVertices());
    ASSERT_EQ(ne+8, C.nbEdges());
    ASSERT_EQ(nf+4, C.nbFaces());
    ASSERT_EQ(nc, C.nbCells());

    auto c = cell(3,3,3);
    ASSERT_FALSE(bool(splitCell(C, cell(4,4,4), {+res3b.membrane, +res2b.membrane, -res3t.membrane, -res2f.membrane})));
    auto res = splitCell(C, c, {+res3b.membrane, +res2b.membrane, -res3t.membrane, -res2f.membrane});
    ASSERT_TRUE(bool(res));
    *res.membrane = Grid3DContent{3.5, 3, 3, 1};
    *res.left = Grid3DContent{3.,3,3};
    *res.right = Grid3DContent{3.5,3,3};

    EXPECT_TRUE(C.isBound(res.left, face1(3,3,3)));
    EXPECT_TRUE(C.isBound(res.right, face1(4,3,3)));
  }
}

TEST_P(TestCubeGrid, replace)
{
  auto nv = C.nbVertices();
  auto ne = C.nbEdges();
  auto nf = C.nbFaces();
  auto nc = C.nbCells();

  auto c = cell(3,3,3); // cell that will get replaced

  auto v1 = C.addVertex();
  ASSERT_NE(vertex_t::null, v1);
  *v1 = Grid3DContent{3.5,3,3};

  auto v2 = C.addVertex();
  ASSERT_NE(vertex_t::null, v2);
  *v2 = Grid3DContent{3.5,4,2};

  auto v3 = C.addVertex();
  ASSERT_NE(vertex_t::null, v3);
  *v3 = Grid3DContent{3.5,4,4};

  auto v4 = C.addVertex();
  ASSERT_NE(vertex_t::null, v4);
  *v4 = Grid3DContent{3.5,3,4};

  auto e1 = edge1(3,3,3);
  auto e2 = edge1(3,4,3);
  auto e3 = edge1(3,4,4);
  auto e4 = edge1(3,3,4);

  auto e1l = edge_t{Grid3DContent{3.,3,3,1}};
  ASSERT_TRUE(::addCell(C, chain({+v1, -vertex(3,3,3)}), e1l));
  auto e1r = edge_t{Grid3DContent{3.5,3,3,1}};
  ASSERT_TRUE(::addCell(C, chain({-v1, +vertex(4,3,3)}), e1r));

  auto e2l = edge_t{Grid3DContent{3.,4,3,1}};
  ASSERT_TRUE(::addCell(C, chain({+v2, -vertex(3,4,3)}), e2l));
  auto e2r = edge_t{Grid3DContent{3.5,4,3,1}};
  ASSERT_TRUE(::addCell(C, chain({-v2, +vertex(4,4,3)}), e2r));

  auto e3l = edge_t{Grid3DContent{3.,4,4,1}};
  ASSERT_TRUE(::addCell(C, chain({+v3, -vertex(3,4,4)}), e3l));
  auto e3r = edge_t{Grid3DContent{3.5,4,4,1}};
  ASSERT_TRUE(::addCell(C, chain({-v3, +vertex(4,4,4)}), e3r));

  auto e4l = edge_t{Grid3DContent{3.,3,4,1}};
  ASSERT_TRUE(::addCell(C, chain({+v4, -vertex(3,3,4)}), e4l));
  auto e4r = edge_t{Grid3DContent{3.5,3,4,1}};
  ASSERT_TRUE(::addCell(C, chain({-v4, +vertex(4,3,4)}), e4r));

  ASSERT_TRUE(::replace(C, chain({+e1}), chain({+e1l, +e1r})));
  ASSERT_TRUE(::replace(C, chain({+e2}), chain({+e2l, +e2r})));
  ASSERT_TRUE(::replace(C, chain({+e3}), chain({+e3l, +e3r})));
  ASSERT_TRUE(::replace(C, chain({+e4}), chain({+e4l, +e4r})));

  ASSERT_TRUE(removeCell(C, e1));
  ASSERT_TRUE(removeCell(C, e2));
  ASSERT_TRUE(removeCell(C, e3));
  ASSERT_TRUE(removeCell(C, e4));

  ASSERT_EQ(nv+4, C.nbVertices());
  ASSERT_EQ(ne+4, C.nbEdges());
  ASSERT_EQ(nf, C.nbFaces());
  ASSERT_EQ(nc, C.nbCells());

  auto e12 = edge_t{Grid3DContent{3.5, 3, 3, 2}};
  ASSERT_TRUE(::addCell(C, chain({+v2, -v1}), e12));
  auto e23 = edge_t{Grid3DContent{3.5, 4, 3, 3}};
  ASSERT_TRUE(::addCell(C, chain({+v3, -v2}), e23));
  auto e43 = edge_t{Grid3DContent{3.5, 3, 4, 2}};
  ASSERT_TRUE(::addCell(C, chain({+v3, -v4}), e43));
  auto e14 = edge_t{Grid3DContent{3.5, 3, 4, 3}};
  ASSERT_TRUE(::addCell(C, chain({+v4, -v1}), e14));

  auto f14 = face2(3,3,3);
  auto f12 = face3(3,3,3);
  auto f23 = face2(3,4,3);
  auto f34 = face3(3,3,4);

  auto f14l = face_t{Grid3DContent{3.,3,3,2}};
  ASSERT_TRUE(::addCell(C, chain({+e1l, +e14, -e4l, -edge3(3,3,3)}), f14l));
  auto f14r = face_t{Grid3DContent{3.5,3,3,2}};
  ASSERT_TRUE(::addCell(C, chain({+e1r, +edge3(4,3,3), -e4r, -e14}), f14r));

  auto f12l = face_t{Grid3DContent{3.,3,3,3}};
  ASSERT_TRUE(::addCell(C, chain({+e1l, +e12, -e2l, -edge2(3,3,3)}), f12l));
  auto f12r = face_t{Grid3DContent{3.5,3,3,3}};
  ASSERT_TRUE(::addCell(C, chain({+e1r, -e12, -e2r, +edge2(4,3,3)}), f12r));

  auto f23l = face_t{Grid3DContent{3.,4,3,2}};
  ASSERT_TRUE(::addCell(C, chain({+e2l, +e23, -e3l, -edge3(3,4,3)}), f23l));
  auto f23r = face_t{Grid3DContent{3.5,4,3,2}};
  ASSERT_TRUE(::addCell(C, chain({+e2r, -e23, -e3r, +edge3(4,4,3)}), f23r));

  auto f34l = face_t{Grid3DContent{3.,3,4,3}};
  ASSERT_TRUE(::addCell(C, chain({+e4l, +e43, -e3l, -edge2(3,3,4)}), f34l));
  auto f34r = face_t{Grid3DContent{3.5,3,4,3}};
  ASSERT_TRUE(::addCell(C, chain({+e4r, -e43, -e3r, +edge2(4,3,4)}), f34r));

  ASSERT_TRUE(::replace(C, chain({+f14}), chain({+f14l, +f14r})));
  ASSERT_TRUE(::replace(C, chain({+f12}), chain({+f12l, +f12r})));
  ASSERT_TRUE(::replace(C, chain({+f34}), chain({+f34l, +f34r})));
  ASSERT_TRUE(::replace(C, chain({+f23}), chain({+f23l, +f23r})));

  ASSERT_TRUE(removeCell(C, f14));
  ASSERT_TRUE(removeCell(C, f12));
  ASSERT_TRUE(removeCell(C, f34));
  ASSERT_TRUE(removeCell(C, f23));

  ASSERT_EQ(nv+4, C.nbVertices());
  ASSERT_EQ(ne+8, C.nbEdges());
  ASSERT_EQ(nf+4, C.nbFaces());
  ASSERT_EQ(nc, C.nbCells());

  auto fm = face_t{Grid3DContent{3.5,3,3,1}};
  ASSERT_TRUE(::addCell(C, chain({+e12, +e23, -e43, -e14}), fm));

  auto cl = cell_t{Grid3DContent{3., 3, 3}};
  ASSERT_TRUE(::addCell(C, chain({+f14l, +fm, -f23l, +f34l, -f12l, -face1(3,3,3)}), cl, false));
  auto cr = cell_t{Grid3DContent{3.5, 3, 3}};
  ASSERT_TRUE(::addCell(C, chain({+f14r, -fm, -f23r, +f34r, -f12r, +face1(4,3,3)}), cr, false));

  ASSERT_TRUE(::replace(C, chain({+c}), chain({+cl, +cr})));

  ASSERT_TRUE(removeCell(C, c));

  ASSERT_EQ(nv+4, C.nbVertices());
  ASSERT_EQ(ne+8, C.nbEdges());
  ASSERT_EQ(nf+5, C.nbFaces());
  ASSERT_EQ(nc+1, C.nbCells());
}

TEST_P(TestCubeGrid, tuple)
{

  // First, try to create a tuple and iterate over the faces of a cube
  {
    auto t = cellTuple(C, cell(3,3,3), face2(3,3,3), edge1(3,3,3), vertex(3,3,3));
    EXPECT_TRUE(t.isValid(C));
    t.clear();
    EXPECT_TRUE(t.empty());
  }

  {
    auto t = findTuple(C);
    EXPECT_FALSE(t.empty());
    EXPECT_TRUE((bool)t);
    EXPECT_TRUE(t.isValid(C));
  }

  {
    auto t = findTuple(C, cell(3,3,3));
    EXPECT_TRUE((bool)t);
    EXPECT_TRUE(t.isValid(C));

    auto t1 = t;
    size_t counter = 0;
    do
    {
      counter++;
      t.flip<1>(C);
      t.flip<2>(C);
    } while(t1 != t);
    EXPECT_EQ(3, counter);

    EXPECT_NE(t.orientation(C), t.flipped<0>(C).orientation(C));
    EXPECT_NE(t.orientation(C), t.flipped<1>(C).orientation(C));
    EXPECT_NE(t.orientation(C), t.flipped<2>(C).orientation(C));
    EXPECT_NE(t.orientation(C), t.flipped<3>(C).orientation(C));

    auto ro = (C.relativeOrientation(t.cell(), t.face()) *
               C.relativeOrientation(t.face(), t.edge()) *
               C.relativeOrientation(t.edge(), t.vertex()));
    EXPECT_EQ(ro, t.orientation(C));

    ro = (C.relativeOrientation(t.face(), t.edge()) *
          C.relativeOrientation(t.edge(), t.vertex()));
    EXPECT_EQ(ro, (t.relativeOrientation<0,2>(C)));
  }

  {
    auto t = findTuple(C, face2(3,3,3));
    EXPECT_TRUE((bool)t);
    EXPECT_TRUE(t.isValid(C));

    auto t1 = t;
    size_t counter = 0;
    do
    {
      counter++;
      t1.flip<1>(C);
      t1.flip<2>(C);
    } while(t1 != t);
    EXPECT_EQ(3, counter);
  }

  {
    auto t = findTuple(C, edge2(3,3,3));
    EXPECT_TRUE((bool)t);
    EXPECT_TRUE(t.isValid(C));

    auto t1 = t;
    size_t counter = 0;
    do
    {
      counter++;
      t1.flip<0>(C);
      t1.flip<1>(C);
    } while(t1 != t);
    EXPECT_EQ(4, counter);
  }

  {
    auto t = findTuple(C, vertex(3,3,3));
    EXPECT_TRUE((bool)t);
    EXPECT_TRUE(t.isValid(C));
  }

  {
    auto t = findTuple(C, cell(3,3,3), vertex(3,4,3));
    EXPECT_FALSE(t.empty());
    EXPECT_TRUE((bool)t);
    EXPECT_TRUE(t.isValid());
    EXPECT_TRUE(t.isValid(C));
  }

  {
    auto t = findTuple(C, cell(3,3,3), edge3(3,4,3));
    EXPECT_TRUE(t.isValid());
    EXPECT_TRUE(t.isValid(C));
  }

  {
    auto t = findTuple(C, face2(3,3,3), vertex(3,3,3));
    EXPECT_TRUE((bool)t);
    EXPECT_TRUE(t.isValid(C));
  }

  {
    auto t = findTuple(C, face2(3,3,3), edge3(3,3,3));
    EXPECT_TRUE((bool)t);
    EXPECT_TRUE(t.isValid(C));
  }

  {
    auto t = findTuple(C, cell(3,3,3), vertex(5,5,5));
    EXPECT_TRUE(t.empty());
    EXPECT_FALSE((bool)t);
  }
}

INSTANTIATE_TEST_CASE_P(TestCubeGridParams, TestCubeGrid, ::testing::ValuesIn(small_sizes));

