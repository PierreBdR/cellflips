#ifndef TEST_REGULAR_H
#define TEST_REGULAR_H

#include "tests.h"

#include <string>

struct Content
{
  double i = -1;
};

inline int num(const Content& cnt)
{
  return cnt.i;
}

struct Grid2DContent
{
  Grid2DContent() { }
  Grid2DContent(double i1, double i2, int t = 0)
    : i(i1)
    , j(i2)
    , type(t)
  { }
  Grid2DContent(size_t i1, size_t i2, int t = 0)
    : i(i1)
    , j(i2)
    , type(t)
  { }
  double i = -1, j = -1;
  int type = 0;
};

inline std::string num(const Grid2DContent& v)
{
  return std::to_string(v.i) + "," + std::to_string(v.j);
}

struct Grid3DContent
{
  Grid3DContent() { }
  Grid3DContent(double i1, double i2, double i3, int t = 0)
    : i(i1)
    , j(i2)
    , k(i3)
    , type(t)
  { }
  Grid3DContent(size_t i1, size_t i2, size_t i3, int t = 0)
    : i(i1)
    , j(i2)
    , k(i3)
    , type(t)
  { }
  double i = -1,j = -1,k = -1;
  int type = 0;
};

inline std::string num(const Grid3DContent& v)
{
  return std::to_string(v.i) + "," + std::to_string(v.j) + "," + std::to_string(v.k);
}

namespace cellflips
{

template <int N>
QString shortString(const Cell<N,Grid3DContent>& c)
{
  if(c->type != 0)
    return QString("<%1,%2,%3|%4>").arg(c->i).arg(c->j).arg(c->k).arg(c->type);
  else
    return QString("<%1,%2,%3>").arg(c->i).arg(c->j).arg(c->k);
}

}


typedef cellflips::CellComplex<Content, Content> CellComplex1d;
typedef cellflips::CellComplex<Grid2DContent, Grid2DContent, Grid2DContent> CellComplex2d;
typedef cellflips::CellComplex<Grid3DContent, Grid3DContent, Grid3DContent, Grid3DContent> CellComplex3d;

struct LineGrid
{
  typedef CellComplex1d::bottom_t bottom_t;
  typedef CellComplex1d::vertex_t vertex_t;
  typedef CellComplex1d::cell_t cell_t;
  typedef CellComplex1d::top_t top_t;

  void create(size_t s)
  {
    n = s;
    vs.resize(n+1, vertex_t::null);
    cs.resize(n, cell_t::null);

    for(size_t i = 0 ; i < vs.size() ; ++i)
    {
      auto v = C.addVertex();
      ASSERT_NE(vertex_t::null, v);
      v->i = int(i);
      vs[i] = v;
    }

    for(size_t i = 0 ; i < n ; ++i)
    {
      cell_t c;
      c->i = int(i);
      ASSERT_TRUE(::addCell(C, {-vs[i], +vs[i+1]}, c));
      cs[i] = c;
    }
  }

  size_t n = 0;
  std::vector<vertex_t> vs;
  std::vector<cell_t> cs;
  CellComplex1d C;
};

struct SquareGrid
{
  typedef CellComplex2d::bottom_t bottom_t;
  typedef CellComplex2d::vertex_t vertex_t;
  typedef CellComplex2d::edge_t edge_t;
  typedef CellComplex2d::cell_t cell_t;
  typedef CellComplex2d::top_t top_t;

  vertex_t& vertex(size_t i, size_t j)
  {
    return vs[j + i*(n2+1)];
  }

  edge_t& edge2(size_t i, size_t j)
  {
    return es[j + i*n2];
  }

  edge_t& edge1(size_t i, size_t j)
  {
    return es[j + i*(n2+1) + n_hor];
  }

  cell_t& cell(size_t i, size_t j)
  {
    return cs[j + i*n2];
  }

  void create(size_t s1, size_t s2)
  {
    n1 = s1;
    n2 = s2;
    vs.resize((n1+1)*(n2+1), vertex_t::null);
    es.resize((n1*(n2+1) + (n1+1)*n2), edge_t::null);
    cs.resize(n1*n2, cell_t::null);

    n_hor = n2*(n1+1);

    for(size_t i = 0 ; i < n1+1 ; ++i)
      for(size_t j = 0 ; j < n2+1 ; ++j)
      {
        auto v = C.addVertex();
        v->i = i;
        v->j = j;
        ASSERT_NE(vertex_t::null, v);
        vertex(i,j) = v;
      }

    for(size_t i = 0 ; i < n1 ; ++i)
      for(size_t j = 0 ; j < n2 ; ++j)
      {
        auto e1 = edge_t{Grid2DContent{i,j,1}};
        auto e2 = edge_t{Grid2DContent{i,j,2}};
        ASSERT_TRUE(::addCell(C, {-vertex(i,j), +vertex(i,j+1)}, e2));
        edge2(i,j) = e2;
        ASSERT_TRUE(::addCell(C, {-vertex(i,j), +vertex(i+1,j)}, e1));
        edge1(i,j) = e1;
      }

    for(size_t i = 0 ; i < n1 ; ++i)
    {
      auto e = edge_t{Grid2DContent{i,n1,2}};
      ASSERT_TRUE(::addCell(C, {-vertex(i,n2), +vertex(i+1,n2)}, e));
      edge1(i, n2) = e;
    }

    for(size_t j = 0 ; j < n2 ; ++j)
    {
      auto e = edge_t{Grid2DContent{n1,j,2}};
      ASSERT_TRUE(::addCell(C, {-vertex(n1,j), +vertex(n1,j+1)}, e));
      edge2(n1, j) = e;
    }

    for(size_t i = 0 ; i < n1 ; ++i)
      for(size_t j = 0 ; j < n2 ; ++j)
      {
        auto eb = edge1(i,j);
        auto et = edge1(i,j+1);
        auto el = edge2(i,j);
        auto er = edge2(i+1,j);

        auto c = cell_t{Grid2DContent{i,j}};

        ASSERT_TRUE(::addCell(C, {+eb, +er, -et, -el}, c));
        cell(i,j) = c;
      }
  }

  size_t n1 = 0, n2 = 0, n_hor = 0;
  std::vector<vertex_t> vs;
  std::vector<edge_t> es;
  std::vector<cell_t> cs;
  CellComplex2d C;
};

struct CubeGrid
{
  typedef CellComplex3d::bottom_t bottom_t;
  typedef CellComplex3d::vertex_t vertex_t;
  typedef CellComplex3d::edge_t edge_t;
  typedef CellComplex3d::face_t face_t;
  typedef CellComplex3d::cell_t cell_t;
  typedef CellComplex3d::top_t top_t;

  vertex_t& vertex(size_t i, size_t j, size_t k)
  {
    return vs[k + j*(n3+1) + i*(n2+1)*(n3+1)];
  }

  edge_t& edge1(size_t i, size_t j, size_t k)
  {
    return es[k + j*(n3+1) + i*(n2+1)*(n3+1)];
  }

  edge_t& edge2(size_t i, size_t j, size_t k)
  {
    return es[nb_edges1 + k + j*(n3+1) + i*n2*(n3+1)];
  }

  edge_t& edge3(size_t i, size_t j, size_t k)
  {
    return es[nb_edges1 + nb_edges2 + k + j*n3 + i*(n2+1)*n3];
  }

  face_t& face1(size_t i, size_t j, size_t k)
  {
    return fs[k + j*n3 + i*n2*n3];
  }

  face_t& face2(size_t i, size_t j, size_t k)
  {
    return fs[nb_faces1 + k + j*n3 + i*(n2+1)*n3];
  }

  face_t& face3(size_t i, size_t j, size_t k)
  {
    return fs[nb_faces1 + nb_faces2 + k + j*(n3+1) + i*(n3+1)*n2];
  }

  cell_t& cell(size_t i, size_t j, size_t k)
  {
    return cs[k + j*n3 + i*n2*n3];
  }

  void create(size_t s1, size_t s2, size_t s3)
  {
    n1 = s1;
    n2 = s2;
    n3 = s3;

    vs.resize((n1+1)*(n2+1)*(n3+1), vertex_t(0));

    nb_edges1 = n1*(n2+1)*(n3+1);
    nb_edges2 = (n1+1)*n2*(n3+1);
    nb_edges3 = (n1+1)*(n2+1)*n3;

    es.resize(nb_edges1 + nb_edges2 + nb_edges3, edge_t(0));

    nb_faces1 = (n1+1)*n2*n3;
    nb_faces2 = n1*(n2+1)*n3;
    nb_faces3 = n1*n2*(n3+1);

    fs.resize(nb_faces1 + nb_faces2 + nb_faces3, face_t(0));

    cs.resize(n1*n2*n3, cell_t(0));

    // Add the vertices
    for(size_t i = 0 ; i < n1+1 ; ++i)
      for(size_t j = 0 ; j < n2+1 ; ++j)
        for(size_t k = 0 ; k < n3+1 ; ++k)
        {
          auto v = C.addVertex(vertex_t{Grid3DContent{i,j,k}});
          ASSERT_NE(vertex_t::null, v);
          vertex(i,j,k) = v;
        }

    // Add the edges
    size_t cnt = 0;
    for(size_t i = 0 ; i < n1 ; ++i)
    {
      for(size_t j = 0 ; j < n2 ; ++j)
      {
        for(size_t k = 0 ; k < n3 ; ++k)
        {
          { // for all i,j,k
            auto e1 = edge_t{Grid3DContent{i,j,k,1}};
            auto e2 = edge_t{Grid3DContent{i,j,k,2}};
            auto e3 = edge_t{Grid3DContent{i,j,k,3}};
            ASSERT_TRUE(::addCell(C, {-vertex(i,j,k), +vertex(i+1,j,k)}, e1));
            ASSERT_TRUE(::addCell(C, {-vertex(i,j,k), +vertex(i,j+1,k)}, e2));
            ASSERT_TRUE(::addCell(C, {-vertex(i,j,k), +vertex(i,j,k+1)}, e3));
            ASSERT_EQ(edge_t::null, edge1(i,j,k));
            ASSERT_EQ(edge_t::null, edge2(i,j,k));
            ASSERT_EQ(edge_t::null, edge3(i,j,k));
            ++cnt;
            edge1(i,j,k) = e1;
            ++cnt;
            edge2(i,j,k) = e2;
            ++cnt;
            edge3(i,j,k) = e3;
          }
          if(i==0) // for all j,k -> add edges e2, e3 with i=n1
          {
            auto e2 = edge_t{Grid3DContent{n1, j, k, 2}};
            auto e3 = edge_t{Grid3DContent{n1, j, k, 3}};
            ASSERT_TRUE(::addCell(C, {-vertex(n1,j,k), +vertex(n1,j+1,k)}, e2));
            ASSERT_TRUE(::addCell(C, {-vertex(n1,j,k), +vertex(n1,j,k+1)}, e3));
            ASSERT_EQ(edge_t::null, edge2(n1,j,k));
            ASSERT_EQ(edge_t::null, edge3(n1,j,k));
            ++cnt;
            edge2(n1,j,k) = e2;
            ++cnt;
            edge3(n1,j,k) = e3;
          }
          if(j == 0) // for all i,k -> add edges e1, e3 with j=n2
          {
            auto e1 = edge_t{Grid3DContent{i,n2,k,1}};
            auto e3 = edge_t{Grid3DContent{i,n2,k,3}};
            ASSERT_TRUE(::addCell(C, {-vertex(i,n2,k), +vertex(i+1,n2,k)}, e1));
            ASSERT_TRUE(::addCell(C, {-vertex(i,n2,k), +vertex(i,n2,k+1)}, e3));
            ASSERT_EQ(edge_t::null, edge1(i,n2,k));
            ASSERT_EQ(edge_t::null, edge3(i,n2,k));
            ++cnt;
            edge1(i,n2,k) = e1;
            ++cnt;
            edge3(i,n2,k) = e3;
            if(i == 0) // for all k -> add edges e3 with i=n1, j=n2
            {
              auto e3 = edge_t{Grid3DContent{n1,n2,k,3}};
              ASSERT_TRUE(::addCell(C, {-vertex(n1,n2,k), +vertex(n1,n2,k+1)}, e3));
              ASSERT_EQ(edge_t::null, edge3(n1,n2,k));
              ++cnt;
              edge3(n1,n2,k) = e3;
            }
          }
        }
        { // for all i,j -> add edges e1, e2 with n3=n3
          auto e1 = edge_t{Grid3DContent{i,j,n3,1}};
          auto e2 = edge_t{Grid3DContent{i,j,n3,2}};
          ASSERT_TRUE(::addCell(C, {-vertex(i,j,n3), +vertex(i+1,j,n3)}, e1));
          ASSERT_TRUE(::addCell(C, {-vertex(i,j,n3), +vertex(i,j+1,n3)}, e2));
          ASSERT_EQ(edge_t::null, edge1(i,j,n3));
          ASSERT_EQ(edge_t::null, edge2(i,j,n3));
          ++cnt;
          edge1(i,j,n3) = e1;
          ++cnt;
          edge2(i,j,n3) = e2;
        }
        if(i==0) // for all j -> add edges e2 with i=n1, k=n3
        {
          auto e2 = edge_t{Grid3DContent{n1,j,n3,2}};
          ASSERT_TRUE(::addCell(C, {-vertex(n1,j,n3), +vertex(n1,j+1,n3)}, e2));
          ASSERT_EQ(edge_t::null, edge2(n1,j,n3));
          ++cnt;
          edge2(n1,j,n3) = e2;
        }
      }
      { // for all i -> add edges e1 with n2=n2, n3=n3
        auto e1 = edge_t{Grid3DContent{i,n2,n3,1}};
        ASSERT_TRUE(::addCell(C, {-vertex(i,n2,n3), +vertex(i+1,n2,n3)}, e1));
        ASSERT_EQ(edge_t::null, edge1(i,n2,n3));
        ++cnt;
        edge1(i,n2,n3) = e1;
      }
    }
    ASSERT_EQ(cnt, es.size());

    cnt = 0;
    // Add the faces
    for(size_t i = 0 ; i < n1 ; ++i)
    {
      for(size_t j = 0 ; j < n2 ; ++j)
      {
        for(size_t k = 0 ; k < n3 ; ++k)
        {
          { // for all i,j,k
            auto f1 = face_t{Grid3DContent{i,j,k,1}};
            auto f2 = face_t{Grid3DContent{i,j,k,2}};
            auto f3 = face_t{Grid3DContent{i,j,k,3}};

            ASSERT_TRUE(::addCell(C, {+edge2(i,j,k), -edge2(i,j,k+1), -edge3(i,j,k), +edge3(i,j+1,k)}, f1));
            ASSERT_TRUE(::addCell(C, {+edge1(i,j,k), -edge1(i,j,k+1), -edge3(i,j,k), +edge3(i+1,j,k)}, f2));
            ASSERT_TRUE(::addCell(C, {+edge1(i,j,k), -edge1(i,j+1,k), -edge2(i,j,k), +edge2(i+1,j,k)}, f3));

            ++cnt;
            face1(i,j,k) = f1;
            ++cnt;
            face2(i,j,k) = f2;
            ++cnt;
            face3(i,j,k) = f3;
          }
          if(i == 0) // For all j,k add f1 with i == n1
          {
            auto f1 = face_t{Grid3DContent{n1,j,k,1}};
            f1->i = n1;
            f1->j = j;
            f1->k = k;

            ASSERT_TRUE(::addCell(C, {+edge2(n1,j,k), -edge2(n1,j,k+1), -edge3(n1,j,k), +edge3(n1,j+1,k)}, f1));
            ++cnt;
            face1(n1,j,k) = f1;
          }
          if(j == 0) // For all i,k add f2 with j = n2
          {
            auto f2 = face_t{Grid3DContent{i,n2,k,2}};

            ASSERT_TRUE(::addCell(C, {+edge1(i,n2,k), -edge1(i,n2,k+1), -edge3(i,n2,k), +edge3(i+1,n2,k)}, f2));
            ++cnt;
            face2(i,n2,k) = f2;
          }
        }
        { // For all i,j add f3 with k = n3
          auto f3 = face_t{Grid3DContent{i,j,n3,3}};

          ASSERT_TRUE(::addCell(C, {+edge1(i,j,n3), -edge1(i,j+1,n3), -edge2(i,j,n3), +edge2(i+1,j,n3)}, f3));
          ++cnt;
          face3(i,j,n3) = f3;
        }
      }
    }

    ASSERT_EQ(cnt, fs.size());

    // Add cells
    for(size_t i = 0 ; i < n1 ; ++i)
      for(size_t j = 0 ; j < n2 ; ++j)
        for(size_t k = 0 ; k < n3 ; ++k)
        {
          auto f11 = face1(i,j,k);
          auto f12 = face1(i+1,j,k);
          auto f21 = face2(i,j,k);
          auto f22 = face2(i,j+1,k);
          auto f31 = face3(i,j,k);
          auto f32 = face3(i,j,k+1);

          auto c = cell_t{Grid3DContent{i,j,k}};

          ASSERT_TRUE(::addCell(C, {+f21, +f12, +f32, -f11, -f22, -f31}, c));
          cell(i,j,k) = c;
        }
  }

  size_t n1 = 0, n2 = 0, n3 = 0;

  size_t nb_edges1 = 0, nb_edges2 = 0, nb_edges3 = 0;
  size_t nb_faces1 = 0, nb_faces2 = 0, nb_faces3 = 0;

  std::vector<vertex_t> vs;
  std::vector<edge_t> es;
  std::vector<face_t> fs;
  std::vector<cell_t> cs;
  CellComplex3d C;
};

#endif // TEST_REGULAR_H

