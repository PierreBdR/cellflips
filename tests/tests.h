#ifndef TESTS_TESTS_H
#define TESTS_TESTS_H

#include <cellflips/cellflips.h>
#include <cellflips/cellflips_edition.h>
#include <gtest/gtest.h>

#include <iostream>

using cellflips::set;
using cellflips::chain;

template <typename CellComplex, int N1>
::testing::AssertionResult splitCell(CellComplex& C, cellflips::ncell_t<N1,CellComplex> cell)
{
  auto result = cellflips::splitCell(C, cell);
  if(result)
    return ::testing::AssertionSuccess();
  return ::testing::AssertionFailure() << "Failed to split cell " << cell << " with error: " << C.errorString().toStdString();
}

template <typename CellComplex, int N1>
::testing::AssertionResult replace(CellComplex& C,
                                   cellflips::Chain<cellflips::ncell_t<N1,CellComplex> > c1,
                                   cellflips::Chain<cellflips::ncell_t<N1,CellComplex> > c2)
{
  if(cellflips::replace(C, c1, c2))
    return ::testing::AssertionSuccess();
  return ::testing::AssertionFailure() << "Failed to replace cell with error: " << C.errorString().toStdString();
}

template <typename CellComplex, int N1>
::testing::AssertionResult addCell(CellComplex& C,
                                   cellflips::Chain<cellflips::ncell_t<N1,CellComplex> > container,
                                   cellflips::ncell_t<N1+1,CellComplex> cell,
                                   bool link_to_top = true)
{
  cellflips::ncell_t<N1+1,CellComplex> result = cellflips::addCell(C, container, cell, link_to_top);
  if(result)
    return ::testing::AssertionSuccess();
  return ::testing::AssertionFailure() << "Failed to add cell with error: " << C.errorString().toStdString();
}

template <typename CellComplex, typename Content, int N1>
::testing::AssertionResult addCell(CellComplex& C,
                                   std::initializer_list<Content> container,
                                   cellflips::ncell_t<N1,CellComplex> cell,
                                   bool link_to_top = true)
{
  return ::addCell(C, chain(container), cell, link_to_top);
}

template <typename CellComplex, int N1>
::testing::AssertionResult addCellFailure(CellComplex& C,
                                          cellflips::Chain<cellflips::ncell_t<N1,CellComplex> > container,
                                          cellflips::ncell_t<N1+1,CellComplex> cell,
                                          const typename CellComplex::ErrorCodes expected,
                                          bool link_to_top = true)
{
  cellflips::ncell_t<N1+1,CellComplex> result = cellflips::addCell(C, container, cell, link_to_top);
  if(not result)
  {
    if(C.error == expected)
    {
      C.error = CellComplex::NO_ERROR;
      return ::testing::AssertionSuccess();
    }
    return ::testing::AssertionFailure() << "Wrong type of failure. Expected '" << C.errorString(expected).toStdString() << "' but got '" << C.errorString().toStdString() << "'";
  }
  return ::testing::AssertionFailure() << "Add cell succeeded when failure was expected";
}

template <typename CellComplex, typename Content, int N1>
::testing::AssertionResult addCellFailure(CellComplex& C,
                                          std::initializer_list<Content> container,
                                          cellflips::ncell_t<N1,CellComplex> cell,
                                          const typename CellComplex::ErrorCodes expected,
                                          bool link_to_top = true)
{
  return ::addCellFailure(C, chain(container), cell, expected, link_to_top);
}

namespace std
{
template <typename T, typename Traits, int N, typename Content>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const cellflips::Cell<N,Content>& c)
{
  ss << "[" << N << "|";
  if(c)
    ss << num(*c);
  else
    ss << u8"Ø";
  ss << "]";
  return ss;
}

template <typename T, typename Traits, int N>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const cellflips::Cell<N,cellflips::TopCellContent>& )
{
  ss << "[" << N << "|T]";
  return ss;
}

template <typename T, typename Traits>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const cellflips::Cell<-1,cellflips::EmptyCell>& )
{
  ss << "[-1|_]";
  return ss;
}

template <typename T, typename Traits, typename... Args>
basic_ostream<T, Traits>& operator<<(basic_ostream<T, Traits>& ss, const cellflips::CellComplex<Args...>& C)
{
  ss << "CC<" << sizeof...(Args) << ">";
  return ss;
}

template <typename T, typename Traits>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const cellflips::RelativeOrientation& ro)
{
  switch(ro)
  {
    case cellflips::pos:
      ss << "+";
      break;
    case cellflips::neg:
      ss << "-";
      break;
    default:
      ss << "?";
  }
  return ss;
}

template <typename T, typename Traits, typename Object>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const cellflips::OrientedObject<Object>& c)
{
  ss << c.orientation() << (~c);
  return ss;
}

template <typename T, typename Traits, int N, typename Content>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const cellflips::Chain<cellflips::Cell<N,Content> >& cs)
{
  ss << "{" << N << "|";
  for(const auto& oc: cs)
  {
    ss << oc.orientation() << num(*~oc) << ";";
  }
  ss << "}";
  return ss;
}

template <typename T, typename Traits, int N, typename Content>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const cellflips::Set<cellflips::Cell<N,Content> >& cs)
{
  ss << "{" << N << "|";
  for(const auto& c: cs)
  {
    ss << num(*c) << ";";
  }
  ss << "}";
  return ss;
}

namespace detail
{
template <bool B>
using EnableIf = typename std::enable_if<B,int>::type;

template <size_t N, typename T, typename Traits, typename Tuple,
          EnableIf<(N==1)> = 0>
void printTupleElement(basic_ostream<T,Traits>& ss, const Tuple& t)
{
  ss << get<0>(t);
}

template <size_t N, typename T, typename Traits, typename Tuple,
          EnableIf<(N==0)> = 0>
void printTupleElement(basic_ostream<T,Traits>& , const Tuple&)
{
}

template <size_t N, typename T, typename Traits, typename Tuple,
          EnableIf<(N>1)> = 0>
void printTupleElement(basic_ostream<T,Traits>& ss, const Tuple& t)
{
  printTupleElement<N-1>(ss, t);
  ss << "," << get<N-1>(t);
}

}

template <typename T, typename Traits, typename... Args>
basic_ostream<T,Traits>& operator<<(basic_ostream<T,Traits>& ss, const tuple<Args...>& t)
{
  ss << "{";
  detail::printTupleElement<sizeof...(Args)>(ss, t);
  ss << "}";
  return ss;
}

}

#endif // TESTS_TESTS_H

